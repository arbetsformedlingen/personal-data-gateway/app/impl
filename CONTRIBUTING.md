# Contributing to [Swedish Public Employment Service](https://arbetsformedlingen.se/) source code

## Ways to Contribute

As a new contributor, you are in an excellent position to give us feedback to our project. For example, you could:

- Fix or report a bug.
- Suggest enhancements to code, tests and documentation.
- Report/fix problems found during installing or developer environments.
- Add suggestions for something else that is missing.

## Agreement

The project reserves the right to reject contributions that don't adhere to project guidelines or align with its goals/architecture.

By contributing, you agree that your submissions will be governed by the project's licence terms, following the "inbound=outbound" norm. This means your contributions will be subject to the same terms as the project's licences, unless explicitly stated otherwise.

## Contribute

It is always best to discuss your plans beforehand, to ensure that your contribution is in line with the project goals.

### File an Issue

Please check briefly if there already exists an Issue with your topic.
If so, you can just add a comment to that with your information instead of creating a new Issue.

### Report a bug

Reporting bugs is a good and easy way to contribute.
To do this, open an Issue that summarizes the bug and set the label to "bug".

### Suggest a feature

To request a new feature you should and summarize the desired functionality and its use case.
Set the Issue label to "feature" or "enhancement".

### Reporting security issues

If you discover a security issue, please bring it to our attention by sending a mail to jobtechdev@arbetsformedlingen.se

### Workflow for code contributions

Generally speaking, you should fork this repository, make changes in your own fork, and then submit a merge request.
This workflow is common, maybe even expected if nothing else mentioned, and is called the [Fork and pull model](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

A practical example of such a workflow could be:

1. Fork the repository.
2. Create a topic branch from your fork main branch, i.e. myfeaturebranch
3. Push your changes to the topic branch (in your fork) of the repository, i.e myfeaturebranch.
4. Open a new merge request to main project.
5. Project maintainers might comment and give feedback on your merge request.

## Feedback and response times

You should expect feedback for an Issue or Merge Request in 5 business days.
NOTE: Feedback might mean a lot of things depending on the scope of your Issue/Merge Request.
What you should expect is at least a comment on your Issue. The issue tracker is not a discussion forum, for discussions please use https://forum.jobtechdev.se.

**_Happy contributing!_**
