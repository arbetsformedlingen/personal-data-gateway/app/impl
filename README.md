An application for individuals to manage their own personal data sharings with third parties.

Implemented with NextJS and tRPC.

This implementation strives to be generic and reusable.

# Table of contents

[[_TOC_]]

# Glossary

# Technology

- [Next.js](https://nextjs.org/)
- [tRPC](https://trpc.io/)
- [openapi-fetch](https://openapi-ts.pages.dev/)
- [pino](https://getpino.io/)
- [superjson]()
- [Zod](https://zod.dev/)
- [@digi/arbetsformedlingen](https://digi.arbetsformedlingen.se/)
- [@digi/arbetsformedlingen-react](https://digi.arbetsformedlingen.se/)
- [Playwright](https://playwright.dev/)
- [Storybook](https://storybook.js.org/)
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/)
- [ESLint](https://eslint.org/)
- [Vitest](https://vitest.dev/)
- [tailwindcss](https://tailwindcss.com/)
- [TypeScript](https://www.typescriptlang.org/)

# Specification first

To reduce the amount of code, we opt for a _specification first methodology_, where we leverage code generation tools to produce code for integration with external APIs. This helps the implementation be aligned accordingly to the specifications.

# Prerequisites

- Requires connection to an API that is compliant with the [Personal Data Gateway API Specification](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs).

# Authentication

User authentication is expected to be handled by a reverse proxy deployed in front of this application. The user's identity is expected to be provided via HTTP header. The header name is configured via the configuration property `AUTH_HEADER`.

For development purposes, we provide a reverse proxy you can use: [Auth proxy](https://gitlab.com/arbetsformedlingen/individdata/axa-pilot/auth-proxy).

# Getting started

Use one of the [published images at the jobtech registry](https://nexus.jobtechdev.se/#browse/browse:JobtechdevDockerRegistry:v2%2Fpersonal-data-gateway-app%2Fpersonal-data-gateway-app%2Ftags%2Fmain) or build the docker image yourself using command: `docker build -t personal-data-gateway:latest .`

## Configuration

While developing you may configure the application by file or environment variables.

The following configuration properties are supported.

| Property | Description |
| - | - |
| NODE_ENV | Allowed values: `development`, `test`, `production` |
| SERVICE_URL | The base url of the PDG Api which this application will interact with. |
| AUTH_HEADER | The HTTP request header name that the application expects to hold the authenticated user's id. |
| X_AUTH_KEY | The authentication key this application uses for queries to the Personal Data Gateway Api. |
| NEXT_PUBLIC_MAINTENANCE_MODE | Activates the maintenance page. Valid values are: "true" |
| NEXT_PUBLIC_ALERT_LEVEL | Defining this property activates the alert banner to be displayed at the top of all pages. Valid values are: "SUCCESS", "INFO", "WARNING", "DANGER". |
| NEXT_PUBLIC_ALERT_TITLE | Text to display as alert header. |
| NEXT_PUBLIC_ALERT_TEXT | Text to display as alert description. |

### Configuration by file

Create a new `.env` file in the project root. Have a look at [`.env.example`](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/-/blob/main/.env.example?ref_type=heads)

### Configuration by environment variables

Configuration values provided as environment variables have a higher order of precedence than ones provided by file (`.env`).

# Development

The repository contains a devcontainer configuration that provides a consistent development environment.

To run the development server use `npm run dev`.

The reverse proxy container runs Caddy on port 4000 and uses the fakeauth container to authenticate requests before
sending them to the devcontainer at port 3000.

## Backend API

The backend api adheres to OpenAPI standards and the client library can be built from the specification.
API client files from the specification, this is automatically done when 
running `npm install`.
Use the command `npm run openapi:generate` to generate 
The specification is stored in the repo [Specifications](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs) at GitLab.

## Tests

Tests are run with Vitest which is fast, Jest compatible and works well with TypeScript.

To run all tests with `npm test` or, if you want test coverage and UI `npm run test:ui`.

### End-to-end with Playwright

For tests with Playwright the application server needs to be started before running the tests with `npm run test:e2e`.
You should preferrably use a production build for these tests (`npm run build` followed by `npm run start`) as the tests
will be faster and more reliable.

### Visual testing with Storybook

For development and visual testing of the project components start Storybook with `npm run storybook`.

## Podman-compose

Different podman-compose commands can be run from within the project directory (from outside the devcontainer) if you
specify `-f` and `-p`. For instance if you want to show the logs from the backend (`logs backend`):

```bash
podman-compose \
-f .devcontainer/docker-compose.development.yml \
-p afconnectservicesfrontenddevcontainer \
logs backend
```

This can also be used to update one of the containers with `pull` and `up -d <service>`.
