# Install dependencies and build from source
FROM node:22.14.0-alpine3.21 as build
# FROM node:22.4-bookworm-slim as build

RUN apk update && apk add --no-cache git && \
  # Upgrade npm
  npm upgrade -g npm
# RUN apt-get update && apt-get install -y git && \
  # Upgrade npm
  #npm upgrade -g npm

WORKDIR /app

# Install node modules
COPY package.json package-lock.json .npmrc ./
COPY ./patches ./patches

RUN npm clean-install

COPY . .

ENV NEXT_TELEMETRY_DISABLED=1

RUN SKIP_ENV_VALIDATION=1 npm run build

# Production image, copy files and run
FROM node:22.14.0-alpine3.21
#FROM node:22.4-bookworm-slim

RUN apk update && apk add --no-cache curl tini=0.19.0-r3 &&\
# RUN apt-get update && apt-get install -y curl tini &&\
    adduser --system --uid 1001 nextjs\
    dpkg -s stdlib

WORKDIR /app

ENV NODE_ENV=production \
  NEXT_TELEMETRY_DISABLED=1

# Copy NextJS build artifacts
COPY --from=build --chown=nextjs:node /app/public ./public
COPY --from=build --chown=nextjs:node /app/.next/standalone /app/logging.config.ts ./
COPY --from=build --chown=nextjs:node /app/.next/static ./.next/static


USER nextjs:node

EXPOSE 3000

ENV PORT 3000

ENTRYPOINT [ "/sbin/tini", "--" ]
CMD ["node", "server.js"]
