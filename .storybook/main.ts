import type { StorybookConfig } from "@storybook/nextjs";

const config: StorybookConfig = {
  stories: ["../stories/**/*.mdx", "../src/**/*.stories.@(js|jsx|mjs|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "@storybook/addon-a11y",
    "@storybook/addon-styling-webpack",
    ({
      name: "@storybook/addon-styling-webpack",

      options: {
        rules: [{
          test: /\.css$/,
          sideEffects: true,
          use: [
            require.resolve("style-loader"),
            {
              loader: require.resolve("css-loader"),
              options: {

                importLoaders: 1,
              },
            }, {
              loader: require.resolve("postcss-loader"),
              options: {
                implementation: require.resolve("postcss"),
              },
            },
          ],
        },],
      }
    }),
    "@storybook/addon-themes",
    "@storybook/addon-mdx-gfm",
    'storybook-react-intl',
  ],
  framework: {
    name: "@storybook/nextjs",
    options: {
      builder: {
        useSWC: true
      }
    },
  },
  staticDirs: ['../public'],
  docs: {
    autodocs: "tag",
  },
  /**
   * Sometimes Storybook gets stuck when starting due to "Error: EBUSY: resource busy or locked"...
   * A temporary workaround is applied below (see https://github.com/storybookjs/storybook/issues/23131)
   */
  webpack: (config, options) => {
    options.cache.set = () => Promise.resolve({ path: '' });
    return config;
  }
};

export default config;
