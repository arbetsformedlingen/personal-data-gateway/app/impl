import { type TRPCError } from '@trpc/server';
import { getHTTPStatusCodeFromError } from '@trpc/server/http';
import { TRPC_ERROR_CODES_BY_KEY } from '@trpc/server/rpc';
import { type AppRouter } from '@app/server/api/root';
import { getBaseUrl } from '@app/utils/api';
import superjson from 'superjson';
import { createTRPCMsw as createTRPCMSW } from 'msw-trpc';

import React from 'react';
import { createTRPCReact, httpLink } from '@trpc/react-query';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const mockedTRPC = createTRPCReact<AppRouter>({
  unstable_overrides: {
    useMutation: {
      async onSuccess(opts) {
        await opts.originalFn();
        await opts.queryClient.invalidateQueries();
      },
    },
  },
});

const mockedTRPCClient = mockedTRPC.createClient({
  transformer: superjson,
  links: [httpLink({ url: 'http://localhost:3000/api/trpc' })],
});

const mockedQueryClient = new QueryClient();

export const MockedTRPCProvider = (props: { children: React.ReactNode }) => {
  return (
    <mockedTRPC.Provider client={mockedTRPCClient} queryClient={mockedQueryClient}>
      <QueryClientProvider client={mockedQueryClient}>{props.children}</QueryClientProvider>
    </mockedTRPC.Provider>
  );
};

export const trpcMsw = createTRPCMSW<AppRouter>({
  basePath: '/api/trpc',
  baseUrl: getBaseUrl(),
  transformer: {
    input: superjson,
    output: superjson,
  },
});

export const mockTrpcErrorResponse = (error: TRPCError, path?: string) => {
  return {
    error: {
      json: {
        message: error.message,
        code: TRPC_ERROR_CODES_BY_KEY[error.code],
        data: {
          code: error.code,
          httpStatus: getHTTPStatusCodeFromError(error),
          stack: error.stack,
          path: path,
        },
      },
    },
  };
};
