/* eslint-disable @typescript-eslint/no-unsafe-return */
import { trpcMsw } from "../mockTrpc"

export const previewIsLoading = () => {
  return trpcMsw.datasource.getDatasourceUserData.query(() => {
    return {
      status: 200,
      error: '',
      message: undefined
  }})
}
