/* eslint-disable @typescript-eslint/no-unsafe-return */
import { TRPCError } from "@trpc/server";
import { trpcMsw } from "../mockTrpc"

export const approveSharingHandler = () => {
  return trpcMsw.sharing.approveSharing.mutation(({ foreignUserId }) => {
    const authCode = '1c0616eb-4297-4e92-ac26-4906f16fa7e7'
    return {
      redirectUrl: `http://example.com/return?authCode=${authCode}&state=${foreignUserId}&message=OK`,
    };
  })
}

export const approveSharingHandlerError = () => {
  return trpcMsw.sharing.approveSharing.mutation(() => {
    throw new TRPCError({ code: 'INTERNAL_SERVER_ERROR', message: 'Some error message.' });
  })
}

export const revokeSharingHandler = () => {
  return trpcMsw.sharing.revokeSharing.mutation(({ foreignUserId }) => {
    return {
      redirectUrl: `http://example.com/return?authCode=&state=${foreignUserId}&message=Error`,
    };
  })
}

export const revokeSharingHandlerError = () => {
  return trpcMsw.sharing.revokeSharing.mutation(() => {
    throw new TRPCError({ code: 'INTERNAL_SERVER_ERROR', message: 'Some error message.' });
  })
}
