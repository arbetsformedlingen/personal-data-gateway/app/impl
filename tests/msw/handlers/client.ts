/* eslint-disable @typescript-eslint/no-unsafe-return */
import { trpcMsw } from "../mockTrpc"

export const getClient = () => {
  return trpcMsw.client.getClient.query(() => {
    return {
      clientId: 'abc-000001',
      name: 'Försäkringsbolag AB',
      organizationId: '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
    };
  })
}

export const getClientRedirects = () => {
  return trpcMsw.client.getClientRedirects.query(() => {
    return [{
      _links: {},
      redirectId: '66c1fd4a-c160-4825-a864-6ac23f37d6d9',
      clientId: 'abc-000001',
      redirectUrl: 'https://example.com/return',
      created: '2024-02-02T15:08:24Z',
      updated: '2024-02-02T16:08:24Z',
    }];
  })
}
