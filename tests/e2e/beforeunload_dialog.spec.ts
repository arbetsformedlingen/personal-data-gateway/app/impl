/* eslint-disable @typescript-eslint/no-misused-promises */
import { test, expect } from '@playwright/test';

test('should pop up beforeunload confirmation dialog when trying to leave page', async ({ page }) => {
  // Visit page
  await page.goto('/?clientId=abcd&state=efgh&scope=query-unemployment');

  // Sign in
  await page.getByRole('combobox').click();
  await page.getByLabel('9010102383').check();

  await page.getByRole('button', { name: 'Logga in' }).click();

  // Interact with page to allow beforeunload event to trigger.
  await page.locator('label').filter({ hasText: 'Jag vill automatiskt skicka intyget att jag är inskriven' }).locator('span').click();

  // When dialog pops up, dismiss if it's a beforeunload dialog.
  page.once('dialog', async (dialog) => {
    // if (dialog.type() !== 'beforeunload') {
    //   throw new Error('Wrong type of dialog.');
    // };
    await dialog.dismiss();
  });

  // Try to leave the page and wait for navigation error (dialog dismissed).
  await expect(page.goto('https://google.com')).rejects.toThrow();
});

test('should pop up beforeunload confirmation dialog when trying to close page', async ({ page }) => {
  // Visit page
  await page.goto('/?clientId=abcd&state=efgh&scope=query-unemployment');

  // Sign in
  await page.getByRole('combobox').click();
  await page.getByLabel('9010102383').check();

  await page.getByRole('button', { name: 'Logga in' }).click();

  // Interact with page to allow beforeunload event to trigger.
  await page.locator('label').filter({ hasText: 'Jag vill automatiskt skicka intyget att jag är inskriven' }).locator('span').click();

  page.once('dialog', async (dialog) => {
    await dialog.dismiss();
  });

  // Try to leave the page, it should be dismissed by the handler.
  await page.close({ runBeforeUnload: true });

  // Wait 10ms to allow the page to settle
  await page.waitForTimeout(10);

  // Expect page to still be open.
  expect(page.isClosed()).toBe(false);
});

test('should close after pop up beforeunload confirmation dialog accepted', async ({ page }) => {
  // Visit page
  await page.goto('/?clientId=abcd&state=efgh&scope=query-unemployment');

  // Sign in
  await page.getByRole('combobox').click();
  await page.getByLabel('9010102383').check();

  await page.getByRole('button', { name: 'Logga in' }).click();

  // Interact with page to allow beforeunload event to trigger.
  await page.locator('label').filter({ hasText: 'Jag vill automatiskt skicka intyget att jag är inskriven' }).locator('span').click();

  // When dialog pops up, dismiss if it's a beforeunload dialog.
  page.once('dialog', async (dialog) => {
    await dialog.accept();
  });

  // Try to leave the page, it should be dismissed by the handler.
  await page.close({ runBeforeUnload: true });

  // Wait 10ms to allow the page to settle
  await page.waitForTimeout(100);

  // Expect page to be closed.
  expect(page.isClosed()).toBe(true);
});
