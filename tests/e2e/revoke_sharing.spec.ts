import { test, expect } from '@playwright/test';

test('should redirect with search parameters when not received sharing', async ({ page }) => {
  await page.goto('/?clientId=abcd&state=efgh&scope=query-unemployment');

  await page.getByRole('combobox').click();
  await page.getByLabel('9010102383').check();

  await page.getByRole('button', { name: 'Logga in' }).click();

  await page.locator('label').filter({ hasText: 'Jag vill själv hämta intyget hos Arbetsförmedlingen och skicka' }).locator('span').click();

  await page.getByRole('button', { name: 'Skicka' }).click();

  await page.waitForURL("https://example.com/redirect?**");

  const url = new URL(page.url());

  expect(url.searchParams.get('message')).toBe('Error')
  expect(url.searchParams.get('state')).toBe('efgh')
  expect(url.searchParams.get('authCode')).toBe('')
});