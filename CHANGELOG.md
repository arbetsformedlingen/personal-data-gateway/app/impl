# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.2.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/compare/v1.1.2...v1.2.0) (2025-02-24)


### Features

* add elastic-apm-node, run npm audit fix ([e0ebf9d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e0ebf9dc4eb0970566e8c793956c3c9a12f7ae6c))


### Bug Fixes

* add outputFileTracingIncludes to copy necessary files from elastic-apm-node to standalone ([4d0586e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/4d0586eb706abed68952bef6d53800898171eca5))
* **deps:** update dependency @t3-oss/env-nextjs to ^0.12.0 ([da8816e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/da8816ec930639b645a19ee959a8608c5bd8970d))
* **deps:** update dependency openapi-fetch to v0.13.4 ([7e95c3f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/7e95c3fb5dd9b0f79bd62f4976f769f8dd420a51))
* **deps:** update dependency pino to v9.6.0 ([8cbee92](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/8cbee92e9bb921769fcba264c05ce80aa18defde))
* **deps:** update dependency react-intl to v7 ([f2e9219](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f2e92191a02b876dd59bc4729e1cdb86d1ef2d52))
* **deps:** update nextjs monorepo to v14.2.23 ([e3c6f88](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e3c6f881072c10b9837f86f8d50d171ba151fdbe))
* pass 'sharingId' parameter when user have an existing sharing and clicks return. ([cb52182](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/cb52182f5de56bf5711efaf0fba019c4dc3d6324))

## [1.1.2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/compare/v1.1.1...v1.1.2) (2025-01-09)


### Bug Fixes

* align with pdg api spec v3 ([d7cc9af](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d7cc9afa6115f3177072b51622f3c65d41299d86))
* **deps:** update dependency proxy-agent to v6.5.0 ([f85ba39](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f85ba39d67b1e09d5a7ff409ac2ebfe514fd77a6))
* **deps:** update dependency zod to v3.24.1 ([ab70974](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/ab70974706333f5070446ed9b419b941d1654ac1))
* **deps:** update nextjs monorepo to v14.2.22 ([efddda8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/efddda83718ac39d44937e61786f4e5bc7327112))

## [1.1.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/compare/v1.1.0...v1.1.1) (2024-12-13)


### Bug Fixes

* add 'state' and 'message' parameters to 'Close & return'-redirect. ([55fb744](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/55fb74455f11964ce5280f1f46b77c1447dc7d8b))
* **deps:** update dependency openapi-fetch to ^0.13.0 ([2e46e51](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2e46e51cbe60e823ae9b024e29daaca6861991a2))
* **deps:** update dependency openapi-fetch to v0.13.3 ([b71b9a9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/b71b9a98ec58f276c82fbfbe3f52c158b9ae7da7))
* **deps:** update dependency pino to v9.5.0 ([49eddfb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/49eddfbe26d92d604e72f5d745575c113fafd352))
* **deps:** update dependency react-intl to v6.8.7 ([371de63](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/371de639dfa933260c00d67d065bc5ae47362030))
* **deps:** update dependency react-intl to v6.8.9 ([dfd0fc3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/dfd0fc3c3b51977fc10c5a19f7cf2d9b3a1cbe90))
* **deps:** update dependency superjson to v2.2.2 ([e890d53](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e890d53abcfde667aac75d336029433fb407ec02))
* pass "state" and "message" parameters when the user cancels the sharing procedure ([d089990](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d08999076989d5a87f4226594e7c41b771582db3))

## [1.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/compare/v1.0.0...v1.1.0) (2024-11-01)


### Features

* An alert banner can now be displayed at the top of the page (activated via env, see readme for more information) ([d35eb8b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d35eb8bb1a54d3422facc99b34e125298bc57da5))
* approval form layout aligned with figma ([437c48a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/437c48a2f12d779605fecd6be65afe39172564e6))
* datasource faqs are now also listed at the sharing page ([a96d354](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a96d354abe89e5436d1e7e01a8887cd641ba04b6))
* implement index page with sharing listing and page where sharings can be revoked ([95256e0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/95256e02a77e48eff5148031d513dab4945c4989))
* improved info and error logging for api router handlers. ([9d64ede](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/9d64edef029adb9d52e027f705dc72996f7f6bbd))
* landing page with sharings list (wip) ([594cbee](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/594cbee53261f30a0a262a5bd3d51a991f405535))
* Maintenance mode can now be enabled by setting env 'NEXT_PUBLIC_MAINTENANCE_MODE' to 'true' ([b8d83b8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/b8d83b8ac71bd84460948bb2baa2972c5b224f72))
* new logger implementation, using ECS format, and LOG_LEVEL is customizable via environment variables (defaults to "LOG_LEVEL=debug" in test suite) ([c7e0af3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/c7e0af35c3f3cf63b3270e9164bec8a85c981674))
* use node 22, remove server side logging ([d399ea6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d399ea60a7f5854d02cf2feed425e722e11d7c5b))


### Bug Fixes

* add IntlProvider to storybook preview rendering and added storybook-react-intl for rendering a locale switcher in storybook ui ([698216b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/698216be4f8bd6284271a2946eb0775fa182f65b))
* align clientAuthMiddleware implementation to the api changes of openapi-fetch v0.10.x ([f014fe1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f014fe1c37749892d8caa7f334e811de2f336d0f))
* applied workaround for issue where Storybook gets stuck on start due to 'Error: EBUSY: resource busy or locked' ([3cfadc7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/3cfadc776987918a2855265c4a0b2734d157c674))
* **deps:** update dependency @t3-oss/env-nextjs to ^0.11.0 ([197028e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/197028efed3f8a281becd10a3626314c8ee35944))
* **deps:** update dependency @t3-oss/env-nextjs to v0.11.1 ([f55cc69](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f55cc6925e755d618e4d2914a42434e56aacc4d7))
* **deps:** update dependency openapi-fetch to ^0.10.0 ([57451e6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/57451e6543278f3ea057bda965b50f60a747f679))
* **deps:** update dependency openapi-fetch to ^0.11.0 ([f669c47](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f669c478b275e83333ae12cc7534677c0679909a))
* **deps:** update dependency openapi-fetch to v0.10.2 ([7041b52](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/7041b522873249b3766b5f089d996034abeaf509))
* **deps:** update dependency openapi-fetch to v0.10.4 ([b32ad8f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/b32ad8fe286846ec43b259f47d5b2bc424536d22))
* **deps:** update dependency openapi-fetch to v0.10.5 ([d516f75](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d516f750bae766ae61ad21694e822ec31094b654))
* **deps:** update dependency pino to v9.3.2 ([6656af3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/6656af37515b159a54d2ab1cf632e4b41840bdc7))
* **deps:** update dependency pino to v9.4.0 ([c5675c6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/c5675c6574cd6d9c7a55fe31c657cd4752e0758f))
* disable the default behavior of useQuery automatically retrying queries. ([6feeb24](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/6feeb248c1b1cad1cc43312efb89e4aa7ee2cc47))
* error in DataInfo component where the rendered loading text was always rendered in swedish language. ([bd18bf1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/bd18bf1c4725c24abacba50da3dd9ae331658938))
* exclude deleted faq items ([a6076bd](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a6076bd34cb26a9979b2fca2c1b16e84a0afdbbb))
* expires tests ([0e1a400](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0e1a400c197cb0c7a1c6b501b2f07270dcf3c390))
* make redirectUrl and foreignUserId optional ([e28fad5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e28fad535cdd722b1be41aa3ff4ec4bb662094c9))
* merge conflict ([ae12c48](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/ae12c488beeeb31a0fc823ad4d8d49ad8db01261))
* navigation flow, align texts to figma, change 'consent' to 'sharing' ([aef84d8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/aef84d82e417e2c239adc42989fb759cf2f6fe54))
* remove explicit retry, create generic Error component, use generic throwTrpcError, rework loading and error logic in the redirect flow and navigation flow hooks, add some error texts in the locales files. ([929e00c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/929e00c705b1c840e12928f1766c6eeb30ad16a3))
* remove vitest test pool configuration, now from vitest 2.x 'forks' is default ([ef7c82a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/ef7c82a3a2c880eebb2050912626cfdd4daa759d))
* specify the exact length of the regex pattern for datatype id ([cb1d49d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/cb1d49de56d83da5c0c8269e83836914ab0f2663))
* try to fix unit test to run without error on gitlab build pipeline ([51efd14](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/51efd144ab24bc067877abed09660cfcc571ada5))
* wip ([2a0f002](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2a0f0023851e5a205b5ca62ba7775b064adc2563))

## [1.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/compare/v0.1.1...v1.0.0) (2024-06-10)


### Features

* adopt api spec v1.0.0 ([b21f73b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/b21f73b1aaa432e67e0837f52231e39c765d3ecd))
* localization ([6afbec9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/6afbec9daf305deba73caec3c492ad5e73c7c5e2))
* organization and datatype faqs are now rendered dynamically ([80540af](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/80540afc9d071c8246a7e69a2eef004a609b6e8c))
* render dynamic texts on sharing page ([329a542](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/329a542496798c268adec334cecdaa9742b59b68))


### Bug Fixes

* align with figma design ([6931e22](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/6931e22fb6aa6ac68cb51737b402635ee3d94811))
* changed storybook doc files suffix from '.stories.mdx' to '.mdx', as needed by storybook v8 ([8e88a3c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/8e88a3c865f6969b14fa3d5d13991f7fea901686))
* **deps:** update dependency openapi-fetch to v0.9.6 ([dffdad4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/dffdad41857a6ab4e6ca66d6eacbec88428a546b))
* **deps:** update dependency openapi-fetch to v0.9.7 ([8012fa8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/8012fa83cfc85da4f72ea3528fe9831b8939187d))
* **deps:** update dependency pino to v8.21.0 ([39fb090](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/39fb090db368b9a0a30111cc5a3875572dbe6370))
* **deps:** update dependency zod to v3.23.5 ([2813b75](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2813b757d374b8a41f360fd95b3614b670e63586))
* **deps:** update dependency zod to v3.23.6 ([adc1df7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/adc1df7d2953bc2460dcbfba814e57fc95590fa1))
* **deps:** update dependency zod to v3.23.7 ([e5c23cc](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e5c23cc4530280702d8f9e77268a2f7ee93ec219))
* **deps:** update dependency zod to v3.23.8 ([cd90686](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/cd906868795fbcc7e548c43405bea585e1580cf0))
* display error technical problem when preview data could not be fetched ([116a9f4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/116a9f4586a5bd6ee821720f7ce90407e715780a))
* nested hooks can not be conditionally rendered ([5c3d2dd](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/5c3d2dde3ce1d090fde6764844de2a7e4770096b))
* only call useDataPreview once and propagate in props to components that needs the values ([095a73f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/095a73f22c9e7a3da0e6b646e04775e65be818b2))
* remove unnecessary mocks ([5701ff0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/5701ff093ced052411ac6e6b12780e7107778671))

## 0.1.1 (2024-04-29)


### Features

* Add FAQ component for UI. ([053e95b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/053e95b156cd508e495e367cf84e254b6b7831a1))
* Add X-Frame-Options and CSP header. ([945bec2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/945bec28852d02485101d107e7218f7fe48b43f9))
* Added curl to docker image to aid troubleshooting. ([511ed64](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/511ed64a6d9f2610f7c7317ccef27d73c5d29faa))
* Added logging with Pino (pisa_id redacted). ([8ef023c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/8ef023cdc78cda1f072e693b2b7f9e83c44cc303))
* adopt-api-spec-v0.12.0 ([c8087ae](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/c8087aed79720df01d48b227613de558cccafd70))
* Authenticate by header. ([0effd4e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0effd4eab4bb9360a9ef2ca4496d198e60eff461))
* Correctly handle case where person not registered. ([1b009b1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/1b009b1934ef253856311ed9004bd0fd03ec7e44))
* Endpoint for testing. ([7bb4f3c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/7bb4f3c054d5c8a078d4ad3420219665014e5035))
* Generate api client from OpenAPI spec. ([1a50337](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/1a503378464446bab6de52c4a4d82986a21d6bc6))
* implement status preview, and fix issues with consent/revoke actions ([436319b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/436319b8f72011f7db8cfcd510721e722c9ed31e))
* Improved validation for PISA utilities. ([db3f763](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/db3f763c042bda874f31e2956d1562a07fcf74c9))
* JWT endpoint url as environment variable. ([4c87546](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/4c875460d7c411d7affc83c01f52d5469047b1fe))
* Logging from the front end. ([fd9c4c1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/fd9c4c1d8cd91599ce8ac2b3502cba61e45880b9))
* Possible to log with context from front end. ([d7cf27c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d7cf27c6c650d5dd8d23c508f8334bf04f813eff))
* Require query parameter scope. ([a66bc98](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a66bc98529c0a75f793d2a3bfbfe70c8a6c923c2))
* UI feedback on form error. ([dd02321](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/dd0232186ff8d195614740c24ffa76bcf4f0526f))
* Upgrade to v0.7.1 specification. ([23aac25](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/23aac258d387c042a5a5c45bf13b9783fc0754df))
* Upgraded to v0.2 of specification. ([445aad6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/445aad68543e284ebad6b3b686b1c25396819e08))
* Upgraded to v0.3 of specification. ([8219040](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/82190401da40c2c2dbc8e4f3493a8e198ca9c1e8))
* Use cookie to get JWT. ([b435e3d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/b435e3d90dda5742ffe567ffdd9cd0a343e488d1))
* Use generated api client. ([68998a3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/68998a366a9e0b71511ab39162661b41e4e95357))
* Use of HTTP proxy. ([d19e1f1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d19e1f1aca10ea6d0088512c13429c7890cba5f5))
* use pdg api spec v0.11.0 ([ba3c92a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/ba3c92a017622ba8b5d41e3667f8d3b8e96044f2))
* Use test results in CI. ([a37d6fc](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a37d6fc1714f818e40404ff2f6ca1e430409e48e))
* Use Zod for validating responses from back end. ([a7352bd](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a7352bd6b8a3321b9d44e89a935be20276e927ef))
* Using iron-session and a middleware to handle user authentication. ([3b592de](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/3b592ded8d221896597456956865d40abdc23163))
* Using new API spec for backend calls. ([18b173c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/18b173c53df1fa32486ac4529e4ad183897795f3))
* Utility functions for PISA. ([2d71651](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2d71651ce9355962b92412c809ac1eb337120493))


### Bug Fixes

* Avoid internal server error by adding PIN if missing. ([348dd1f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/348dd1fe44b38096c4606d6a3b79f2351b0ebdf5))
* AXA logotype. ([e6633f1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/e6633f1fa8a06ac4c37c39f0255c93640dc5a504))
* Broken test. ([1f63940](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/1f63940407e209fc6b9f12d0d4f78e10d2afd4d4))
* Build. ([76575ce](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/76575ce16f668c8072e5f2dcd3398f0aea33b9c0))
* Dependency upgrades. ([a0615a1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a0615a1d95d89385faaee3b6d6852e8942a3f5dd))
* **deps:** update dependency @t3-oss/env-nextjs to ^0.9.0 ([74ae661](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/74ae661ee2c0281f18e3f3b780953fe7025ff33f))
* **deps:** update dependency openapi-fetch to ^0.9.0 ([0079ac8](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0079ac88976b8b3d1ee22fc6f7b64cc19487a5bf))
* **deps:** update dependency openapi-fetch to v0.9.2 ([25bd358](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/25bd3581828720691715a6f7568e4ee99045a87d))
* **deps:** update dependency openapi-fetch to v0.9.3 ([0fca900](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0fca900dbe6e9cde94981352579bf91cbaf28a98))
* **deps:** update dependency openapi-fetch to v0.9.4 ([6fe44da](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/6fe44daa0231526db8793412611b777ac4622872))
* **deps:** update dependency openapi-fetch to v0.9.5 ([d0d96ec](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d0d96eceba05a11d442dfa3d8e9b8575e3c244e2))
* **deps:** update dependency pino to v8.18.0 ([0008d0d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0008d0d7aac667f7c04426d57118c90e04aa37f7))
* **deps:** update dependency pino to v8.19.0 ([21ba81c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/21ba81ccd08ccf1947869e91825524bda76eaff3))
* **deps:** update dependency pino to v8.20.0 ([9bcff5c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/9bcff5c4cde8762aa2b09aa1f90f6225c46be207))
* **deps:** update dependency proxy-agent to v6.4.0 ([3e31442](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/3e31442b19b31a04f959b0001c9000738a431063))
* **deps:** update dependency zod to v3.22.5 ([606062c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/606062c648aef6e57cc09f4571dd933859d16c9f))
* **deps:** update trpc monorepo to v10.45.1 ([c16cd6d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/c16cd6db465458f0c2c9032f87f3e8cace343bc1))
* **deps:** update trpc monorepo to v10.45.2 ([f40f27c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f40f27c71c6bf423d91728612a12731e053f151f))
* Error modal show logic error. ([4e91fb1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/4e91fb10cfdcb62b8af47198de01e9078701067b))
* Error modal title and message. ([73e781d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/73e781db6df131da70dff0517c00db62329b0e6a))
* error when ServiceDetails component fails to removeChild ([ef1a9a6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/ef1a9a64162d471b0af50b770c480d4918d05b84))
* Exclamation mark! ([37fae2a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/37fae2abb0ff04fee60b1df1a82e4badcc6e9bf9))
* Flakey tests. ([5ebedff](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/5ebedff1de2e3176f88cdad1115b845236755484))
* Header name to lower case. ([7dc63e6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/7dc63e6475d3ed1c137abdd482de552d4ac4cfd7))
* ignore the generated spec.d.ts in lib/api ([2c52a97](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2c52a97472a719c565b40a0ed1c79ee29b0a6d34))
* Limit session lifetime. ([fdd59fd](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/fdd59fdcbbfe1621461d20e98657a4496f73a9e6))
* Linting that prevented build. ([0819dee](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/0819deee77236081009b2210b2bc30227818f5c5))
* Moved auth to getServerSideProps (from middleware). ([5b2fed7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/5b2fed7c7323f71315728f3adfcc4256b141a29c))
* Remove conversion to lowercase of logotype file names. ([c2b36cc](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/c2b36cc9604709fe55fdb95cbe02e3e1b2f76f75))
* Remove mockServiceWorker.js from container build. ([5f4b7f0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/5f4b7f0d18dc51b61095834096c7242d92a57d20))
* Removed env NEXT_PUBLIC_SERVER_URL. ([d37d246](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/d37d246a20d98324e12d789977532add86b65558))
* Removed react-cookie dep. ([50984b6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/50984b6c508c4349ff572bb80232ef6c2069fdf3))
* Rename of getPersonnummerFromJWT. ([2dbcf05](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/2dbcf052a0c36daa4638ea9e791fce63ef08f828))
* Revert to default time outs for playwright tests. ([1f8bad9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/1f8bad9944eb1c95552f612ad757b020c4201499))
* Tests. ([aab24df](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/aab24dfcf697bd38b0d3925c0e17c7d3907f0db9))
* Throw TRPCErrors in tRPC procedures instead of return error as data. ([a8729ed](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/a8729ed565e80b06f02a959e57c4124b01ae4db4))
* Upgrade to v0.7.2 of API. ([8af5c1f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/8af5c1fa5ea1c4bd30d62091a6277678033a8c5b))
* Use node-fetch to avoid issues with NextJS edge runtime. ([f379d97](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/f379d9778181c0e35bba46c833057bb2a327e374))
* Zod validation adjusted to expect null value for authCode. ([7cc2d73](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/commit/7cc2d7381c8344538c4d8d70b815176fb29b980c))
