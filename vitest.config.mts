import { configDefaults, defineConfig } from 'vitest/config';
import react from '@vitejs/plugin-react';
import { resolve } from 'node:path';

export default defineConfig({
  plugins: [react()],
  test: {
    alias: [{ find: "@app", replacement: resolve(__dirname, "./src") }],
    env: {
      NODE_ENV: 'test',
      SERVICE_URL: "http://localhost:4000",
      X_AUTH_KEY: 'example-x-auth-key',
      AUTH_HEADER: "Remote-User",
      LOG_LEVEL: "debug",
    },
    include: ['**/*.test.?(c|m)[jt]s?(x)'],
    exclude: [...configDefaults.exclude, '.next'],
    coverage: {
      all: true,
      include: ['src/'],
      exclude: ['**/*.stories.*', 'src/lib/api/*.d.ts', '**/*.test.ts?(x)'],
      reporter: ['text', 'html', 'cobertura']
    },
    setupFiles: "./tests/setup.ts"
  },
})
