import { ecsFormat } from '@elastic/ecs-pino-format';
import pino from 'pino';

import { getEnv } from '@app/env.mjs';

export const createLogger = (name: string) => {
  const opts = ecsFormat();
  opts.level = getEnv().LOG_LEVEL ?? 'info';
  opts.name = name;
  return pino(opts);
};
