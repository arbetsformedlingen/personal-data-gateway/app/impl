import { afterEach, describe, expect, it, vi } from 'vitest';
import { getBaseUrl, api, configFn, enabledFn } from './api';
import superjson from 'superjson';
import { httpBatchLink, loggerLink } from '@trpc/client';

vi.mock('@trpc/client', () => ({
  __esModule: true,
  httpBatchLink: vi.fn(),
  loggerLink: vi.fn(),
}));

vi.mock('@trpc/next', () => ({
  __esModule: true,
  createTRPCNext: mockCreateTRPCNext,
}));

const { mockCreateTRPCNext } = vi.hoisted(() => ({
  mockCreateTRPCNext: vi.fn(() => 'dummy-api-objekt'),
}));

describe('getBaseUrl function', () => {
  afterEach(() => {
    vi.unstubAllEnvs();
    vi.unstubAllGlobals();
  });

  it('should return a relative URL in the browser environment', () => {
    expect(getBaseUrl()).toBe('http://localhost:3000');
  });

  it('should return the Vercel URL in the server-side rendering environment', () => {
    vi.stubEnv('VERCEL_URL', 'example.vercel.app');
    expect(getBaseUrl()).toBe('https://example.vercel.app');
  });

  it('should return localhost URL in the development server-side rendering environment', () => {
    vi.stubEnv('PORT', '8000');
    expect(getBaseUrl()).toBe('http://localhost:8000');
  });

  it('should return empty string when window global', () => {
    vi.stubGlobal('window', 'dummywindow');
    expect(getBaseUrl()).toBe('');
  });
});

describe('api', () => {
  it('should call createTRPCNext with correct arguments and return api object', () => {
    expect(api).toBe('dummy-api-objekt');
    expect(mockCreateTRPCNext).toBeCalledWith({
      config: configFn,
      ssr: false,
    });
  });
});

describe('configFn', () => {
  it('should return correct object', () => {
    const mockLoggerLink = vi.mocked(loggerLink);
    const mockHttpBatchLink = vi.mocked(httpBatchLink);

    const config = configFn();

    expect(config).toBeDefined();
    expect(config.transformer).toBe(superjson);
    expect(config.links).toHaveLength(2);

    expect(mockLoggerLink).toBeCalledWith({ enabled: enabledFn });
    expect(mockHttpBatchLink).toBeCalledWith({
      url: `http://localhost:3000/api/trpc`,
    });
  });
});

describe('enabledFn', () => {
  afterEach(() => {
    vi.unstubAllEnvs();
  });

  // Log everythin in development mode
  it('should return true for development environment', () => {
    vi.stubEnv('NODE_ENV', 'development');

    const enabled = enabledFn({});

    expect(enabled).toBeTruthy();
  });

  // When in non-development mode only log errors in direction down.
  it('should return true for production environment when direction down and Error result', () => {
    vi.stubEnv('NODE_ENV', 'production');

    const enabled = enabledFn({ direction: 'down', result: Error() });

    expect(enabled).toBeTruthy();
  });

  // When in non-development mode only log errors in direction down.
  it('should return false for production environment, direction up', () => {
    vi.stubEnv('NODE_ENV', 'production');

    const enabled = enabledFn({ direction: 'up' });

    expect(enabled).toBeFalsy();
  });
});
