import { describe, expect, it, beforeEach, vi } from 'vitest';
import { createLogger } from './logger';
import type { Logger } from 'pino';

const { env } = vi.hoisted(() => ({
  env: {
    getEnv: vi.fn().mockReturnValue({ LOG_LEVEL: 'debug' }),
  },
}));

vi.mock('@app/env.mjs', () => env);

vi.mock('@elastic/ecs-pino-format', () => ({
  ecsFormat: () => ({}),
}));

const { pino } = vi.hoisted(() => ({
  pino: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));

vi.mock('pino', () => ({ default: () => pino }));

describe('logger', () => {
  describe('createLogger', () => {
    describe('log levels', () => {
      it('can instantiate a logger with level: undefined', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: undefined });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });

      it('can instantiate a logger with level: "debug"', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: 'debug' });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });

      it('can instantiate a logger with level: "info"', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: 'info' });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });

      it('can instantiate a logger with level: "warn"', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: 'warn' });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });

      it('can instantiate a logger with level: "error"', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: 'error' });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });

      it('can instantiate a logger with level: "fatal"', () => {
        env.getEnv.mockReturnValueOnce({ LOG_LEVEL: 'fatal' });
        const logger = createLogger('Test logger');
        expect(logger).toBeDefined();
      });
    });

    describe('logger instance', () => {
      let logger: Logger<never>;

      beforeEach(() => {
        vi.clearAllMocks();
        logger = createLogger('Test logger');
      });

      it('can log debug messages', () => {
        logger.debug('Debug');
        expect(pino.debug).toHaveBeenCalledTimes(1);
        expect(pino.debug).toHaveBeenCalledWith('Debug');
        expect(pino.info).not.toHaveBeenCalled();
        expect(pino.warn).not.toHaveBeenCalled();
        expect(pino.error).not.toHaveBeenCalled();
        expect(pino.fatal).not.toHaveBeenCalled();
      });

      it('can log info messages', () => {
        logger.info('Info');
        expect(pino.debug).not.toHaveBeenCalled();
        expect(pino.info).toHaveBeenCalledTimes(1);
        expect(pino.info).toHaveBeenCalledWith('Info');
        expect(pino.warn).not.toHaveBeenCalled();
        expect(pino.error).not.toHaveBeenCalled();
        expect(pino.fatal).not.toHaveBeenCalled();
      });

      it('can log warning messages', () => {
        logger.warn('Warn');
        expect(pino.debug).not.toHaveBeenCalled();
        expect(pino.info).not.toHaveBeenCalled();
        expect(pino.warn).toHaveBeenCalledTimes(1);
        expect(pino.warn).toHaveBeenCalledWith('Warn');
        expect(pino.error).not.toHaveBeenCalled();
        expect(pino.fatal).not.toHaveBeenCalled();
      });

      it('can log error messages', () => {
        logger.error('Error');
        expect(pino.debug).not.toHaveBeenCalled();
        expect(pino.info).not.toHaveBeenCalled();
        expect(pino.warn).not.toHaveBeenCalled();
        expect(pino.error).toHaveBeenCalledTimes(1);
        expect(pino.error).toHaveBeenCalledWith('Error');
        expect(pino.fatal).not.toHaveBeenCalled();
      });

      it('can log fatal messages', () => {
        logger.fatal('Fatal');
        expect(pino.debug).not.toHaveBeenCalled();
        expect(pino.info).not.toHaveBeenCalled();
        expect(pino.warn).not.toHaveBeenCalled();
        expect(pino.error).not.toHaveBeenCalled();
        expect(pino.fatal).toHaveBeenCalledTimes(1);
        expect(pino.fatal).toHaveBeenCalledWith('Fatal');
      });
    });
  });
});
