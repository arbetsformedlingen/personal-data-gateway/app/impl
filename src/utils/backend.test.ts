import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import {
  getClient,
  getClientRedirects,
  createSharing,
  getDatasource,
  getISODateTime,
  getDataAccesses,
  getDatasourceUserData,
  revokeSharing,
  getUserSharing,
  getUserSharings,
  getOrganization,
  getDatatype,
  getDatatypeFaqs,
  getOrganizationFaqs,
  getDatasourceFaqs,
} from './backend';
import { clientFactory } from '@app/lib/api';

vi.mock('@app/lib/api');
vi.mock('@app/utils/logger');

describe('getISODateTime', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('should return ISO-formatted string', () => {
    const date = new Date(Date.UTC(2025, 3, 6, 12, 13, 14)); // Months are zero indexed.
    vi.setSystemTime(date);

    const result = getISODateTime();

    expect(result).toBe('2025-04-06T12:13:14Z');
  });
});

describe('getClient', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';

    const response = await getClient(clientId);

    expect(mockClient.GET).toHaveBeenCalledWith('/clients/{client-id}', {
      params: { path: { 'client-id': clientId } },
    });

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';

    const response = await getClient(clientId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getClientRedirects', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';

    const response = await getClientRedirects(clientId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/clients/{client-id}/redirects',
      {
        params: { path: { 'client-id': clientId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';

    const response = await getClientRedirects(clientId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDatasource', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datasourceId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatasource(datasourceId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/datasources/{datasource-id}',
      {
        params: { path: { 'datasource-id': datasourceId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const datasourceId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatasource(datasourceId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDatasourceFaqs', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datasourceId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatasourceFaqs(datasourceId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/datasources/{datasource-id}/faqs',
      {
        params: { path: { 'datasource-id': datasourceId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const datasourceId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatasourceFaqs(datasourceId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDatatype', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('throws FAIL when datatypeUrl does not match pattern', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datatypeUrl = 'wrongDatatypeUrl';

    await expect(() => getDatatype(datatypeUrl)).rejects.toThrowError(
      'FAIL patterna matching',
    );
  });
  it('throws FAIL when parsed datatypeId is not a uuid', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datatypeUrl =
      'https://example.com/v0/datatypes/75c74be3-c826-x84d-86a2-8d01d7943b9a';

    await expect(() => getDatatype(datatypeUrl)).rejects.toThrowError(
      'FAIL not a valid uuid',
    );
  });
  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datatypeUrl =
      'https://example.com/v0/datatypes/75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatatype(datatypeUrl);

    expect(mockClient.GET).toHaveBeenCalledWith('/datatypes/{datatype-id}', {
      params: {
        path: { 'datatype-id': '75c74be3-c826-484d-86a2-8d01d7943b9a' },
      },
    });

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const datatypeUrl =
      'https://example.com/v0/datatypes/75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatatype(datatypeUrl);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDatatypeFaqs', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datatypeId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatatypeFaqs(datatypeId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/datatypes/{datatype-id}/faqs',
      {
        params: { path: { 'datatype-id': datatypeId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const datatypeId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getDatatypeFaqs(datatypeId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});
describe('getOrganization', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const organizationId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getOrganization(organizationId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/organizations/{organization-id}',
      {
        params: { path: { 'organization-id': organizationId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const organizationId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getOrganization(organizationId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getOrganizationFaqs', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const organizationId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getOrganizationFaqs(organizationId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/organizations/{organization-id}/faqs',
      {
        params: { path: { 'organization-id': organizationId } },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const organizationId = '75c74be3-c826-484d-86a2-8d01d7943b9a';

    const response = await getOrganizationFaqs(organizationId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getUserserSharings', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const userId = '199010102383';

    const response = await getUserSharings(userId);

    expect(mockClient.GET).toHaveBeenCalledWith('/users/{user-id}/sharings', {
      params: { path: { 'user-id': userId } },
    });

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const userId = '199010102383';

    const response = await getUserSharings(userId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getUserserSharing', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const userId = '199010102383';
    const sharingId = 'sharingId';

    const response = await getUserSharing(userId, sharingId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/users/{user-id}/sharings/{sharing-id}',
      {
        params: {
          path: {
            'user-id': userId,
            'sharing-id': sharingId,
          },
        },
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const userId = '199010102383';
    const sharingId = 'sharingId';

    const response = await getUserSharing(userId, sharingId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDataAccesses', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const sharingId = 'sharingId';

    const response = await getDataAccesses(sharingId);

    expect(mockClient.GET).toHaveBeenCalledWith('/data/{data-id}/accesses', {
      params: {
        path: {
          'data-id': sharingId,
        },
      },
    });

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const sharingId = 'sharingId';

    const response = await getDataAccesses(sharingId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('getDatasourceUserData', () => {
  it('calls backend with correct params', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce('dummy-response');
    const datasourceId = '74bf6ff9-bfeb-442a-aa47-7918c4bbb5c8';
    const userId = '199910109876';

    const response = await getDatasourceUserData(datasourceId, userId);

    expect(mockClient.GET).toHaveBeenCalledWith(
      '/datasources/{datasource-id}/users/{user-id}/data',
      {
        params: { path: { 'datasource-id': datasourceId, 'user-id': userId } },
        headers: { Accept: 'text/plain' },
        parseAs: 'text',
      },
    );

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { GET: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.GET.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const datasourceId = '74bf6ff9-bfeb-442a-aa47-7918c4bbb5c8';
    const userId = '199910109876';

    const response = await getDatasourceUserData(datasourceId, userId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });
  });
});

describe('createSharing', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('calls backend with correct params and data', async () => {
    const mockClient = { POST: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.POST.mockResolvedValueOnce('dummy-response');
    const userId = '199910109876';
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';
    const datasourceId = 'datasourceId';
    const body = {
      datasourceId,
      clientId,
      public: false,
    };

    const response = await createSharing(userId, clientId, datasourceId);

    expect(mockClient.POST).toBeCalledWith('/users/{user-id}/sharings', {
      params: { path: { 'user-id': userId } },
      body,
    });

    expect(response).toEqual('dummy-response');
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { POST: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.POST.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const userId = '199910109876';
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';
    const datasourceId = 'datasourceId';

    const response = await createSharing(userId, clientId, datasourceId);

    expect(response.error).toStrictEqual({
      error: 'Internal server error',
      status: 500,
    });

    // await expect(() => createSharing(userId, clientId, kind)).rejects.toThrowError('Failed to create sharing.')
  });
});

describe('revokeSharing', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.clearAllMocks();
    vi.useRealTimers();
  });

  it('calls backend with correct params and data', async () => {
    const mockClient = { PATCH: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    const dt = new Date(Date.UTC(2030, 1, 1, 13, 14, 15));
    vi.setSystemTime(dt);

    const patchResponse = {
      _links: {
        self: {
          href: 'https://example.com/foo',
        },
      },
      sharingId: '75c74be3-c826-484d-86a2-8d01d7943b9f',
      kind: 'https://example.com/example-data.schema.json',
      userId: 199010102932,
      clientId: '20de412c-5265-4ad8-806d-8c384c15fecf',
      created: '2024-02-08T15:04:28Z',
      revoked: '2024-03-08T15:04:28Z',
    };

    mockClient.PATCH.mockResolvedValueOnce({
      data: patchResponse,
    });

    const userId = '199910109876';
    const sharingId = '75c74be3-c826-484d-86a2-8d01d7943b9f';
    const response = await revokeSharing(userId, sharingId);

    expect(mockClient.PATCH).toBeCalledWith(
      '/users/{user-id}/sharings/{sharing-id}',
      {
        params: { path: { 'user-id': userId, 'sharing-id': sharingId } },
        body: { revoked: '2030-02-01T13:14:15Z' },
      },
    );

    expect(response.data).toEqual(patchResponse);
  });

  it('gives error response when calling backend fails', async () => {
    const mockClient = { PATCH: vi.fn() };
    const mockClientFactory = vi.mocked(clientFactory, { partial: true });
    mockClientFactory.mockReturnValue(mockClient);
    mockClient.PATCH.mockResolvedValueOnce({
      error: { status: 500, error: 'Internal server error' },
    });
    const userId = '199910109876';
    const clientId = '75c74be3-c826-484d-86a2-8d01d7943b9f';

    const response = await revokeSharing(userId, clientId);

    expect(response.error).toStrictEqual({
      status: 500,
      error: 'Internal server error',
    });
  });
});
