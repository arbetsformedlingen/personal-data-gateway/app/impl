import { clientFactory } from '@app/lib/api';
import type { ParseAs } from 'openapi-fetch';
import { z } from 'zod';

export const getISODateTime = () => {
  // Return a string of the current date and time in ISO format without milliseconds.
  return new Date().toISOString().split('.')[0] + 'Z';
};

export const getClient = async (clientId: string) => {
  const url = '/clients/{client-id}';
  const options = {
    params: { path: { 'client-id': clientId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getClientRedirects = async (clientId: string) => {
  const url = '/clients/{client-id}/redirects';
  const options = {
    params: { path: { 'client-id': clientId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDatasource = async (datasourceId: string) => {
  const url = '/datasources/{datasource-id}';
  const options = {
    params: { path: { 'datasource-id': datasourceId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDatasourceFaqs = async (datasourceId: string) => {
  const url = '/datasources/{datasource-id}/faqs';
  const options = {
    params: { path: { 'datasource-id': datasourceId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getOrganization = async (organizationId: string) => {
  const url = '/organizations/{organization-id}';
  const options = {
    params: { path: { 'organization-id': organizationId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDatatype = async (url: string) => {
  const pattern = /^(.*)\/datatypes\/(.{36})$/;

  const match = pattern.exec(url);

  if (match === null) {
    throw new Error('FAIL patterna matching');
  }

  const baseUrl = match[1];
  const parsedDatatypeId = z.string().uuid().safeParse(match[2]);

  if (!parsedDatatypeId.success) {
    throw new Error('FAIL not a valid uuid');
  }

  const datatypeId = parsedDatatypeId.data;

  const options = {
    params: { path: { 'datatype-id': datatypeId } },
  };

  const client = clientFactory(baseUrl);
  return client.GET('/datatypes/{datatype-id}', options);
};

export const getUserSharings = async (userId: string) => {
  const url = '/users/{user-id}/sharings';
  const options = {
    params: { path: { 'user-id': userId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getUserSharing = async (userId: string, sharingId: string) => {
  const url = '/users/{user-id}/sharings/{sharing-id}';
  const options = {
    params: { path: { 'user-id': userId, 'sharing-id': sharingId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDataAccesses = async (sharingId: string) => {
  const url = '/data/{data-id}/accesses';
  const options = {
    params: { path: { 'data-id': sharingId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDatasourceUserData = async (
  datasourceId: string,
  userId: string,
) => {
  const url = '/datasources/{datasource-id}/users/{user-id}/data';
  const options = {
    params: {
      path: {
        'datasource-id': datasourceId,
        'user-id': userId,
      },
    },
    headers: {
      Accept: 'text/plain',
    },
    parseAs: 'text' as ParseAs,
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const createSharing = async (
  userId: string,
  clientId: string,
  datasourceId: string,
) => {
  const url = '/users/{user-id}/sharings';
  const options = {
    params: {
      path: {
        'user-id': userId,
      },
    },
    body: {
      datasourceId,
      clientId,
      public: false,
    },
  };

  const client = clientFactory();
  return client.POST(url, options);
};

export const revokeSharing = async (userId: string, sharingId: string) => {
  const url = '/users/{user-id}/sharings/{sharing-id}';
  const options = {
    params: {
      path: {
        'user-id': userId,
        'sharing-id': sharingId,
      },
    },
    body: {
      revoked: getISODateTime(),
    },
  };

  const client = clientFactory();
  return client.PATCH(url, options);
};

export const getOrganizationFaqs = async (organizationId: string) => {
  const url = '/organizations/{organization-id}/faqs';
  const options = {
    params: { path: { 'organization-id': organizationId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};

export const getDatatypeFaqs = async (datatypeId: string) => {
  const url = '/datatypes/{datatype-id}/faqs';
  const options = {
    params: { path: { 'datatype-id': datatypeId } },
  };

  const client = clientFactory();
  return client.GET(url, options);
};
