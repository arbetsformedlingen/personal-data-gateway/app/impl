/* eslint-disable @typescript-eslint/dot-notation */
import { describe, expect, it } from 'vitest';

import i18n from '@app/i18n';

describe('i18n', () => {
  describe('default export', () => {
    it('is defined', () => {
      expect(i18n).toBeDefined();
    });

    it('has the same number of properties in each locale', () => {
      let prevPropCount: number | undefined;

      Object.entries(i18n).forEach(([_locale, props]) => {
        const count = Object.keys(props).length;
        if (prevPropCount) {
          expect(count).toBe(prevPropCount);
        }
        prevPropCount = count;
      });
    });

    describe('sv locale', () => {
      it('is defined', () => {
        expect(i18n.sv).toBeDefined();
      });

      it('has "home-page-header" property', () => {
        expect(i18n.sv['home-page-header']).toBe(
          'Datadelningar som du godkänt',
        );
      });

      it('has "home-page-paragraph" property', () => {
        expect(i18n.sv['home-page-paragraph']).toBe(
          'Ta del av information du delar med dina mottagare och välj om du vill fortsätta dela.',
        );
      });

      it('has "home-page-paragraph-none" property', () => {
        expect(i18n.sv['home-page-paragraph-none']).toBe(
          'Du har för närvarande inga godkända datadelningar.',
        );
      });

      it('has "home-faq-1-title" property', () => {
        expect(i18n.sv['home-faq-1-title']).toBe(
          'Vad händer när jag avslår delning?',
        );
      });

      it('has "home-faq-2-title" property', () => {
        expect(i18n.sv['home-faq-2-title']).toBe(
          'Hur hanteras mina personuppgifter?',
        );
      });

      it('has "home-faq-1-content" property', () => {
        expect(i18n.sv['home-faq-1-content']).toBe(
          'En mottagare får bara tillgång till informationen om du har Godkänt en begäran att få hämta den. Om du väljer Avslå, eller inte väljer något alls, så får mottagaren meddelandet “Godkännande saknas” när den försöker hämta uppgiften. Du kan också välja att Avslå en tidigare Godkänd begäran, då får inte längre mottagaren tillgång till informationen. Om du ångrar ditt avslag behöver du be Mottagaren att göra en ny begäran att få hämta dina uppgifter.',
        );
      });

      it('has "home-faq-2-content" property', () => {
        expect(i18n.sv['home-faq-2-content']).toBe(
          'När du loggar in för att ta del av våra elektroniska tjänster behandlar vi dina personuppgifter på liknande vis som vid en fysisk identitetskontroll. Vi behandlar ditt namn och personnummer i samband med inloggning och signering för att kontrollera att din e-legitimation är äkta och gällande. Denna behandling sker enligt den rättsliga grunden allmänt intresse och/eller som ett led i vår myndighetsutövning. Läs mer om Arbetsförmedlingens hantering av personuppgifter.',
        );
      });

      it('has "sharing-info-datatype-header" property', () => {
        expect(i18n.sv['sharing-info-datatype-header']).toBe('INFORMATION');
      });

      it('has "sharing-info-consumer-header" property', () => {
        expect(i18n.sv['sharing-info-consumer-header']).toBe('MOTTAGARE');
      });

      it('has "sharing-info-approved" property', () => {
        expect(i18n.sv['sharing-info-approved']).toBe('Godkänd');
      });

      it('has "sharing-info-open-action" property', () => {
        expect(i18n.sv['sharing-info-open-action']).toBe('Öppna');
      });

      it('has "sharing-page-header" property', () => {
        expect(i18n.sv['sharing-page-header']).toBe(
          'Digital hämtning av uppgifter från {providerName}',
        );
      });

      it('has "sharing-approve-paragraph" property', () => {
        expect(i18n.sv['sharing-approve-paragraph']).toBe(
          'Välj hur du vill hantera begäran om att få hämta informationen om dig hos {providerName}.',
        );
      });

      it('has "sharing-change-paragraph" property', () => {
        expect(i18n.sv['sharing-change-paragraph']).toBe(
          'Här är de uppgifter du har godkänt att mottagaren får hämta. Om du inte längre vill dela dessa uppgifter kan du avslå digital hämtning.',
        );
      });

      it('has "sharing-approve-action-header" property', () => {
        expect(i18n.sv['sharing-approve-action-header']).toBe(
          'Vill du dela dina uppgifter?',
        );
      });

      it('has "dismiss-approve" property', () => {
        expect(i18n.sv['dismiss-approve']).toBe('Avslå');
      });

      it('has "confirm-change" property', () => {
        expect(i18n.sv['confirm-approve']).toBe('Godkänn');
      });

      it('has "sharing-reject-action-header" property', () => {
        expect(i18n.sv['sharing-change-action-header']).toBe(
          'Ändra ditt beslut',
        );
      });

      it('has "dismiss-change" property', () => {
        expect(i18n.sv['dismiss-change']).toBe('Tillbaka');
      });

      it('has "confirm-change" property', () => {
        expect(i18n.sv['confirm-change']).toBe('Bekräfta');
      });

      it('has "check-to-continue" property', () => {
        expect(i18n.sv['check-to-continue']).toBe(
          'Valet behöver vara markerat för att ändra beslutet.',
        );
      });

      it('has "receiver" property', () => {
        expect(i18n.sv['receiver']).toBe('Mottagare');
      });

      it('has "organization-number" property', () => {
        expect(i18n.sv['organization-number']).toBe('Organisationsnummer');
      });

      it('has "data-render-error" property', () => {
        expect(i18n.sv['data-render-error']).toBe(
          'Gick inte att rendera data.',
        );
      });

      it('has "load-data-preview-error" property', () => {
        expect(i18n.sv['load-data-preview-error']).toBe(
          'Gick inte att ladda förhandsgranskning.',
        );
      });

      it('has "loading" property', () => {
        expect(i18n.sv['loading']).toBe('Laddar');
      });

      it('has "information-that-is-fetched" property', () => {
        expect(i18n.sv['information-that-is-fetched']).toBe(
          'Information som hämtas',
        );
      });

      it('has "approve-digital-sharing" property', () => {
        expect(i18n.sv['approve-digital-sharing']).toBe(
          'Godkänn digital hämtning.',
        );
      });

      it('has "check-reject-digital-sharing" property', () => {
        expect(i18n.sv['check-reject-digital-sharing']).toBe(
          'Avslå digital hämtning. Inga uppgifter delas längre digitalt.',
        );
      });

      it('has "digital-sharing-not-possible" property', () => {
        expect(i18n.sv['digital-sharing-not-possible']).toBe(
          'Digital hämtning inte möjlig',
        );
      });

      it('has "error-invalid-client-id" property', () => {
        expect(i18n.sv['error-invalid-client-id']).toBe('Felaktigt klient ID');
      });

      it('has "error-invalid-redirect-url" property', () => {
        expect(i18n.sv['error-invalid-redirect-url']).toBe(
          'Värdet för redirectUrl saknas bland giltiga värden för given klient',
        );
      });

      it('has "error-invalid-datasource-id" property', () => {
        expect(i18n.sv['error-invalid-datasource-id']).toBe(
          'Felaktigt datasource ID',
        );
      });

      it('has "error-invalid-client-organization" property', () => {
        expect(i18n.sv['error-invalid-client-organization']).toBe(
          'Ogiltig organization för klienten',
        );
      });

      it('has "error-invalid-datasource-organization" property', () => {
        expect(i18n.sv['error-invalid-datasource-organization']).toBe(
          'Ogiltig organization för datakällan',
        );
      });

      it('has "contact-dataconsumer-for-more-info" property', () => {
        expect(i18n.sv['contact-dataconsumer-for-more-info']).toBe(
          'Kontakta datakonsumenten för mer information.',
        );
      });

      it('has "you-can-now-close-window" property', () => {
        expect(i18n.sv['you-can-now-close-window']).toBe(
          'Du kan nu stänga detta fönster.',
        );
      });

      it('has "please-try-again-later" property', () => {
        expect(i18n.sv['please-try-again-later']).toBe(
          'Vänligen försök igen senare.',
        );
      });

      it('has "generic-error" property', () => {
        expect(i18n.sv['generic-error']).toBe('Något gick fel');
      });
    });
  });

  describe('en locale', () => {
    it('is defined', () => {
      expect(i18n.en).toBeDefined();
    });

    it('has "home-page-header" property', () => {
      expect(i18n.en['home-page-header']).toBe(
        'Data sharings you have approved',
      );
    });

    it('has "home-page-paragraph" property', () => {
      expect(i18n.en['home-page-paragraph']).toBe(
        'Check information you share with the recipients and chose if you want to reject the sharing',
      );
    });

    it('has "home-page-paragraph-none" property', () => {
      expect(i18n.en['home-page-paragraph-none']).toBe(
        'You currently do not have any data sharings.',
      );
    });

    it('has "home-faq-1-title" property', () => {
      expect(i18n.en['home-faq-1-title']).toBe(
        'What happens when I decline a request?',
      );
    });

    it('has "home-faq-2-title" property', () => {
      expect(i18n.en['home-faq-2-title']).toBe(
        'How is my personal data processed?',
      );
    });

    it('has "home-faq-1-content" property', () => {
      expect(i18n.en['home-faq-1-content']).toBe(
        "When you decline someone's request to retrieve your data, the data will not be shared. The requester will receive the message “Approval missing” when they attempt to access the data. This is the same message they receive if you haven't responded to their request at all. You can also choose to deny a previously approved request, in which case the recipient will no longer have access to the information. If you change your mind about the denial, you will need to ask the recipient to submit a new request to retrieve your information.",
      );
    });

    it('has "home-faq-2-content" property', () => {
      expect(i18n.en['home-faq-2-content']).toBe(
        'When you log in to access our electronic services, we process your personal data in a manner similar to a physical identity check. We process your name and personal identification number during login and signing to verify that your e-ID is valid and authentic. This processing is based on the legal grounds of public interest and/or as part of our official duties. Read more about how Arbetsförmedlingen process personal data.',
      );
    });

    it('has "sharing-info-datatype-header" property', () => {
      expect(i18n.en['sharing-info-datatype-header']).toBe('INFORMATION');
    });

    it('has "sharing-info-consumer-header" property', () => {
      expect(i18n.en['sharing-info-consumer-header']).toBe('RECIPIENT');
    });

    it('has "sharing-info-approved" property', () => {
      expect(i18n.en['sharing-info-approved']).toBe('Approved');
    });

    it('has "sharing-info-open-action" property', () => {
      expect(i18n.en['sharing-info-open-action']).toBe('Open');
    });

    it('has "sharing-page-header" property', () => {
      expect(i18n.en['sharing-page-header']).toBe(
        'Digital sharing of information from {providerName}',
      );
    });

    it('has "sharing-approve-paragraph" property', () => {
      expect(i18n.en['sharing-approve-paragraph']).toBe(
        'The recipient has requested to fetch information about you at {providerName}.',
      );
    });

    it('has "sharing-change-paragraph" property', () => {
      expect(i18n.en['sharing-change-paragraph']).toBe(
        'You chose to share this information with the recipient. You can change your decision, so that information no longer will be shared with the recipient.',
      );
    });

    it('has "sharing-approve-action-header" property', () => {
      expect(i18n.en['sharing-approve-action-header']).toBe(
        'Do you want to share your information?',
      );
    });

    it('has "dismiss-approve" property', () => {
      expect(i18n.en['dismiss-approve']).toBe('Reject');
    });

    it('has "approve" property', () => {
      expect(i18n.en['confirm-approve']).toBe('Approve');
    });

    it('has "sharing-reject-action-header" property', () => {
      expect(i18n.en['sharing-change-action-header']).toBe(
        'Change your decision',
      );
    });

    it('has "dismiss-change" property', () => {
      expect(i18n.en['dismiss-change']).toBe('Back');
    });

    it('has "confirm-change" property', () => {
      expect(i18n.en['confirm-change']).toBe('Confirm');
    });

    it('has "organization-number" property', () => {
      expect(i18n.en['organization-number']).toBe('Organization number');
    });

    it('has "data-render-error" property', () => {
      expect(i18n.en['data-render-error']).toBe('Could not render data');
    });

    it('has "load-data-preview-error" property', () => {
      expect(i18n.en['load-data-preview-error']).toBe(
        'Could not load preview.',
      );
    });

    it('has "loading" property', () => {
      expect(i18n.en['loading']).toBe('Loading');
    });

    it('has "information-that-is-fetched" property', () => {
      expect(i18n.en['information-that-is-fetched']).toBe(
        'Information that is fetched',
      );
    });

    it('has "approve-digital-sharing" property', () => {
      expect(i18n.en['approve-digital-sharing']).toBe(
        'Approve digital sharing.',
      );
    });

    it('has "check-to-continue" property', () => {
      expect(i18n.en['check-to-continue']).toBe(
        'The box has to be checked to change your approved sharing.',
      );
    });

    it('has "digital-sharing-not-possible" property', () => {
      expect(i18n.en['digital-sharing-not-possible']).toBe(
        'Digital sharing not possible',
      );
    });

    it('has "error-invalid-client-id" property', () => {
      expect(i18n.en['error-invalid-client-id']).toBe('Invalid client ID');
    });

    it('has "error-invalid-redirect-url" property', () => {
      expect(i18n.en['error-invalid-redirect-url']).toBe(
        'The given redirectUrl value is not among permitted redirect URL values for the given client',
      );
    });

    it('has "error-invalid-datasource-id" property', () => {
      expect(i18n.en['error-invalid-datasource-id']).toBe(
        'Invalid datasource ID',
      );
    });

    it('has "error-invalid-client-organization" property', () => {
      expect(i18n.en['error-invalid-client-organization']).toBe(
        'Invalid client organization detected',
      );
    });

    it('has "error-invalid-datasource-organization" property', () => {
      expect(i18n.en['error-invalid-datasource-organization']).toBe(
        'Invalid datasource organization detected',
      );
    });

    it('has "contact-dataconsumer-for-more-info" property', () => {
      expect(i18n.en['contact-dataconsumer-for-more-info']).toBe(
        'Contact data consumer for more information.',
      );
    });

    it('has "you-can-now-close-window" property', () => {
      expect(i18n.en['you-can-now-close-window']).toBe(
        'You may now close this window.',
      );
    });

    it('has "please-try-again-later" property', () => {
      expect(i18n.en['please-try-again-later']).toBe('Please try again later.');
    });

    it('has "generic-error" property', () => {
      expect(i18n.en['generic-error']).toBe('Something went wrong');
    });
  });
});
