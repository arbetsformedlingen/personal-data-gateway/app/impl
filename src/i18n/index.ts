import en from '@app/i18n/locales/en.json';
import sv from '@app/i18n/locales/sv.json';

const messages = { en, sv };

export default messages;
