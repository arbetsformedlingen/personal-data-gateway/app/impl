import type { Middleware, MiddlewareCallbackParams } from 'openapi-fetch';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { clientAuthMiddlewareFactory } from './clientAuthMiddleware';

describe('clientAuthMiddleware', () => {
  describe('clientAuthMiddlewareFactory', () => {
    let clientAuthMiddleware: Middleware;

    beforeEach(() => {
      clientAuthMiddleware = clientAuthMiddlewareFactory('example-x-auth-key');
    });

    it('instantiates an object with "onRequest" method', () => {
      expect(clientAuthMiddleware.onRequest).toBeDefined();
    });

    it('instantiates an object with "onResponse" method', () => {
      expect(clientAuthMiddleware.onResponse).toBeDefined();
    });

    describe('onRequest', () => {
      it('sets header "X-Auth-Key" on request', async () => {
        // Arrange
        const mockSet = vi.fn();
        const request = { headers: { set: mockSet } } as unknown as Request;
        const middlewareCallbackParams = {
          request,
        } as MiddlewareCallbackParams;

        // Act
        const result = await clientAuthMiddleware.onRequest!(
          middlewareCallbackParams,
        );

        // Assert
        expect(mockSet).toHaveBeenCalledWith(
          'X-Auth-Key',
          'example-x-auth-key',
        );
        expect(result).toBe(request);
      });
    });

    describe('onResponse', () => {
      it('simply returns the passed response argument', async () => {
        // Arrange
        const response = {} as unknown as Response;
        const middlewareCallbackParams = {
          response,
        } as MiddlewareCallbackParams & { response: Response };

        // Act
        const result = await clientAuthMiddleware.onResponse!(
          middlewareCallbackParams,
        );

        // Assert
        expect(result).toBe(response);
      });
    });
  });
});
