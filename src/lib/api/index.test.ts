import type { Middleware } from 'openapi-fetch';
import { describe, expect, it, vi } from 'vitest';
import { clientFactory } from '.';
import createClient from 'openapi-fetch';
import { clientAuthMiddlewareFactory } from './clientAuthMiddleware';

vi.mock('@app/env.mjs', () => ({
  env: {
    SERVICE_URL: 'example-service-url',
    X_AUTH_KEY: 'example-x-auth-key',
  },
}));
vi.mock('openapi-fetch');
vi.mock('./clientAuthMiddleware');

describe('index', () => {
  it('should provide a client', () => {
    // Arrange
    const mockClientAuthMiddlewareFactory = vi.mocked(
      clientAuthMiddlewareFactory,
      { partial: true },
    );
    const mockClientAuthMiddleware = vi.fn() as unknown as Middleware;
    mockClientAuthMiddlewareFactory.mockReturnValueOnce(
      mockClientAuthMiddleware,
    );
    const mockCreateClient = vi.mocked(createClient, { partial: true });
    const mockClient = { use: vi.fn() };
    mockCreateClient.mockReturnValueOnce(mockClient);

    // Act
    const client = clientFactory();

    // Assert
    expect(createClient).toHaveBeenCalledWith({
      baseUrl: 'example-service-url',
    });
    expect(mockClientAuthMiddlewareFactory).toHaveBeenCalledWith(
      'example-x-auth-key',
    );
    expect(mockClient.use).toHaveBeenCalledWith(mockClientAuthMiddleware);
    expect(client).toBe(mockClient);
  });
});
