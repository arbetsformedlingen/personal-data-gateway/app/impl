import createClient from 'openapi-fetch';
import type { paths } from '@dist/spec';
import { env } from '@app/env.mjs';
import { clientAuthMiddlewareFactory } from './clientAuthMiddleware';

export const clientFactory = (baseUrl?: string) => {
  const client = createClient<paths>({ baseUrl: baseUrl ?? env.SERVICE_URL });
  client.use(clientAuthMiddlewareFactory(env.X_AUTH_KEY));
  return client;
};
