/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/require-await */
import type { Middleware } from 'openapi-fetch';

export const clientAuthMiddlewareFactory = (xAuthKey: string): Middleware => {
  return {
    async onRequest({ request }) {
      request.headers.set('X-Auth-Key', xAuthKey);
      return request;
    },
    async onResponse({ response }) {
      return response;
    },
  };
};
