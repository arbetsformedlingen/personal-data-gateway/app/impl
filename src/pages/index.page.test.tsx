// @vitest-environment jsdom
import React from 'react';
import { render } from '@testing-library/react';
import { vi, describe, expect, it } from 'vitest';
import IndexPage from './index.page';

const { env } = vi.hoisted(() => ({
  env: {
    getEnv: vi.fn().mockReturnValue({
      MAINTENANCE_MODE: undefined,
    }),
  },
}));

vi.mock('@app/env.mjs', () => env);

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

describe('Index Page', () => {
  it('renders maintenance screen when NEXT_PUBLIC_MAINTENANCE_MODE is "true"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({ NEXT_PUBLIC_MAINTENANCE_MODE: 'true' });

    // Act
    const { queryByTestId } = render(<IndexPage />);

    // Assert
    expect(queryByTestId('maintenance-page')).toBeInTheDocument();
  });

  it('renders the Index component when NEXT_PUBLIC_MAINTENANCE_MODE is "false"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({ NEXT_PUBLIC_MAINTENANCE_MODE: 'false' });

    // Act
    const { queryByTestId } = render(<IndexPage />);

    // Assert
    expect(queryByTestId('maintenance-page')).not.toBeInTheDocument();
  });
});
