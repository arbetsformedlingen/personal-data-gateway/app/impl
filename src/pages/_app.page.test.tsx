// @vitest-environment jsdom
import React from 'react';
import { describe, expect, it, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import { MyApp } from '@app/pages/_app.page';

const { mockUseRouter } = vi.hoisted(() => ({
  mockUseRouter: vi.fn(),
}));

vi.mock('next/router', () => ({
  useRouter: mockUseRouter,
}));

describe('MyApp', () => {
  it('renders without crashing', () => {
    mockUseRouter.mockReturnValue({
      locale: 'sv',
      defaultLocale: 'sv',
    });

    const DummyComponent: React.FC = () => <p data-testid="dummy">Dummy</p>;

    render(
      <MyApp Component={DummyComponent} pageProps={{}} router={undefined} />,
    );

    const dummy = screen.queryByTestId('dummy');
    expect(dummy).toBeInTheDocument();
  });
});
