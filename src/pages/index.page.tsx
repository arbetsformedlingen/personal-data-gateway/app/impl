import Maintenance from '@app/components/Maintenance';
import { getEnv } from '@app/env.mjs';
import dynamic from 'next/dynamic';
const Index = dynamic(() => import('@app/components/Index'), { ssr: false });

export default function IndexPage() {
  const { NEXT_PUBLIC_MAINTENANCE_MODE } = getEnv();

  if (NEXT_PUBLIC_MAINTENANCE_MODE === 'true') {
    return <Maintenance />;
  }

  return <Index />;
}
