/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { afterEach, describe, expect, it, vi } from 'vitest';
import { apiHandler, errorHandler } from './[trpc].page';
import { type NextApiRequest } from 'next';

const { env } = vi.hoisted(() => ({
  env: {
    getEnv: vi.fn().mockReturnValue({ NODE_ENV: 'development' }),
  },
}));

vi.mock('@app/env.mjs', () => env);

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));
vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

const { mockCreateNextApiHandler, createAppRouterMock } = vi.hoisted(() => ({
  mockCreateNextApiHandler: vi.fn(() => 'Fake API Handler'),
  createAppRouterMock: vi.fn(() => 'FAKE App Router'),
}));

vi.mock('@trpc/server/adapters/next', () => ({
  createNextApiHandler: mockCreateNextApiHandler,
}));

vi.mock('@app/server/api/root', () => ({
  createAppRouter: createAppRouterMock,
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCContext: 'FAKE TRPCContext',
}));

const consoleErrorMock = vi.fn();

vi.stubGlobal('console', {
  log: vi.fn(),
  error: consoleErrorMock,
});

describe('API Handler', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('Should be created when in development env', () => {
    env.getEnv.mockReturnValue({ NODE_ENV: 'development' });
    const actualApiHandler = apiHandler();
    expect(actualApiHandler).toBe('Fake API Handler');
    expect(mockCreateNextApiHandler).toBeCalledWith({
      createContext: 'FAKE TRPCContext',
      onError: expect.any(Function),
      router: 'FAKE App Router',
    });
  });
});

describe('errorHandler', () => {
  afterEach(() => {
    vi.clearAllMocks();
  });

  it('should log error message with path present', () => {
    env.getEnv.mockReturnValue({ NODE_ENV: 'development' });
    errorHandler({
      path: '/some/error/path',
      error: {
        message: 'Some nasty error.',
        code: 'BAD_REQUEST',
        name: 'TestErrorName',
      },
      type: 'query',
      req: {} as NextApiRequest,
      input: {},
      ctx: {},
    });

    expect(logger.error).toBeCalledWith(
      '❌ tRPC failed on /some/error/path: Some nasty error.',
    );
  });

  it('should log error message without no path present', () => {
    env.getEnv.mockReturnValue({ NODE_ENV: 'development' });
    errorHandler({
      path: undefined,
      error: {
        message: 'Some nasty error.',
        code: 'BAD_REQUEST',
        name: 'TestErrorName',
      },
      type: 'query',
      req: {} as NextApiRequest,
      input: {},
      ctx: {},
    });

    expect(logger.error).toBeCalledWith(
      '❌ tRPC failed on <no-path>: Some nasty error.',
    );
  });
});
