import { createNextApiHandler } from '@trpc/server/adapters/next';
import { createTRPCContext } from '@app/server/api/trpc';
import { createAppRouter } from '@app/server/api/root';
import { getEnv } from '@app/env.mjs';
import { type AnyRouter } from '@trpc/server';
import { type NextApiRequest } from 'next';
import { type OnErrorFunction } from 'node_modules/@trpc/server/dist/internals/types';
import { createLogger } from '@app/utils/logger';

const logger = createLogger('pages/api/trpc');

export const errorHandler: OnErrorFunction<AnyRouter, NextApiRequest> = ({
  error,
  path,
}) => {
  if (getEnv().NODE_ENV === 'development') {
    logger.error(`❌ tRPC failed on ${path ?? '<no-path>'}: ${error.message}`);
  }
};

// export API handler
export const apiHandler = () => {
  return createNextApiHandler({
    router: createAppRouter(),
    createContext: createTRPCContext,
    onError: errorHandler,
  });
};

export default apiHandler();
