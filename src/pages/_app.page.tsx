import { type AppType } from 'next/app';
import { api } from '@app/utils/api';
import { IntlProvider } from 'react-intl';

import '@app/styles/globals.css';
import { useRouter } from 'next/router';
import messages from '@app/i18n';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

export const MyApp: AppType = ({ Component, pageProps }) => {
  const { locale, defaultLocale } = useRouter();
  const localeMessages = messages[locale as keyof typeof messages];

  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false } },
  });

  return (
    <QueryClientProvider client={queryClient}>
      <IntlProvider
        locale={locale!}
        defaultLocale={defaultLocale}
        messages={localeMessages}
      >
        <Component {...pageProps} />
      </IntlProvider>
    </QueryClientProvider>
  );
};

export default api.withTRPC(MyApp);
