// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it, test } from 'vitest';
import useRedirectFlowData from '@app/hooks/useRedirectFlowData';

import { vi } from 'vitest';

const {
  clientGetClientUseQueryMock,
  clientGetClientRedirectsUseQueryMock,
  datasourceGetDatasourcetUseQueryMock,
  datasourceGetDatasourceUserDataUseQueryMock,
  organizationGetOrganizationUseQueryMock,
  sharingGetUserSharingsUseQueryMock,
  sharingGetDataAccessesUseQueryMock,
} = vi.hoisted(() => ({
  clientGetClientUseQueryMock: vi.fn(),
  clientGetClientRedirectsUseQueryMock: vi.fn(),
  datasourceGetDatasourcetUseQueryMock: vi.fn(),
  datasourceGetDatasourceUserDataUseQueryMock: vi.fn(),
  organizationGetOrganizationUseQueryMock: vi.fn(),
  sharingGetUserSharingsUseQueryMock: vi.fn(),
  sharingGetDataAccessesUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    client: {
      getClient: { useQuery: clientGetClientUseQueryMock },
      getClientRedirects: { useQuery: clientGetClientRedirectsUseQueryMock },
    },
    datasource: {
      getDatasource: { useQuery: datasourceGetDatasourcetUseQueryMock },
      getDatasourceUserData: {
        useQuery: datasourceGetDatasourceUserDataUseQueryMock,
      },
    },
    organization: {
      getOrganization: { useQuery: organizationGetOrganizationUseQueryMock },
    },
    sharing: {
      getUserSharings: { useQuery: sharingGetUserSharingsUseQueryMock },
      getDataAccesses: { useQuery: sharingGetDataAccessesUseQueryMock },
    },
  },
}));

describe('useRedirectFlowData handles api isLoading', () => {
  test.each([
    [true, false, false, false, false, false, undefined, true],
    [false, true, false, false, false, false, undefined, true],
    [false, false, true, false, false, false, undefined, true],
    [false, false, false, true, false, false, undefined, true],
    [false, false, false, false, true, false, undefined, true],
    [false, false, false, false, false, true, 'sharingId', true],
  ])(
    ' [isLoadingClient=%s, isLoadingRedirect=%s, isLoadingDatasource=%s, isLoadingOrganization=%s, isLoadingSharing=%s, isLoadingDataAccess=%s, sharingId=%s] returns expected isLoading=%s',
    (
      isLoadingClient,
      isLoadingRedirect,
      isLoadingDatasource,
      isLoadingOrganization,
      isLoadingSharing,
      isLoadingDataAccess,
      sharingId,
      expectLoading,
    ) => {
      clientGetClientUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingClient,
        isError: false,
      }));
      clientGetClientRedirectsUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingRedirect,
        isError: false,
      }));
      datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingDatasource,
        isError: false,
      }));
      organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingOrganization,
        isError: false,
      }));
      sharingGetUserSharingsUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingSharing,
        isError: false,
        data: [{ sharingId: sharingId }],
      }));
      sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingDataAccess,
        isError: false,
      }));
      datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
        isLoading: true,
        isError: false,
      }));

      const { result } = renderHook(() =>
        useRedirectFlowData('clientId', 'datsourceId'),
      );
      expect(result.current.isLoading).toEqual(expectLoading);
    },
  );
});

describe('useRedirectFlowData', () => {
  it('returns unavailable for legal reasons', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'clientName' },
    }));
    clientGetClientRedirectsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ redirect: 'redirectUrl' }],
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { orgNr: 'orgNr' },
    }));
    sharingGetUserSharingsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [],
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { status: 451, message: 'statusMessage' },
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [],
    }));

    const { result } = renderHook(() =>
      useRedirectFlowData('clientId', 'datsourceId'),
    );
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.error?.clientError).toBeFalsy();
    expect(result.current.data?.unavailableForLegalReasons).toEqual(true);
    expect(result.current.data?.unavailableForLegalReasonsMessage).toEqual(
      'statusMessage',
    );
  });
});

describe('useRedirectFlowData', () => {
  it('returns sharing information', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'clientName' },
    }));
    clientGetClientRedirectsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ redirect: 'redirectUrl' }],
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { orgNr: 'orgNr' },
    }));
    sharingGetUserSharingsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ created: 'sharingCreated' }],
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: true,
      isError: false,
      data: { status: 451, message: 'statusMessage' },
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [],
    }));

    const { result } = renderHook(() =>
      useRedirectFlowData('clientId', 'datsourceId'),
    );
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.error?.clientError).toBeFalsy();
    expect(result.current.data?.sharingCreated).toEqual('sharingCreated');
  });
});

describe('useRedirectFlowData', () => {
  it('returns last accessed information', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'clientName' },
    }));
    clientGetClientRedirectsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ redirect: 'redirectUrl' }],
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { orgNr: 'orgNr' },
    }));
    sharingGetUserSharingsUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ created: 'sharingCreated' }],
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: true,
      isError: false,
      data: { status: 451, message: 'statusMessage' },
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ accessed: '2024-03-12' }, { accessed: '2024-03-13' }],
    }));

    const { result } = renderHook(() =>
      useRedirectFlowData('clientId', 'datsourceId'),
    );
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.error?.clientError).toBeFalsy();
    expect(result.current.data?.sharingCreated).toEqual('sharingCreated');
    expect(result.current.data?.sharingLastAccessed).toEqual('2024-03-13');
  });
});
