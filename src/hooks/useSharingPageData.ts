import { type components } from '@dist/spec';
import { useIntl } from 'react-intl';
import { isActive } from '@app/components/util';
import useDatasourceFaqs from '@app/hooks/useDatasourceFaqs';

type Props = {
  providerName: string;
  sharing?: components['schemas']['GetUserSharingResponse'];
  datasourceId: string;
};

function useSharingPageData({ providerName, sharing, datasourceId }: Props) {
  const intl = useIntl();
  const created = sharing?.created;
  const revoked = sharing?.revoked;
  const expires = sharing?.expires;
  const active = isActive(created, revoked, expires);

  const variant: 'approve' | 'change' = active ? 'change' : 'approve';
  const { faqItems } = useDatasourceFaqs({ datasourceId });

  const header = intl.formatMessage(
    { id: 'sharing-page-header' },
    { providerName },
  );

  const paragraph = intl.formatMessage(
    { id: `sharing-${variant}-paragraph` },
    { providerName },
  );

  return {
    header,
    paragraph,
    created,
    revoked,
    expires,
    active,
    faqItems,
    variant,
  };
}

export default useSharingPageData;
