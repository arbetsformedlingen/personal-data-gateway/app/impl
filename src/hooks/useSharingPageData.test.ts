import { beforeEach, describe, expect, it, vi } from 'vitest';
import useSharingPageData from './useSharingPageData';
import { type IntlShape, useIntl } from 'react-intl';
import { isActive } from '@app/components/util';
import { type components } from '@dist/spec';
import useDatasourceFaqs from '@app/hooks/useDatasourceFaqs';

vi.mock('react-intl');
vi.mock('@app/components/util');
vi.mock('@app/hooks/useDatasourceFaqs');

describe('useSharingPageData', () => {
  const formatMessageMock = vi.fn(({ id }: { id: string }) => {
    return `example-${id}`;
  });

  beforeEach(() => {
    vi.mocked(useIntl).mockReturnValue({
      formatMessage: formatMessageMock,
    } as unknown as IntlShape);

    vi.mocked(useDatasourceFaqs).mockReturnValue({
      faqItems: [],
    });
  });

  it('returns a "header"-property with value "example-sharing-page-header"', () => {
    vi.mocked(isActive).mockReturnValue(false);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).header,
    ).toBe('example-sharing-page-header');
  });

  it('returns a "paragraph"-property with value "example-sharing-approve-paragraph" when prop "variant" is set to "approve"', () => {
    vi.mocked(isActive).mockReturnValue(false);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).paragraph,
    ).toBe('example-sharing-approve-paragraph');
  });

  it('returns a "paragraph"-property with value "example-sharing-change-paragraph" when prop "variant" is set to "change"', () => {
    vi.mocked(isActive).mockReturnValue(true);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).paragraph,
    ).toBe('example-sharing-change-paragraph');
  });

  it('returns a "expires"-property with value "expires"', () => {
    vi.mocked(isActive).mockReturnValue(false);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).expires,
    ).toBe('expires');
  });

  it('returns a "revoked"-property with value "revoked"', () => {
    vi.mocked(isActive).mockReturnValue(false);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).revoked,
    ).toBe('revoked');
  });

  it('returns a "active"-property with value "true"', () => {
    vi.mocked(isActive).mockReturnValue(true);

    expect(
      useSharingPageData({
        providerName: 'providerName',
        sharing: {
          created: 'created',
          revoked: 'revoked',
          expires: 'expires',
        } as components['schemas']['GetUserSharingResponse'],
        datasourceId: 'datasourceId',
      }).active,
    ).toBeTruthy();
  });
});
