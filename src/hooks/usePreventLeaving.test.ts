// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';
import usePreventLeaving from '@app/hooks/usePreventLeaving';

describe('usePreventLeaving', () => {
  it('invokes preventDefault on events when enabled is true', () => {
    const mockPreventDefault = vi.fn();
    const addEventListenerSpy = vi.spyOn(window, 'addEventListener');
    addEventListenerSpy.mockImplementationOnce(
      (type: string, listener: EventListenerOrEventListenerObject) => {
        const evt = {
          preventDefault: mockPreventDefault,
          returnValue: 'something',
        };
        (listener as EventListener)(evt as unknown as Event);
      },
    );

    renderHook(() => usePreventLeaving(true));

    expect(addEventListenerSpy).toHaveBeenCalledWith(
      'beforeunload',
      expect.anything(),
    );
    expect(mockPreventDefault).toHaveBeenCalledTimes(1);
  });

  it('does not invoke preventDefault on events when enabled is false', () => {
    const mockPreventDefault = vi.fn();
    const addEventListenerSpy = vi.spyOn(window, 'addEventListener');
    addEventListenerSpy.mockImplementationOnce(
      (type: string, listener: EventListenerOrEventListenerObject) => {
        const evt = {
          preventDefault: mockPreventDefault,
          returnValue: 'something',
        };
        (listener as EventListener)(evt as unknown as Event);
      },
    );

    renderHook(() => usePreventLeaving(false));

    expect(addEventListenerSpy).toHaveBeenCalledWith(
      'beforeunload',
      expect.anything(),
    );
    expect(mockPreventDefault).not.toHaveBeenCalled();
  });

  it('does invoke preventDefault on events when enabled is being set', () => {
    const mockPreventDefault = vi.fn();
    const addEventListenerSpy = vi.spyOn(window, 'addEventListener');
    addEventListenerSpy.mockImplementationOnce(
      (type: string, listener: EventListenerOrEventListenerObject) => {
        const evt = {
          preventDefault: mockPreventDefault,
          returnValue: 'something',
        };
        (listener as EventListener)(evt as unknown as Event);
      },
    );

    renderHook(() => {
      const { setPreventLeaving } = usePreventLeaving(false);
      setPreventLeaving(false);
      setPreventLeaving(false);
      setPreventLeaving(true);
    });

    expect(addEventListenerSpy).toHaveBeenCalledTimes(1);
    expect(mockPreventDefault).toHaveBeenCalledTimes(1);
  });
});
