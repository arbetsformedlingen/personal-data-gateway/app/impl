// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import useUserSharings from './useUserSharings';
import { renderHook } from '@testing-library/react';

const { apiGetUserSharingsUseQueryMock } = vi.hoisted(() => ({
  apiGetUserSharingsUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    sharing: { getUserSharings: { useQuery: apiGetUserSharingsUseQueryMock } },
  },
}));

describe('useUserSharings', () => {
  it('returns data from api', () => {
    const userSharingsDataMock = vi.fn();
    apiGetUserSharingsUseQueryMock.mockImplementation((_isActive: boolean) => ({
      data: { userSharingsDataMock },
    }));

    const { result } = renderHook(() => useUserSharings(false));
    expect(result.current.userSharings).toEqual({ userSharingsDataMock });
  });

  it('returns empty array if no data from api', () => {
    apiGetUserSharingsUseQueryMock.mockImplementation((_isActive: boolean) => ({
      data: undefined,
    }));

    const { result } = renderHook(() => useUserSharings(false));
    expect(result.current.userSharings).toEqual([]);
  });
});
