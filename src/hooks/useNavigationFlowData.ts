import { api } from '@app/utils/api';
import { type components } from '@dist/spec';

const useNavigationFlowData = (sharingId: string) => {
  const userSharingQuery = api.sharing.getUserSharing.useQuery({ sharingId });

  const userSharing = userSharingQuery.data;

  const clientId = userSharing?.clientId;
  const client = api.client.getClient.useQuery(
    { clientId: clientId! },
    { enabled: clientId !== undefined },
  );

  const datasourceId = userSharing?.datasourceId;
  const datasource = api.datasource.getDatasource.useQuery(
    { datasourceId: datasourceId! },
    { enabled: datasourceId !== undefined },
  );

  const preview = api.datasource.getDatasourceUserData.useQuery(
    { datasourceId: datasourceId! },
    { enabled: datasourceId !== undefined },
  );

  const clientOrganizationId = client.data?.organizationId;
  const clientOrganization = api.organization.getOrganization.useQuery(
    { organizationId: clientOrganizationId! },
    { enabled: clientOrganizationId !== undefined },
  );

  const datasourceOrganizationId = datasource.data?.organizationId;
  const datasourceOrganization = api.organization.getOrganization.useQuery(
    { organizationId: datasourceOrganizationId! },
    { enabled: datasourceOrganizationId !== undefined },
  );

  const dataAccess = api.sharing.getDataAccesses.useQuery(
    { sharingId: sharingId },
    { enabled: sharingId !== undefined },
  );

  const accesses: components['schemas']['GetDataAccessesResponse'] =
    dataAccess.data ?? [];
  accesses.sort((a, b) => Date.parse(b.accessed) - Date.parse(a.accessed));

  const dataLastAccess = accesses[0];

  const sharingLastAccessed =
    accesses.length > 1
      ? accesses.sort(
          (a, b) => Date.parse(b.accessed) - Date.parse(a.accessed),
        )[0]?.accessed
      : accesses.length > 0
        ? accesses[0]?.accessed
        : undefined;

  const isLoading =
    client.isLoading ||
    datasource.isLoading ||
    clientOrganization.isLoading ||
    datasourceOrganization.isLoading ||
    userSharingQuery.isLoading ||
    dataAccess.isLoading;
  const clientError = client.isError;
  const datasourceError = datasource.isError;
  const clientOrganizationError = clientOrganization.isError;
  const datasourceOrganizationError = datasourceOrganization.isError;
  const userSharingError = userSharingQuery.isError;
  const dataAccessError = dataAccess.isError;
  const isError =
    clientError ||
    datasourceError ||
    clientOrganizationError ||
    datasourceOrganizationError;
  if (isLoading || isError) {
    return {
      isLoading: !isError && isLoading,
      isError,
      previewIsLoading: preview.isLoading,
      previewError: preview.isError,
      error: {
        clientError,
        datasourceError,
        clientOrganizationError,
        datasourceOrganizationError,
        userSharingError,
        dataAccessError,
      },
    };
  }

  const activeSharing = userSharing;
  const sharingCreated = activeSharing?.created;
  const sharingRevoked = activeSharing?.revoked;
  const sharingExpires = activeSharing?.expires;

  return {
    userSharing,
    dataLastAccess: dataLastAccess,
    isLoading,
    isError,
    previewIsLoading: preview.isLoading,
    previewError: preview.isError,
    error: {
      clientError,
      datasourceError,
      clientOrganizationError,
      datasourceOrganizationError,
      userSharingError,
      dataAccessError,
    },
    data: {
      consumerName: client.data!.name,
      consumerClientId: client.data!.clientId,
      consumerOrgNr: clientOrganization.data?.orgNr,
      providerName: datasourceOrganization.data?.name,
      datasourceId,
      preview: preview.data,
      sharingId,
      sharingCreated,
      sharingRevoked,
      sharingExpires,
      sharingLastAccessed,
    },
  };
};

export default useNavigationFlowData;
