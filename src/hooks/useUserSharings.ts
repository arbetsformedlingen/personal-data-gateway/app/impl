import { api } from '@app/utils/api';

export type UserSharing = {
  sharingId: string;
  clientName: string;
  datasourceName: string;
  created: string;
};

function useUserSharings(
  isActive: boolean,
  clientId?: string,
  datasourceId?: string,
) {
  const userSharings = api.sharing.getUserSharings.useQuery({
    isActive,
    clientId,
    datasourceId,
  });
  if (userSharings.data === undefined) {
    return { userSharings: [] as UserSharing[] };
  }
  return { userSharings: userSharings.data as UserSharing[] };
}

export default useUserSharings;
