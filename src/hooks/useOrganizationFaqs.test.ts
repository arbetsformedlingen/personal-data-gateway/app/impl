// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import useOrganizationFaqs from '@app/hooks/useOrganizationFaqs';

import { vi } from 'vitest';

const { apiGetOrganizationFaqsUseQueryMock } = vi.hoisted(() => ({
  apiGetOrganizationFaqsUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    organization: {
      getOrganizationFaqs: { useQuery: apiGetOrganizationFaqsUseQueryMock },
    },
  },
}));

describe('useOrganizationFaqs', () => {
  it('when organizationId given returns data from api', () => {
    const organizationFaqsDataMock = vi.fn();
    apiGetOrganizationFaqsUseQueryMock.mockImplementation(
      (_organizationId: string) => ({
        data: { organizationFaqsDataMock },
      }),
    );
    const { result } = renderHook(() => useOrganizationFaqs('organizationId'));
    expect(result.current.organizationFaqs).toEqual({
      organizationFaqsDataMock,
    });
    expect(apiGetOrganizationFaqsUseQueryMock).toBeCalledWith(
      { organizationId: 'organizationId' },
      { enabled: true },
    );
  });
  it('when organizationId undefined returns no data from api', () => {
    apiGetOrganizationFaqsUseQueryMock.mockImplementation(
      (_datatypeUrl: undefined) => ({ data: [] }),
    );
    const { result } = renderHook(() => useOrganizationFaqs(undefined));
    expect(apiGetOrganizationFaqsUseQueryMock).toHaveBeenCalledWith(
      { organizationId: undefined },
      { enabled: false },
    );
    expect(result.current.organizationFaqs).toEqual([]);
  });
});
