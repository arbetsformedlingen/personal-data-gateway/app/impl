import { type components } from '@dist/spec';
import { api } from '@app/utils/api';

const useRedirectFlowData = (clientId: string, datasourceId: string) => {
  const client = api.client.getClient.useQuery({ clientId });

  const clientRedirects = api.client.getClientRedirects.useQuery({ clientId });

  const datasource = api.datasource.getDatasource.useQuery({ datasourceId });

  const preview = api.datasource.getDatasourceUserData.useQuery({
    datasourceId,
  });

  let unavailableForLegalReasons = false;
  let unavailableForLegalReasonsMessage: string | undefined;
  if (preview !== undefined && typeof preview !== 'string') {
    const data = preview.data as components['schemas']['ErrorResponse'];
    if (data && data.status === 451) {
      unavailableForLegalReasons = true;
      unavailableForLegalReasonsMessage = data.message;
    }
  }

  const clientOrganizationId = client.data?.organizationId;
  const clientOrganization = api.organization.getOrganization.useQuery(
    { organizationId: clientOrganizationId! },
    { enabled: clientOrganizationId !== undefined },
  );

  const datasourceOrganizationId = datasource.data?.organizationId;
  const datasourceOrganization = api.organization.getOrganization.useQuery(
    { organizationId: datasourceOrganizationId! },
    { enabled: datasourceOrganizationId !== undefined },
  );

  const userSharings = api.sharing.getUserSharings.useQuery({
    clientId,
    datasourceId,
    isActive: true,
  });

  const activeSharing =
    userSharings.isLoading || userSharings.isError
      ? undefined
      : userSharings.data[0];
  const sharingId = activeSharing?.sharingId;
  const sharingCreated = activeSharing?.created;
  const sharingRevoked = activeSharing?.revoked;
  const sharingExpires = activeSharing?.expires;

  const dataAccess = api.sharing.getDataAccesses.useQuery(
    { sharingId: sharingId! },
    { enabled: sharingId !== undefined },
  );

  const accesses: components['schemas']['GetDataAccessesResponse'] =
    dataAccess.data ?? [];
  accesses.sort((a, b) => Date.parse(b.accessed) - Date.parse(a.accessed));

  const dataLastAccess = accesses[0];

  const sharingLastAccessed = dataAccess.data?.sort(
    (a, b) => Date.parse(b.accessed) - Date.parse(a.accessed),
  )[0]?.accessed;

  const isLoading =
    client.isLoading ||
    clientRedirects.isLoading ||
    datasource.isLoading ||
    clientOrganization.isLoading ||
    datasourceOrganization.isLoading ||
    userSharings.isLoading ||
    (dataAccess.isLoading && sharingId !== undefined);

  const clientError = client.isError;
  const clientRedirectsError = clientRedirects.isError;
  const datasourceError = datasource.isError;
  const clientOrganizationError = clientOrganization.isError;
  const datasourceOrganizationError = datasourceOrganization.isError;
  const userSharingsError = userSharings.isError;
  const dataAccessError = dataAccess.isError;
  const isError =
    clientError ||
    clientRedirectsError ||
    datasourceError ||
    clientOrganizationError ||
    datasourceOrganizationError;
  if (isLoading || isError) {
    return {
      isLoading: !isError && isLoading,
      isError,
      previewIsLoading: preview.isLoading,
      previewError: preview.isError,
      error: {
        clientError,
        clientRedirectsError,
        datasourceError,
        clientOrganizationError,
        datasourceOrganizationError,
        userSharingsError,
        dataAccessError,
      },
      userSharing: activeSharing,
    };
  }

  return {
    isLoading,
    isError,
    previewIsLoading: preview.isLoading,
    previewError: preview.isError,
    error: {
      clientError,
      clientRedirectsError,
      datasourceError,
      clientOrganizationError,
      datasourceOrganizationError,
      userSharingsError,
      dataAccessError,
    },
    data: {
      consumerName: client.data!.name,
      consumerClientId: client.data!.clientId,
      consumerOrgNr: clientOrganization.data?.orgNr,
      providerName: datasourceOrganization.data?.name,
      clientRedirects: clientRedirects.data?.map(
        (redirect) => redirect.redirectUrl,
      ),
      preview: preview.data,
      sharingCreated,
      sharingRevoked,
      sharingExpires,
      sharingLastAccessed,
      unavailableForLegalReasons,
      unavailableForLegalReasonsMessage,
    },
    userSharing: activeSharing,
    dataLastAccess,
  };
};

export default useRedirectFlowData;
