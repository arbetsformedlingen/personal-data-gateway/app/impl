import { api } from '@app/utils/api';

function useOrganizationFaqs(organizationId?: string) {
  const organizationFaqs = api.organization.getOrganizationFaqs.useQuery(
    { organizationId: organizationId! },
    { enabled: organizationId !== undefined },
  );
  return { organizationFaqs: organizationFaqs.data };
}

export default useOrganizationFaqs;
