import { useState } from 'react';
import type { Dispatch, SetStateAction } from 'react';
import useTargetUrl from '@app/hooks/useTargetUrl';
import usePreventLeaving from '@app/hooks/usePreventLeaving';
import type { ErrorState } from '@app/hooks/useErrorState';
import { api } from '@app/utils/api';
import { useRouter } from 'next/navigation';

export type Props = {
  clientId: string;
  datasourceId: string;
  foreignUserId?: string;
  redirectUrl?: string;
};

export enum SharingChoice {
  Unknown = 'UNKNOWN',
  Give = 'GIVE',
  Revoke = 'REVOKE',
}

export default function useSharing(
  { clientId, datasourceId, foreignUserId, redirectUrl }: Props,
  setError: Dispatch<SetStateAction<ErrorState>>,
  setValidationError: Dispatch<SetStateAction<boolean>>,
  initialState: SharingChoice,
) {
  const router = useRouter();

  const { setPreventLeaving } = usePreventLeaving(false);
  const { setTargetUrl } = useTargetUrl(undefined, setPreventLeaving);

  const [userSharing, setUserSharing] = useState<SharingChoice>(initialState);

  const approveSharing = api.sharing.approveSharing.useMutation({
    onSuccess: (data) => {
      if (data.redirectUrl) {
        setTargetUrl(data.redirectUrl);
      } else {
        router.push('/');
      }
    },
    onError: (error) => {
      setError({ show: true, message: error.message });
      setTargetUrl(undefined);
    },
  });

  const revokeSharing = api.sharing.revokeSharing.useMutation({
    onSuccess: (data) => {
      if (data.redirectUrl) {
        setTargetUrl(data.redirectUrl);
      } else {
        router.push('/');
      }
    },
    onError: (_error) => {
      setError({ show: true });
      setTargetUrl(undefined);
    },
  });

  const handleAbort = (existingSharingId?: string) => {
    if (redirectUrl) {
      let targetUrl = `${redirectUrl}?`;
      let message = 'Denied';
      if (existingSharingId) {
        targetUrl += `sharingId=${existingSharingId}&`;
        message = 'OK';
      }
      targetUrl += `state=${foreignUserId}&`;
      targetUrl += `message=${message}`;
      setTargetUrl(targetUrl);
    } else {
      router.push('/');
    }
  };

  const handleSend = () => {
    if (userSharing === SharingChoice.Unknown) {
      setValidationError(true);
    } else if (userSharing === SharingChoice.Revoke) {
      revokeSharing.mutate({
        clientId,
        datasourceId,
        foreignUserId,
        redirectUrl,
      });
    } else if (
      userSharing === SharingChoice.Give &&
      foreignUserId &&
      redirectUrl
    ) {
      approveSharing.mutate({
        clientId,
        datasourceId,
        foreignUserId,
        redirectUrl,
      });
    }
  };

  return {
    userSharing: userSharing,
    setUserSharing: setUserSharing,
    handleAbort,
    handleSend,
  };
}
