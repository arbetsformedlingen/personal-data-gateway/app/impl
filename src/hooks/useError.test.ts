// @vitest-environment jsdom
import { act, renderHook } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import useError from '@app/hooks/useError';

describe('useError', () => {
  it('returns current state when setError is called', () => {
    const { result } = renderHook(() => useError());
    expect(result.current.error).toBe(false);

    act(() => {
      result.current.setError(true);
    });
    expect(result.current.error).toBe(true);

    act(() => {
      result.current.setError(false);
    });
    expect(result.current.error).toBe(false);
  });
});
