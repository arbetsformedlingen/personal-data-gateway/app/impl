// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { type Mocked, describe, expect, it } from 'vitest';
import useDatatypeFaqs from '@app/hooks/useDatatypeFaqs';
import useDatatype from '@app/hooks/useDatatype';

import { vi } from 'vitest';
import { type components } from '@dist/spec';

const { apiGetDatatypeFaqsUseQueryMock } = vi.hoisted(() => ({
  apiGetDatatypeFaqsUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    datatype: { getDatatypeFaqs: { useQuery: apiGetDatatypeFaqsUseQueryMock } },
  },
}));

vi.mock('@app/hooks/useDatatype');
const useDatatypeMock = vi.mocked(useDatatype);

describe('useDatatypeFaqs', () => {
  it('when datatypeUrl given returns data from api', () => {
    const datatypeFaqsDataMock = vi.fn();
    apiGetDatatypeFaqsUseQueryMock.mockImplementation(
      (_datatypeUrl: string) => ({ data: { datatypeFaqsDataMock } }),
    );
    useDatatypeMock.mockReturnValue({
      datatype: { datatypeId: 'datatypeId' } as Mocked<
        components['schemas']['GetDatatypeResponse']
      >,
    });

    const { result } = renderHook(() => useDatatypeFaqs('datatypeUrl'));
    expect(result.current.datatypeFaqs).toEqual({ datatypeFaqsDataMock });
    expect(useDatatypeMock).toBeCalledWith('datatypeUrl');
    expect(apiGetDatatypeFaqsUseQueryMock).toBeCalledWith(
      { datatypeId: 'datatypeId' },
      { enabled: true },
    );
  });
  it('when datatypeUrl undefined returns no data from api', () => {
    apiGetDatatypeFaqsUseQueryMock.mockImplementation(
      (_datatypeUrl: undefined) => ({ data: [] }),
    );
    useDatatypeMock.mockReturnValue({ datatype: undefined });
    const { result } = renderHook(() => useDatatypeFaqs(undefined));
    expect(apiGetDatatypeFaqsUseQueryMock).toHaveBeenCalledWith(
      { datatypeId: undefined },
      { enabled: false },
    );
    expect(result.current.datatypeFaqs).toEqual([]);
  });
});
