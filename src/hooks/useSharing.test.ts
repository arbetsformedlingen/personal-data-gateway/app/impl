// @vitest-environment jsdom
import type { Dispatch, SetStateAction } from 'react';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { act, renderHook } from '@testing-library/react';
import { useRouter } from 'next/navigation';
import useSharing, { SharingChoice } from '@app/hooks/useSharing';
import type { ErrorState } from '@app/hooks/useErrorState';

const setErrorMock: Dispatch<SetStateAction<ErrorState>> = vi.fn();
const setValidationErrorMock: Dispatch<SetStateAction<boolean>> = vi.fn();

const setPreventLeavingMock: Dispatch<SetStateAction<boolean>> = vi.fn();

const { usePreventLeaving } = vi.hoisted(() => ({
  usePreventLeaving: vi.fn((_state) => ({
    setPreventLeaving: setPreventLeavingMock,
  })),
}));

vi.mock('./usePreventLeaving', () => ({
  default: usePreventLeaving,
}));

const setTargetUrlMock: Dispatch<SetStateAction<string | undefined>> = vi.fn();

const { useTargetUrl } = vi.hoisted(() => ({
  useTargetUrl: vi.fn((_url, _setPreventLeaving) => ({
    setTargetUrl: setTargetUrlMock,
  })),
}));

vi.mock('./useTargetUrl', () => ({
  default: useTargetUrl,
}));

vi.mock('next/navigation', () => ({ useRouter: vi.fn() }));
const pushMock: (_href: string) => void = vi.fn((_href: string) => ({}));
vi.mocked(useRouter, { partial: true }).mockReturnValue({ push: pushMock });

type Callback = {
  onSuccess: (data: { redirectUrl: string | undefined }) => void;
  onError: (error: { message: string }) => void;
};

const approveSharingMutateMock = vi.fn();
const revokeSharingMutateMock = vi.fn();

let approveSharingCallback: Callback;
let revokeSharingCallback: Callback;
const { approveSharingUseMutationMock, revokeSharingUseMutationMock } =
  vi.hoisted(() => ({
    approveSharingUseMutationMock: vi.fn((callback: Callback) => {
      approveSharingCallback = callback;
      return {
        mutate: approveSharingMutateMock,
      };
    }),
    revokeSharingUseMutationMock: vi.fn((callback: Callback) => {
      revokeSharingCallback = callback;
      return {
        mutate: revokeSharingMutateMock,
      };
    }),
  }));

vi.mock('@app/utils/api', () => ({
  api: {
    sharing: {
      approveSharing: {
        useMutation: approveSharingUseMutationMock,
      },
      revokeSharing: {
        useMutation: revokeSharingUseMutationMock,
      },
    },
  },
}));

describe('useSharing', () => {
  it('calls appropriate functions when calling approveSharing onSuccess', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    act(() => {
      approveSharingCallback.onSuccess({ redirectUrl: 'redirectUrl1' });
    });

    expect(setTargetUrlMock).toHaveBeenCalledWith('redirectUrl1');
  });

  it('calls appropriate functions when calling approveSharing onError else ...', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    act(() => {
      approveSharingCallback.onError({ message: '' });
    });
    expect(setErrorMock).toHaveBeenCalledWith({ show: true, message: '' });
    expect(setTargetUrlMock).toHaveBeenCalledWith(undefined);
  });

  it('calls appropriate functions when calling revokeSharing onSuccess', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    act(() => {
      revokeSharingCallback.onSuccess({ redirectUrl: 'redirectUrl2' });
    });

    expect(setTargetUrlMock).toHaveBeenCalledWith('redirectUrl2');
  });

  it('calls appropriate functions when calling revokeSharing onError', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    act(() => {
      revokeSharingCallback.onError({ message: 'whatever' });
    });
    expect(setErrorMock).toHaveBeenCalledWith({ show: true });
    expect(setTargetUrlMock).toHaveBeenCalledWith(undefined);
  });

  it('calls setTargetUrl when handleAbort is being called and sharingId is undefined', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    const { result } = renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    result.current.handleAbort();
    expect(setTargetUrlMock).toHaveBeenCalledWith(
      'redirectUrl?state=foreignUserId&message=Denied',
    );
  });

  it('calls setTargetUrl when handleAbort is being called and sharingId is defined', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    const { result } = renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    result.current.handleAbort('exampleSharingId');
    expect(setTargetUrlMock).toHaveBeenCalledWith(
      'redirectUrl?sharingId=exampleSharingId&state=foreignUserId&message=OK',
    );
  });

  it('calls appropriate functions when handleSend is being called', () => {
    const props = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    const { result } = renderHook(() =>
      useSharing(
        props,
        setErrorMock,
        setValidationErrorMock,
        SharingChoice.Unknown,
      ),
    );

    result.current.handleSend();
    expect(setValidationErrorMock).toHaveBeenCalledWith(true);

    act(() => {
      result.current.setUserSharing(SharingChoice.Give);
    });
    result.current.handleSend();
    expect(approveSharingMutateMock).toHaveBeenCalledWith(props);

    act(() => {
      result.current.setUserSharing(SharingChoice.Revoke);
    });
    result.current.handleSend();
    expect(revokeSharingMutateMock).toHaveBeenCalledWith(props);
  });

  describe('with undefined redirectUrl', () => {
    beforeEach(() => {
      vi.clearAllMocks();
    });

    it('approves sharing on success', () => {
      const props = {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        foreignUserId: undefined,
        redirectUrl: undefined,
      };
      const { result } = renderHook(() =>
        useSharing(
          props,
          setErrorMock,
          setValidationErrorMock,
          SharingChoice.Give,
        ),
      );

      result.current.handleSend();
      expect(setTargetUrlMock).toHaveBeenCalledTimes(0);

      act(() => {
        approveSharingCallback.onSuccess({ redirectUrl: undefined });
      });

      expect(pushMock).toHaveBeenCalledWith('/');
    });

    it('revokes sharing on success', () => {
      const props = {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        foreignUserId: undefined,
        redirectUrl: undefined,
      };
      const { result } = renderHook(() =>
        useSharing(
          props,
          setErrorMock,
          setValidationErrorMock,
          SharingChoice.Revoke,
        ),
      );

      result.current.handleSend();
      expect(setTargetUrlMock).toHaveBeenCalledTimes(0);

      act(() => {
        revokeSharingCallback.onSuccess({ redirectUrl: undefined });
      });

      expect(pushMock).toHaveBeenCalledWith('/');
    });

    it('handles abort', () => {
      const props = {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        foreignUserId: undefined,
        redirectUrl: undefined,
      };
      const { result } = renderHook(() =>
        useSharing(
          props,
          setErrorMock,
          setValidationErrorMock,
          SharingChoice.Unknown,
        ),
      );

      result.current.handleAbort();
      expect(setTargetUrlMock).toHaveBeenCalledTimes(0);
      expect(pushMock).toHaveBeenCalledWith('/');
    });
  });
});
