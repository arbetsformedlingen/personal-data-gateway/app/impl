// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import useDatasource from '@app/hooks/useDatasource';

import { vi } from 'vitest';

const { apiGetDatasourceUseQueryMock } = vi.hoisted(() => ({
  apiGetDatasourceUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    datasource: { getDatasource: { useQuery: apiGetDatasourceUseQueryMock } },
  },
}));

describe('useDatasource', () => {
  it('returns data from api', () => {
    apiGetDatasourceUseQueryMock.mockImplementation(
      (_datasourceId: string) => ({ data: 'example-data' }),
    );
    const { result } = renderHook(() => useDatasource('datasourceId'));
    expect(apiGetDatasourceUseQueryMock).toHaveBeenCalledWith(
      { datasourceId: 'datasourceId' },
      { enabled: true },
    );
    expect(result.current.datasource).toEqual('example-data');
  });
});
