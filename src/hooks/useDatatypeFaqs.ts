import { api } from '@app/utils/api';
import useDatatype from '@app/hooks/useDatatype';

function useDatatypeFaqs(datatypeUrl?: string) {
  const { datatype } = useDatatype(datatypeUrl);

  const datatypeId = datatype?.datatypeId;

  const datatypeFaqs = api.datatype.getDatatypeFaqs.useQuery(
    { datatypeId: datatypeId! },
    { enabled: datatype !== undefined },
  );
  return { datatypeFaqs: datatypeFaqs.data };
}

export default useDatatypeFaqs;
