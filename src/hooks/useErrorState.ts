import { useState } from 'react';

export type ErrorState = {
  show: boolean;
  title?: string;
  message?: string;
};

export default function useErrorState() {
  const [errorState, setErrorState] = useState<ErrorState>({
    show: false,
    title: undefined,
    message: undefined,
  });
  return { errorState, setErrorState };
}
