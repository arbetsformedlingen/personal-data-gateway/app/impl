import { beforeEach, describe, expect, it, vi } from 'vitest';
import { type components } from '@dist/spec';
import useDatasource from '@app/hooks/useDatasource';
import useDatatypeFaqs from '@app/hooks/useDatatypeFaqs';
import useOrganizationFaqs from '@app/hooks/useOrganizationFaqs';
import useFaqItems from '@app/hooks/useFaqItems';
import useDatasourceFaqs from './useDatasourceFaqs';

vi.mock('@app/hooks/useDatasource');
vi.mock('@app/hooks/useDatatypeFaqs');
vi.mock('@app/hooks/useOrganizationFaqs');
vi.mock('@app/hooks/useFaqItems');

const { apiGetDatasourceFaqsUseQueryMock } = vi.hoisted(() => ({
  apiGetDatasourceFaqsUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    datasource: {
      getDatasourceFaqs: { useQuery: apiGetDatasourceFaqsUseQueryMock },
    },
  },
}));

describe('useDatasourceFaqs', () => {
  beforeEach(() => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as components['schemas']['GetDatasourceResponse'],
    });

    vi.mocked(useDatatypeFaqs).mockReturnValue({
      datatypeFaqs: [] as components['schemas']['GetDatatypeFaqsResponse'],
    });

    vi.mocked(useOrganizationFaqs).mockReturnValue({
      organizationFaqs:
        [] as components['schemas']['GetOrganizationFaqsResponse'],
    });

    apiGetDatasourceFaqsUseQueryMock.mockReturnValue({
      data: [] as components['schemas']['GetDatasourceFaqsResponse'],
    });

    vi.mocked(useFaqItems).mockReturnValueOnce({
      faqItems: [
        {
          key: 'organization-faq-key',
          heading: 'organization-faq-heading',
          itemContent: ['organization-faq-content'],
        },
      ],
    });

    vi.mocked(useFaqItems).mockReturnValueOnce({
      faqItems: [
        {
          key: 'datasource-faq-key',
          heading: 'datasource-faq-heading',
          itemContent: ['datasource-faq-content'],
        },
      ],
    });

    vi.mocked(useFaqItems).mockReturnValueOnce({
      faqItems: [
        {
          key: 'datatype-faq-key',
          heading: 'datatype-faq-heading',
          itemContent: ['datatype-faq-content'],
        },
      ],
    });
  });

  it('returns a "faqItems"-property', () => {
    const { faqItems } = useDatasourceFaqs({
      datasourceId: 'datasourceId',
    });

    expect(faqItems).toStrictEqual([
      {
        heading: 'organization-faq-heading',
        itemContent: ['organization-faq-content'],
        key: 'organization-faq-key',
      },
      {
        heading: 'datasource-faq-heading',
        itemContent: ['datasource-faq-content'],
        key: 'datasource-faq-key',
      },
      {
        heading: 'datatype-faq-heading',
        itemContent: ['datatype-faq-content'],
        key: 'datatype-faq-key',
      },
    ]);
  });
});
