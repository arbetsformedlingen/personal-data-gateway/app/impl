import { api } from '@app/utils/api';

function useDatasource(datasourceId?: string) {
  const datasource = api.datasource.getDatasource.useQuery(
    { datasourceId: datasourceId! },
    { enabled: datasourceId !== undefined },
  );
  return { datasource: datasource.data };
}

export default useDatasource;
