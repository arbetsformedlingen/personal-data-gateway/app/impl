import { api } from '@app/utils/api';

function useDatatype(datatypeUrl?: string) {
  const datatype = api.datatype.getDatatype.useQuery(
    { datatypeUrl: datatypeUrl! },
    { enabled: datatypeUrl !== undefined },
  );
  return { datatype: datatype.data };
}

export default useDatatype;
