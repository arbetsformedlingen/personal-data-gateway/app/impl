import { type FAQItem } from '@app/components/FAQ';
import { type components } from '@dist/spec';
import { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';

export type Faqs =
  | components['schemas']['GetDatatypeFaqsResponse']
  | components['schemas']['GetDatasourceFaqsResponse']
  | components['schemas']['GetOrganizationFaqsResponse'];

function useFaqItems(faqs?: Faqs) {
  const intl = useIntl();
  const [faqItems, setFaqItems] = useState<FAQItem[]>([]);

  useEffect(() => {
    if (faqs) {
      const faqsWithRightLocale = faqs.filter(
        (faq) => faq.lang === intl.locale,
      );
      const faqsNotDeleted = faqsWithRightLocale.filter(
        (faq) => faq.deleted === undefined,
      );
      setFaqItems(
        faqsNotDeleted.map((faq) => {
          const paragraphs = faq.content.split('\\n');
          return {
            key: faq.faqId,
            heading: faq.title,
            itemContent: paragraphs,
          };
        }),
      );
    }
  }, [faqs, intl.locale]);

  return { faqItems };
}

export default useFaqItems;
