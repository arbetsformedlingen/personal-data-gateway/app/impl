// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import useDatatype from '@app/hooks/useDatatype';

import { vi } from 'vitest';

const { apiGetDatatypeUseQueryMock } = vi.hoisted(() => ({
  apiGetDatatypeUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    datatype: { getDatatype: { useQuery: apiGetDatatypeUseQueryMock } },
  },
}));

describe('useDatatype', () => {
  it('when datatypeUrl given returns data from api', () => {
    const datatypeDataMock = vi.fn();
    apiGetDatatypeUseQueryMock.mockImplementation((_datatypeUrl: string) => ({
      data: { datatypeDataMock },
    }));
    const { result } = renderHook(() => useDatatype('datatypeUrl'));
    expect(result.current.datatype).toEqual({ datatypeDataMock });
    expect(apiGetDatatypeUseQueryMock).toBeCalledWith(
      { datatypeUrl: 'datatypeUrl' },
      { enabled: true },
    );
  });
  it('when datatypeUrl undefined returns no data from api', () => {
    apiGetDatatypeUseQueryMock.mockImplementation(
      (_datatypeUrl: undefined) => ({ data: undefined }),
    );
    const { result } = renderHook(() => useDatatype(undefined));
    expect(apiGetDatatypeUseQueryMock).toHaveBeenCalledWith(
      { datatypeUrl: undefined },
      { enabled: false },
    );
    expect(result.current.datatype).toEqual(undefined);
  });
});
