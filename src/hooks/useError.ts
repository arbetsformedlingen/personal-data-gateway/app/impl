import { useState } from 'react';

export default function useError() {
  const [error, setError] = useState<boolean>(false);

  return { error, setError };
}
