import { useEffect, useRef } from 'react';

export default function usePreventLeaving(enabled: boolean) {
  const preventLeaving = useRef(enabled);

  const handleBeforeunloadEvent = (event: BeforeUnloadEvent) => {
    if (preventLeaving.current) {
      event.preventDefault();
      event.returnValue = '';
    }
  };

  // Handle event listener for beforeunload events.
  useEffect(() => {
    window.addEventListener('beforeunload', handleBeforeunloadEvent);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeunloadEvent);
    };
  }, []);

  const setPreventLeaving = (enabled: boolean) => {
    preventLeaving.current = enabled;
  };

  return { setPreventLeaving };
}
