// @vitest-environment jsdom
import { act, renderHook } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import useErrorState from './useErrorState';

describe('useErrorState', () => {
  it('returns state when setErrorState is called', () => {
    const { result } = renderHook(() => useErrorState());
    expect(result.current.errorState).toEqual({
      show: false,
      title: undefined,
      message: undefined,
    });

    act(() => {
      result.current.setErrorState({
        show: true,
        title: 'title',
        message: 'message',
      });
    });
    expect(result.current.errorState).toEqual({
      show: true,
      title: 'title',
      message: 'message',
    });

    act(() => {
      result.current.setErrorState({
        show: false,
        title: 'nothing',
        message: 'here',
      });
    });
    expect(result.current.errorState).toEqual({
      show: false,
      title: 'nothing',
      message: 'here',
    });
  });
});
