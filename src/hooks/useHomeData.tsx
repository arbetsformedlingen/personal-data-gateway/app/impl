import { useIntl } from 'react-intl';
import useUserSharings from './useUserSharings';

export type FaqItem = {
  key: string;
  heading: string;
  itemContent: string[];
};

function useHomeData() {
  const intl = useIntl();
  const { userSharings } = useUserSharings(true);

  const header = intl.formatMessage({ id: 'home-page-header' });

  const paragraph = intl.formatMessage({
    id: `home-page-paragraph${userSharings.length > 0 ? '' : '-none'}`,
  });

  const faqItems: FaqItem[] = [
    {
      key: 'key1',
      heading: intl.formatMessage({ id: 'home-faq-1-title' }),
      itemContent: [intl.formatMessage({ id: 'home-faq-1-content' })],
    },
    {
      key: 'key2',
      heading: intl.formatMessage({ id: 'home-faq-2-title' }),
      itemContent: [intl.formatMessage({ id: 'home-faq-2-content' })],
    },
  ];

  return { header, paragraph, faqItems, userSharings };
}

export default useHomeData;
