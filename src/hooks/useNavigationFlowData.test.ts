// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { describe, expect, it, test } from 'vitest';
import useNavigationFlowData from '@app/hooks/useNavigationFlowData';

import { vi } from 'vitest';

const {
  clientGetClientUseQueryMock,
  clientGetClientRedirectsUseQueryMock,
  datasourceGetDatasourcetUseQueryMock,
  datasourceGetDatasourceUserDataUseQueryMock,
  organizationGetOrganizationUseQueryMock,
  sharingGetUserSharingUseQueryMock,
  sharingGetDataAccessesUseQueryMock,
} = vi.hoisted(() => ({
  clientGetClientUseQueryMock: vi.fn(),
  clientGetClientRedirectsUseQueryMock: vi.fn(),
  datasourceGetDatasourcetUseQueryMock: vi.fn(),
  datasourceGetDatasourceUserDataUseQueryMock: vi.fn(),
  organizationGetOrganizationUseQueryMock: vi.fn(),
  sharingGetUserSharingUseQueryMock: vi.fn(),
  sharingGetDataAccessesUseQueryMock: vi.fn(),
}));

vi.mock('@app/utils/api', () => ({
  api: {
    client: {
      getClient: { useQuery: clientGetClientUseQueryMock },
      getClientRedirects: { useQuery: clientGetClientRedirectsUseQueryMock },
    },
    datasource: {
      getDatasource: { useQuery: datasourceGetDatasourcetUseQueryMock },
      getDatasourceUserData: {
        useQuery: datasourceGetDatasourceUserDataUseQueryMock,
      },
    },
    organization: {
      getOrganization: { useQuery: organizationGetOrganizationUseQueryMock },
    },
    sharing: {
      getUserSharing: { useQuery: sharingGetUserSharingUseQueryMock },
      getDataAccesses: { useQuery: sharingGetDataAccessesUseQueryMock },
    },
  },
}));

describe('useNavigationFlowData handles api isLoading', () => {
  test.each([
    [true, false, false, false, false],
    [false, true, false, false, false],
    [false, false, true, false, false],
    [false, false, false, true, false],
    [false, false, false, false, true],
  ])(
    ' [isLoadingClient=%s, isLoadingRedirect=%s, isLoadingDatasource=%s, isLoadingOrganization=%s, isLoadingSharing=%s] returns expected isLoading=true',
    (
      isLoadingClient,
      isLoadingDatasource,
      isLoadingOrganization,
      isLoadingSharing,
      isLoadingDataAccess,
    ) => {
      sharingGetUserSharingUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingSharing,
        isError: false,
        data: {},
      }));
      clientGetClientUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingClient,
        isError: false,
        data: {},
      }));
      datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingDatasource,
        isError: false,
      }));
      organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingOrganization,
        isError: false,
        data: {},
      }));
      datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
        isLoading: true,
        isError: false,
      }));
      sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
        isLoading: isLoadingDataAccess,
        isError: false,
      }));

      const { result } = renderHook(() => useNavigationFlowData('sharingId'));
      expect(result.current.isLoading).toEqual(true);
    },
  );
});

describe('useNavigationFlowData', () => {
  it('returns data without access information', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'clientOrganizationId',
      },
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'datasourceName', orgNr: 'orgNr' },
    }));
    sharingGetUserSharingUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { datasourceId: 'datasourceId', created: 'created' },
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {},
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [],
    }));

    const { result } = renderHook(() => useNavigationFlowData('sharingId'));
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.isError).toEqual(false);
    expect(result.current.data).toEqual({
      consumerClientId: 'clientId',
      consumerName: 'clientName',
      consumerOrgNr: 'orgNr',
      datasourceId: 'datasourceId',
      preview: {},
      providerName: 'datasourceName',
      sharingCreated: 'created',
      sharingId: 'sharingId',
      sharingLastAccessed: undefined,
    });
  });

  it('returns data with one access', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'clientOrganizationId',
      },
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'datasourceName', orgNr: 'orgNr' },
    }));
    sharingGetUserSharingUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { datasourceId: 'datasourceId', created: 'created' },
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {},
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [{ accessed: '2024-08-12' }],
    }));

    const { result } = renderHook(() => useNavigationFlowData('sharingId'));
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.isError).toEqual(false);
    expect(result.current.data).toEqual({
      consumerClientId: 'clientId',
      consumerName: 'clientName',
      consumerOrgNr: 'orgNr',
      datasourceId: 'datasourceId',
      preview: {},
      providerName: 'datasourceName',
      sharingCreated: 'created',
      sharingId: 'sharingId',
      sharingLastAccessed: '2024-08-12',
    });
  });

  it('returns data with last access', () => {
    clientGetClientUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'clientOrganizationId',
      },
    }));
    datasourceGetDatasourcetUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { organizationId: 'datasourceOrganizationId' },
    }));
    organizationGetOrganizationUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { name: 'datasourceName', orgNr: 'orgNr' },
    }));
    sharingGetUserSharingUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: { datasourceId: 'datasourceId', created: 'created' },
    }));
    datasourceGetDatasourceUserDataUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: {},
    }));
    sharingGetDataAccessesUseQueryMock.mockImplementation(() => ({
      isLoading: false,
      isError: false,
      data: [
        { accessed: '2024-06-02' },
        { accessed: '2024-05-15' },
        { accessed: '2024-08-12' },
      ],
    }));

    const { result } = renderHook(() => useNavigationFlowData('sharingId'));
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.isError).toEqual(false);
    expect(result.current.data).toEqual({
      consumerClientId: 'clientId',
      consumerName: 'clientName',
      consumerOrgNr: 'orgNr',
      datasourceId: 'datasourceId',
      preview: {},
      providerName: 'datasourceName',
      sharingCreated: 'created',
      sharingId: 'sharingId',
      sharingLastAccessed: '2024-08-12',
    });
  });
});
