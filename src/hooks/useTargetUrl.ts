import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

export default function useTargetUrl(
  url: string | undefined,
  setPreventLeaving: (enabled: boolean) => void,
) {
  const [targetUrl, setTargetUrl] = useState<string | undefined>(url);

  const router = useRouter();

  // Push new route when state targetUrl is set.
  useEffect(() => {
    const effect = async () => {
      if (targetUrl) {
        setPreventLeaving(false);
        await router.push(targetUrl);
      }
    };

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    effect().catch(() => {});
  }, [router, targetUrl, setPreventLeaving]);

  return { setTargetUrl };
}
