import { beforeEach, describe, expect, it, vi } from 'vitest';
import useHomeData from './useHomeData';
import useUserSharings, { type UserSharing } from './useUserSharings';

const { formatMessageMock } = vi.hoisted(() => ({
  formatMessageMock: vi.fn().mockImplementation(({ id }: { id: string }) => {
    return `example-${id}`;
  }),
}));

vi.mock('react-intl', () => ({
  useIntl: () => ({
    locale: 'sv',
    formatMessage: formatMessageMock,
  }),
}));

vi.mock('./useUserSharings');

describe('useHomeData', () => {
  describe('when nested "useUserSharings" hook returns a populated "userSharings" array', () => {
    let userSharingMock: UserSharing;

    beforeEach(() => {
      userSharingMock = {
        label: 'userSharingMock',
      } as unknown as UserSharing;

      vi.mocked(useUserSharings).mockReturnValue({
        userSharings: [userSharingMock],
      });
    });

    it('returns property "header" with value "example-home-page-header"', () => {
      expect(useHomeData().header).toBe('example-home-page-header');
    });

    it('returns property "paragraph" with value "example-home-page-paragraph"', () => {
      expect(useHomeData().paragraph).toBe('example-home-page-paragraph');
    });

    it('returns a populated "userSharings" array', () => {
      expect(useHomeData().userSharings).toStrictEqual([userSharingMock]);
    });

    it('returns populated "faqItems" array', () => {
      expect(useHomeData().faqItems).toStrictEqual([
        {
          key: 'key1',
          heading: 'example-home-faq-1-title',
          itemContent: ['example-home-faq-1-content'],
        },
        {
          key: 'key2',
          heading: 'example-home-faq-2-title',
          itemContent: ['example-home-faq-2-content'],
        },
      ]);
    });
  });

  describe('when nested "useUserSharings" hook returns an empty "userSharings" array', () => {
    beforeEach(() => {
      vi.mocked(useUserSharings).mockReturnValue({
        userSharings: [],
      });
    });

    it('returns property "header" with value "example-home-page-header"', () => {
      expect(useHomeData().header).toBe('example-home-page-header');
    });

    it('returns property "paragraph" with value "example-home-page-paragraph-none"', () => {
      expect(useHomeData().paragraph).toBe('example-home-page-paragraph-none');
    });

    it('returns an empty "userSharings" array', () => {
      expect(useHomeData().userSharings).toStrictEqual([]);
    });

    it('returns a populated "faqItems" array', () => {
      expect(useHomeData().faqItems).toStrictEqual([
        {
          key: 'key1',
          heading: 'example-home-faq-1-title',
          itemContent: ['example-home-faq-1-content'],
        },
        {
          key: 'key2',
          heading: 'example-home-faq-2-title',
          itemContent: ['example-home-faq-2-content'],
        },
      ]);
    });
  });
});
