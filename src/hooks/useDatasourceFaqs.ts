import useDatasource from './useDatasource';
import useDatatypeFaqs from './useDatatypeFaqs';
import useFaqItems from './useFaqItems';
import useOrganizationFaqs from './useOrganizationFaqs';
import { api } from '@app/utils/api';

type Props = {
  datasourceId: string;
};

function useDatasourceFaqs({ datasourceId }: Props) {
  const { datasource } = useDatasource(datasourceId);
  const { datatypeFaqs } = useDatatypeFaqs(datasource?.datatypeUrl);
  const { organizationFaqs } = useOrganizationFaqs(datasource?.organizationId);

  const { data: datasourceFaqs } = api.datasource.getDatasourceFaqs.useQuery(
    { datasourceId },
    { enabled: datasource !== undefined },
  );

  const { faqItems: organizationFaqItems } = useFaqItems(organizationFaqs);
  const { faqItems: datasourceFaqItems } = useFaqItems(datasourceFaqs);
  const { faqItems: datatypeFaqItems } = useFaqItems(datatypeFaqs);

  const faqItems = [
    ...organizationFaqItems,
    ...datasourceFaqItems,
    ...datatypeFaqItems,
  ];

  return { faqItems };
}

export default useDatasourceFaqs;
