// @vitest-environment jsdom
import { renderHook } from '@testing-library/react';
import { type Mocked, describe, expect, it, beforeEach, vi } from 'vitest';
import useFaqItems, { type Faqs } from './useFaqItems';
import { type components } from '@dist/spec';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    locale: 'sv',
  }),
}));

describe('useFaqItems', () => {
  it('returns empty array if called with empty array', () => {
    const { result } = renderHook(() => useFaqItems());
    expect(result.current.faqItems).toEqual([]);
  });

  describe('mapping of input array to faqItems', () => {
    let faqs: Faqs = [];
    beforeEach(() => {
      faqs = [
        {
          lang: 'sv',
          title: 'title',
          faqId: 'faqId',
          content: 'one line of text',
        },
      ] as Mocked<components['schemas']['GetDatatypeFaqsResponse']>;
    });

    it('maps content to itemContent', () => {
      const { result } = renderHook(() => useFaqItems(faqs));
      expect(result.current.faqItems[0]?.itemContent.length).toEqual(1);
      expect(result.current.faqItems[0]?.itemContent[0]).toEqual(
        'one line of text',
      );
    });

    it('maps multi line content (delimited with \\n) to itemContent', () => {
      faqs[0]!.content = 'first line of text\\nsecond line';

      const { result } = renderHook(() => useFaqItems(faqs));
      expect(result.current.faqItems[0]?.itemContent.length).toEqual(2);
      expect(result.current.faqItems[0]?.itemContent[0]).toEqual(
        'first line of text',
      );
      expect(result.current.faqItems[0]?.itemContent[1]).toEqual('second line');
    });

    it('maps faqId to key', () => {
      const { result } = renderHook(() => useFaqItems(faqs));
      expect(result.current.faqItems[0]?.key).toEqual('faqId');
    });

    it('maps title to heading', () => {
      const { result } = renderHook(() => useFaqItems(faqs));
      expect(result.current.faqItems[0]?.heading).toEqual('title');
    });
  });
});
