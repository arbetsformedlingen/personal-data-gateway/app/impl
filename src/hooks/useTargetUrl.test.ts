// @vitest-environment jsdom
import { act, renderHook } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';
import { useRouter } from 'next/router';
import useTargetUrl from './useTargetUrl';
import usePreventLeaving from './usePreventLeaving';

vi.mock('@app/hooks/usePreventLeaving');
vi.mock('next/router', () => ({ useRouter: vi.fn() }));

describe('useTargetUrl', () => {
  it('returns setTargetUrl state when setTargetUrl is called', () => {
    const preventLeavingMock = vi.fn();
    vi.mocked(usePreventLeaving).mockReturnValue({
      setPreventLeaving: preventLeavingMock,
    });

    const pushMock = vi.fn((_url: URL) => Promise.resolve(true));
    vi.mocked(useRouter, { partial: true }).mockReturnValue({ push: pushMock });

    const { result } = renderHook(() =>
      useTargetUrl(undefined, preventLeavingMock),
    );
    expect(pushMock).toBeCalledTimes(0);
    expect(preventLeavingMock).toBeCalledTimes(0);

    act(() => {
      result.current.setTargetUrl('url1');
    });
    expect(pushMock).toBeCalledTimes(1);
    expect(pushMock).toBeCalledWith('url1');
    expect(preventLeavingMock).toBeCalledTimes(1);
    expect(preventLeavingMock).toBeCalledWith(false);
  });
});
