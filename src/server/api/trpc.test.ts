import { describe, expect, it, vi } from 'vitest';
import {
  createInnerTRPCContext,
  createTRPCContext,
  createTRPCRouter,
  publicProcedure,
} from './trpc';
import { type NextApiRequest, type NextApiResponse } from 'next/types';

import { ZodError } from 'zod';
import { TRPCError } from '@trpc/server';

const { useMock } = vi.hoisted(() => ({
  useMock: vi.fn((arg: { middleware: MiddlewareArg }) => {
    arg.middleware({
      ctx: { user: 'user' },
      next: (arg: { ctx: { user: string | undefined } }) => arg,
    });
  }),
}));

const { routerMock } = vi.hoisted(() => ({
  routerMock: vi.fn((_arg) => 'example router'),
}));

type User = string | undefined;
type Ctx = { user: User };
type NextArg = { ctx: Ctx };
type NextFn = (arg: NextArg) => NextArg;
type MiddlewareArg = (arg: { ctx: Ctx; next: NextFn }) => NextArg;

const { middlewareMock, middlewareArgs } = vi.hoisted(() => {
  const middlewareArgs = new Map<string, MiddlewareArg>();
  return {
    middlewareArgs,
    middlewareMock: vi.fn((arg: MiddlewareArg) => {
      middlewareArgs.set('test', arg);
      return {
        middleware: arg,
      };
    }),
  };
});

type CreateArg = {
  transformer: unknown;
  errorFormatter: (arg: {
    shape: { data: unknown };
    error: { cause: unknown };
  }) => { dummy: string };
};

const { createMock, createArgs } = vi.hoisted(() => {
  const createArgs = new Map<string, CreateArg>();
  return {
    createArgs,
    createMock: vi.fn((arg: CreateArg) => {
      createArgs.set('test', arg);
      return {
        router: routerMock,
        middleware: middlewareMock,
        procedure: {
          use: useMock,
        },
      };
    }),
  };
});

const { contextMock } = vi.hoisted(() => ({
  contextMock: vi.fn(() => ({
    create: createMock,
  })),
}));

vi.mock('@trpc/server', async (importOriginal) => {
  // eslint-disable-next-line @typescript-eslint/consistent-type-imports
  const actual = await importOriginal<typeof import('@trpc/server')>();
  return {
    ...actual,
    initTRPC: {
      context: contextMock,
    },
  };
});

describe('createInnerTRPCContext', () => {
  it('returns user from context - ubdefined', () => {
    const result = createInnerTRPCContext({ user: undefined });

    expect(result).toEqual({ user: undefined });
  });

  it('returns user from context - string', () => {
    const result = createInnerTRPCContext({ user: '51263412' });

    expect(result).toEqual({ user: '51263412' });
  });
});

describe('createTRPCContext', () => {
  it('returns user from requests AUTH_HEADER', () => {
    const mockRequest = {
      headers: { 'remote-user': '612734t67234t1' },
    };
    const result = createTRPCContext({
      req: mockRequest as unknown as NextApiRequest,
      res: {} as NextApiResponse,
    });

    expect(result).toEqual({ user: '612734t67234t1' });
  });

  it('returns undefined from requests AUTH_HEADER when auth header is an array', () => {
    const mockRequest = {
      headers: { 'remote-user': ['612734t67234t1'] },
    };
    const result = createTRPCContext({
      req: mockRequest as unknown as NextApiRequest,
      res: {} as NextApiResponse,
    });

    expect(result).toEqual({ user: undefined });
  });
});

describe('createTRPCRouter', () => {
  it('does a singleton initialization, return the router, and run error tests - zod', () => {
    const result = createTRPCRouter({});
    expect(result).toBe('example router');

    const fn = createArgs.get('test');
    const errorResult = fn!.errorFormatter({
      shape: { data: { foo: 'foo', bar: 'bar' } },
      error: { cause: ZodError.create([]) },
    });

    expect(errorResult).toStrictEqual({
      data: {
        bar: 'bar',
        foo: 'foo',
        zodError: { fieldErrors: {}, formErrors: [] },
      },
    });
  });

  it('does a singleton initialization, return the router, and run error tests', () => {
    const result = createTRPCRouter({});
    expect(result).toBe('example router');

    const fn = createArgs.get('test');
    const errorResult = fn!.errorFormatter({
      shape: { data: { foo: 'foo', bar: 'bar' } },
      error: { cause: 'cause' },
    });

    expect(errorResult).toStrictEqual({
      data: { bar: 'bar', foo: 'foo', zodError: null },
    });
  });
});

describe('privateProcedure', () => {
  it('does a singleton initialization, throws TRPCError if no user present', () => {
    try {
      middlewareArgs.get('test')!({
        ctx: { user: undefined },
        next: (arg: { ctx: { user: string | undefined } }) => arg,
      });
    } catch (e) {
      expect(e).toEqual(new TRPCError({ code: 'UNAUTHORIZED' }));
    }
  });
});

describe('publicProcedure', () => {
  it('does a singleton initialization and returns procedure', () => {
    const result = publicProcedure;
    expect(result).toBeDefined();
  });
});
