import { describe, expect, it, vi, beforeEach } from 'vitest';
import { organizationRouterFactory } from './organization';

const { createTRPCRouter, publicProcedure } = vi.hoisted(() => ({
  createTRPCRouter: vi.fn().mockReturnValue('trpc router'),
  publicProcedure: {
    input: vi.fn().mockReturnThis(),
    query: vi
      .fn()
      .mockReturnValueOnce('getOrganization query result')
      .mockReturnValueOnce('getOrganizationFaqs query result'),
  },
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCRouter,
  publicProcedure,
}));

const {
  getOrganizationInputSchema,
  getOrganizationQueryFunction,
  getOrganizationFaqsInputSchema,
  getOrganizationFaqsQueryFunction,
} = vi.hoisted(() => ({
  getOrganizationInputSchema: 'getOrganization input schema',
  getOrganizationQueryFunction: vi.fn(),
  getOrganizationFaqsInputSchema: 'getOrganizationFaqs input schema',
  getOrganizationFaqsQueryFunction: vi.fn(),
}));

vi.mock('./schemas/organization', () => ({
  getOrganizationInputSchema,
  getOrganizationFaqsInputSchema,
}));
vi.mock('./handlers/organization', () => ({
  getOrganizationQueryFunction,
  getOrganizationFaqsQueryFunction,
}));

vi.mock('@app/utils/backend');

describe('organization', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('runs', () => {
    // Act
    const result = organizationRouterFactory();

    // Assert
    expect(publicProcedure.input).toHaveBeenCalledTimes(2);
    expect(publicProcedure.input).toHaveBeenCalledWith(
      'getOrganization input schema',
    );
    expect(publicProcedure.query).toHaveBeenCalledWith(
      getOrganizationQueryFunction,
    );
    expect(publicProcedure.input).toHaveBeenCalledWith(
      'getOrganizationFaqs input schema',
    );
    expect(publicProcedure.query).toHaveBeenCalledWith(
      getOrganizationFaqsQueryFunction,
    );
    expect(createTRPCRouter).toHaveBeenCalledWith({
      getOrganization: 'getOrganization query result',
      getOrganizationFaqs: 'getOrganizationFaqs query result',
    });
    expect(result).toBe('trpc router');
  });
});
