import { describe, expect, it } from 'vitest';
import { getClientInputSchema, getClientRedirectsInputSchema } from './client';

describe('client', () => {
  it('exports getClientInputSchema', () => {
    const result = getClientInputSchema.parse({ clientId: 'clientId' });
    expect(result.clientId).toBe('clientId');
  });

  it('exports getClientRedirectsInputSchema', () => {
    const result = getClientRedirectsInputSchema.parse({
      clientId: 'clientId',
    });
    expect(result.clientId).toBe('clientId');
  });
});
