import { describe, expect, it } from 'vitest';
import { getDatatypeInputSchema, getDatatypeFaqsInputSchema } from './datatype';

describe('datatype', () => {
  it('exports getDatatypeInputSchema', () => {
    const result = getDatatypeInputSchema.parse({ datatypeUrl: 'datatypeUrl' });
    expect(result.datatypeUrl).toBe('datatypeUrl');
  });

  it('exports getDatatypeFaqsInputSchema', () => {
    const result = getDatatypeFaqsInputSchema.parse({
      datatypeId: 'datatypeId',
    });
    expect(result.datatypeId).toBe('datatypeId');
  });
});
