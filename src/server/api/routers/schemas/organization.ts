import { z } from 'zod';

export const getOrganizationInputSchema = z.object({
  organizationId: z.string(),
});
export type GetOrganizationInputSchema = z.infer<
  typeof getOrganizationInputSchema
>;

export const getOrganizationFaqsInputSchema = z.object({
  organizationId: z.string(),
});
export type GetOrganizationFaqsInputSchema = z.infer<
  typeof getOrganizationFaqsInputSchema
>;
