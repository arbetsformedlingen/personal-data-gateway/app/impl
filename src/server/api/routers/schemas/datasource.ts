import { z } from 'zod';

export const getDatasourceInputSchema = z.object({ datasourceId: z.string() });
export type GetDatasourceInputSchema = z.infer<typeof getDatasourceInputSchema>;

export const getDatasourceFaqsInputSchema = z.object({
  datasourceId: z.string(),
});
export type GetDatasourceFaqsInputSchema = z.infer<
  typeof getDatasourceFaqsInputSchema
>;

export const getDatasourceUserDataInputSchema = z.object({
  datasourceId: z.string(),
});
export type GetDatasourceUserDataInputSchema = z.infer<
  typeof getDatasourceUserDataInputSchema
>;
