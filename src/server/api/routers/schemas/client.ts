import { z } from 'zod';

export const getClientInputSchema = z.object({ clientId: z.string() });
export type GetClientInputSchema = z.infer<typeof getClientInputSchema>;

export const getClientRedirectsInputSchema = z.object({ clientId: z.string() });
export type GetClientRedirectsInputSchema = z.infer<
  typeof getClientRedirectsInputSchema
>;
