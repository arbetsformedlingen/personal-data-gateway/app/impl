import { z } from 'zod';

export const getUserSharingsInputSchema = z.object({
  isActive: z.boolean(),
  clientId: z.optional(z.string()),
  datasourceId: z.optional(z.string()),
});
export type GetUserSharingsInputSchema = z.infer<
  typeof getUserSharingsInputSchema
>;

export const getUserSharingInputSchema = z.object({ sharingId: z.string() });
export type GetUserSharingInputSchema = z.infer<
  typeof getUserSharingInputSchema
>;

export const getDataAccessesInputSchema = z.object({ sharingId: z.string() });
export type GetDataAccessesInputSchema = z.infer<
  typeof getDataAccessesInputSchema
>;

export const approveSharingInputSchema = z.object({
  clientId: z.string(),
  datasourceId: z.string(),
  foreignUserId: z.string(),
  redirectUrl: z.string().url(),
});
export type ApproveSharingInputSchema = z.infer<
  typeof approveSharingInputSchema
>;

export const revokeSharingInputSchema = z.object({
  clientId: z.string(),
  datasourceId: z.string(),
  foreignUserId: z.optional(z.string()),
  redirectUrl: z.optional(z.string().url()),
});
export type RevokeSharingInputSchema = z.infer<typeof revokeSharingInputSchema>;
