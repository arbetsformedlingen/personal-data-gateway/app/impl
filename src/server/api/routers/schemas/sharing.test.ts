import { describe, expect, it } from 'vitest';
import { approveSharingInputSchema, revokeSharingInputSchema } from './sharing';

describe('sharing', () => {
  it('exports approveSharingInputSchema', () => {
    const result = approveSharingInputSchema.parse({
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'http://example.com/return',
    });
    expect(result.clientId).toBe('clientId');
    expect(result.datasourceId).toBe('datasourceId');
    expect(result.foreignUserId).toBe('foreignUserId');
    expect(result.redirectUrl).toBe('http://example.com/return');
  });

  it('exports revokeSharingInputSchema', () => {
    const result = revokeSharingInputSchema.parse({
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'http://example.com/return',
    });
    expect(result.clientId).toBe('clientId');
    expect(result.datasourceId).toBe('datasourceId');
    expect(result.foreignUserId).toBe('foreignUserId');
    expect(result.redirectUrl).toBe('http://example.com/return');
  });
});
