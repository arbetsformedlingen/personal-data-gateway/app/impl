import { describe, expect, it } from 'vitest';
import {
  getOrganizationInputSchema,
  getOrganizationFaqsInputSchema,
} from './organization';

describe('organization', () => {
  it('exports getOrganizationInputSchema', () => {
    const result = getOrganizationInputSchema.parse({
      organizationId: 'organizationId',
    });
    expect(result.organizationId).toBe('organizationId');
  });

  it('exports getOrganizationFaqsInputSchema', () => {
    const result = getOrganizationFaqsInputSchema.parse({
      organizationId: 'organizationId',
    });
    expect(result.organizationId).toBe('organizationId');
  });
});
