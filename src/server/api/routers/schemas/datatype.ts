import { z } from 'zod';

export const getDatatypeInputSchema = z.object({ datatypeUrl: z.string() });
export type GetDatatypeInputSchema = z.infer<typeof getDatatypeInputSchema>;

export const getDatatypeFaqsInputSchema = z.object({ datatypeId: z.string() });
export type GetDatatypeFaqsInputSchema = z.infer<
  typeof getDatatypeFaqsInputSchema
>;
