import { describe, expect, it } from 'vitest';
import {
  getDatasourceFaqsInputSchema,
  getDatasourceInputSchema,
  getDatasourceUserDataInputSchema,
} from './datasource';

describe('datasource', () => {
  it('exports getDatasourceInputSchema', () => {
    const result = getDatasourceInputSchema.parse({
      datasourceId: 'datasourceId',
    });
    expect(result.datasourceId).toBe('datasourceId');
  });

  it('exports getDatasourceFaqsInputSchema', () => {
    const result = getDatasourceFaqsInputSchema.parse({
      datasourceId: 'datasourceId',
    });
    expect(result.datasourceId).toBe('datasourceId');
  });

  it('exports getDatasourceUserDataInputSchema', () => {
    const result = getDatasourceUserDataInputSchema.parse({
      datasourceId: 'datasourceId',
    });
    expect(result.datasourceId).toBe('datasourceId');
  });
});
