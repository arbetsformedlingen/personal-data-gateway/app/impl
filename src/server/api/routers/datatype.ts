import { createTRPCRouter, publicProcedure } from '@app/server/api/trpc';
import {
  getDatatypeFaqsInputSchema,
  getDatatypeInputSchema,
} from './schemas/datatype';
import {
  getDatatypeQueryFunction,
  getDatatypeFaqsQueryFunction,
} from './handlers/datatype';

export const datatypeRouterFactory = () =>
  createTRPCRouter({
    getDatatype: publicProcedure
      .input(getDatatypeInputSchema)
      .query(getDatatypeQueryFunction),

    getDatatypeFaqs: publicProcedure
      .input(getDatatypeFaqsInputSchema)
      .query(getDatatypeFaqsQueryFunction),
  });
