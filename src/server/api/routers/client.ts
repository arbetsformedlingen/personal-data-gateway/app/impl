import { createTRPCRouter, publicProcedure } from '@app/server/api/trpc';
import {
  getClientInputSchema,
  getClientRedirectsInputSchema,
} from './schemas/client';
import {
  getClientQueryFunction,
  getClientRedirectsQueryFunction,
} from './handlers/client';

export const clientRouterFactory = () =>
  createTRPCRouter({
    getClient: publicProcedure
      .input(getClientInputSchema)
      .query(getClientQueryFunction),
    getClientRedirects: publicProcedure
      .input(getClientRedirectsInputSchema)
      .query(getClientRedirectsQueryFunction),
  });
