import {
  createTRPCRouter,
  privateProcedure,
  publicProcedure,
} from '@app/server/api/trpc';
import {
  getDatasourceInputSchema,
  getDatasourceFaqsInputSchema,
  getDatasourceUserDataInputSchema,
} from './schemas/datasource';
import {
  getDatasourceQueryFunction,
  getDatasourceFaqsQueryFunction,
  getDatasourceUserDataQueryFunction,
} from './handlers/datasource';

export const datasourceRouterFactory = () =>
  createTRPCRouter({
    getDatasource: publicProcedure
      .input(getDatasourceInputSchema)
      .query(getDatasourceQueryFunction),
    getDatasourceFaqs: publicProcedure
      .input(getDatasourceFaqsInputSchema)
      .query(getDatasourceFaqsQueryFunction),
    getDatasourceUserData: privateProcedure
      .input(getDatasourceUserDataInputSchema)
      .query(getDatasourceUserDataQueryFunction),
  });
