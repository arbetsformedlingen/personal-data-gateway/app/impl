import { createTRPCRouter, publicProcedure } from '@app/server/api/trpc';
import {
  getOrganizationFaqsInputSchema,
  getOrganizationInputSchema,
} from './schemas/organization';
import {
  getOrganizationFaqsQueryFunction,
  getOrganizationQueryFunction,
} from './handlers/organization';

export const organizationRouterFactory = () =>
  createTRPCRouter({
    getOrganization: publicProcedure
      .input(getOrganizationInputSchema)
      .query(getOrganizationQueryFunction),

    getOrganizationFaqs: publicProcedure
      .input(getOrganizationFaqsInputSchema)
      .query(getOrganizationFaqsQueryFunction),
  });
