import { describe, expect, it, vi, beforeEach } from 'vitest';
import { datatypeRouterFactory } from './datatype';

const { createTRPCRouter, publicProcedure } = vi.hoisted(() => ({
  createTRPCRouter: vi.fn().mockReturnValue('trpc router'),
  publicProcedure: {
    input: vi.fn().mockReturnThis(),
    query: vi
      .fn()
      .mockReturnValueOnce('getDatatype query result')
      .mockReturnValueOnce('getDatatypeFaqs query result'),
  },
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCRouter,
  publicProcedure,
}));

const {
  getDatatypeInputSchema,
  getDatatypeQueryFunction,
  getDatatypeFaqsInputSchema,
  getDatatypeFaqsQueryFunction,
} = vi.hoisted(() => ({
  getDatatypeInputSchema: 'getDatatype input schema',
  getDatatypeQueryFunction: vi.fn(),
  getDatatypeFaqsInputSchema: 'getDatatypeFaqs input schema',
  getDatatypeFaqsQueryFunction: vi.fn(),
}));

vi.mock('./schemas/datatype', () => ({
  getDatatypeInputSchema,
  getDatatypeFaqsInputSchema,
}));
vi.mock('./handlers/datatype', () => ({
  getDatatypeQueryFunction,
  getDatatypeFaqsQueryFunction,
}));

vi.mock('@app/utils/backend');

describe('organization', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('runs', () => {
    // Act
    const result = datatypeRouterFactory();

    // Assert
    expect(publicProcedure.input).toHaveBeenCalledTimes(2);
    expect(publicProcedure.input).toHaveBeenCalledWith(
      'getDatatype input schema',
    );
    expect(publicProcedure.query).toHaveBeenCalledWith(
      getDatatypeQueryFunction,
    );
    expect(publicProcedure.input).toHaveBeenCalledWith(
      'getDatatypeFaqs input schema',
    );
    expect(publicProcedure.query).toHaveBeenCalledWith(
      getDatatypeFaqsQueryFunction,
    );
    expect(createTRPCRouter).toHaveBeenCalledWith({
      getDatatype: 'getDatatype query result',
      getDatatypeFaqs: 'getDatatypeFaqs query result',
    });
    expect(result).toBe('trpc router');
  });
});
