import { afterEach, describe, expect, it, vi } from 'vitest';
import { getOrganization, getOrganizationFaqs } from '@app/utils/backend';
import {
  getOrganizationFaqsQueryFunction,
  getOrganizationQueryFunction,
} from './organization';
import { TRPCError } from '@trpc/server';
import { type components } from '@dist/spec';

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));
vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

vi.mock('@app/utils/backend');
vi.mock('../schemas/organization');

describe('organization', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  describe('getOrganization', () => {
    it('returns values from getOrganization response', async () => {
      // Arrange
      const getOrganizationMock = vi.mocked(getOrganization, { partial: true });
      getOrganizationMock.mockResolvedValueOnce({
        data: {
          _links: {
            self: {
              href: 'https://example.com/foo',
            },
          },
          organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          orgNr: 'orgNr',
          name: 'ABC',
          created: '2024-02-02T15:08:24Z',
          updated: '2024-02-02T15:08:24Z',
        },
      });

      // Act
      const response = await getOrganizationQueryFunction({
        input: {
          organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(getOrganizationMock).toBeCalledTimes(1);
      expect(getOrganizationMock).toBeCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.organizationId).toBe(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.orgNr).toBe('orgNr');
      expect(response?.name).toBe('ABC');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getOrganization response error is "404 Not Found"', async () => {
      // Arrange
      const getOrganizationMock = vi.mocked(getOrganization, { partial: true });
      getOrganizationMock.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getOrganizationQueryFunction({
          input: {
            organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch organization information.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getOrganizationQueryFunction',
          input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message: 'Failed to fetch organization information.',
          }),
        },
        'NOT_FOUND - Failed to fetch organization information.',
      );
    });

    it('throws TRPCError when getOrganization response error is other than "404 Not found"', async () => {
      // Arrange
      const getOrganizationMock = vi.mocked(getOrganization, { partial: true });
      getOrganizationMock.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getOrganizationQueryFunction({
          input: {
            organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch organization information.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getOrganizationQueryFunction',
          input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to fetch organization information.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to fetch organization information.',
      );
    });
  });

  describe('getOrganizationFaqs', () => {
    it('returns values from getOrganizationFaqs response', async () => {
      // Arrange
      const getOrganizationFaqsMock = vi.mocked(getOrganizationFaqs, {
        partial: true,
      });
      getOrganizationFaqsMock.mockResolvedValueOnce({
        data: [
          {
            _links: {
              self: {
                href: 'https://example.com/foo',
              },
            },
            faqId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
            organizationId: '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
            lang: 'sv',
            title: 'title',
            content: 'content',
            created: '2024-02-02T15:08:24Z',
            updated: '2024-02-02T15:08:24Z',
          },
        ],
      });

      // Act
      const response = await getOrganizationFaqsQueryFunction({
        input: {
          organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(getOrganizationFaqsMock).toBeCalledTimes(1);
      expect(getOrganizationFaqsMock).toBeCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.length).toBe(1);
      const faqArray =
        response as unknown as components['schemas']['GetOrganizationFaqResponse'][];
      const faq = faqArray[0];

      expect(faq?.faqId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(faq?.lang).toBe('sv');
      expect(faq?.title).toBe('title');
      expect(faq?.content).toBe('content');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationFaqsQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getOrganizationFaqs response error is "404 Not Found"', async () => {
      // Arrange
      const getOrganizationFaqsMock = vi.mocked(getOrganizationFaqs, {
        partial: true,
      });
      getOrganizationFaqsMock.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getOrganizationFaqsQueryFunction({
          input: {
            organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch organization FAQs.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationFaqsQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getOrganizationFaqsQueryFunction',
          input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message: 'Failed to fetch organization FAQs.',
          }),
        },
        'NOT_FOUND - Failed to fetch organization FAQs.',
      );
    });

    it('throws TRPCError when getOrganization response error is other than "404 Not found"', async () => {
      // Arrange
      const getOrganizationFaqsMock = vi.mocked(getOrganizationFaqs, {
        partial: true,
      });
      getOrganizationFaqsMock.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getOrganizationFaqsQueryFunction({
          input: {
            organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch organization FAQs.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getOrganizationFaqsQueryFunction',
        input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getOrganizationFaqsQueryFunction',
          input: { organizationId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to fetch organization FAQs.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to fetch organization FAQs.',
      );
    });
  });
});
