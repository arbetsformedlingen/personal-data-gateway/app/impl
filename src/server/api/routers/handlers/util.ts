import { createLogger } from '@app/utils/logger';
import { TRPCError } from '@trpc/server';
import { type TRPC_ERROR_CODE_KEY } from '@trpc/server/rpc';

const logger = createLogger('api/routers/handlers/sharing');

export const isActive = (revoked?: string, expires?: string) => {
  const now = new Date().getTime();
  return (
    (revoked === undefined || Date.parse(revoked) > now) && // in practice revocations timestamps will always be in the past
    (expires === undefined || Date.parse(expires) > now)
  ); // only sharings that has not yet expired are considered active
};

const trpcErrorCode: (status?: number) => TRPC_ERROR_CODE_KEY = (
  status?: number,
) => {
  switch (status) {
    case 401:
      return 'UNAUTHORIZED';
    case 403:
      return 'FORBIDDEN';
    case 404:
      return 'NOT_FOUND';
    default:
      return 'INTERNAL_SERVER_ERROR';
  }
};

export const throwTrcpError = (
  func: string,
  input: unknown,
  status: number,
  message: string,
) => {
  const code = trpcErrorCode(status);
  const trpcError = new TRPCError({
    code,
    message,
  });
  logger.error(
    { function: func, input, trpcError },
    `${trpcError.code} - ${trpcError.message}`,
  );
  throw trpcError;
};
