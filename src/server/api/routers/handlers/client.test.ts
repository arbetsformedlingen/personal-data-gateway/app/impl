/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { afterEach, describe, expect, it, vi } from 'vitest';
import { getClient, getClientRedirects } from '@app/utils/backend';
import {
  getClientQueryFunction,
  getClientRedirectsQueryFunction,
} from './client';

const { logger, isActiveMock, throwTrcpErrorMock } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
  isActiveMock: vi.fn(),
  throwTrcpErrorMock: vi.fn(),
}));

vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));
vi.mock('@app/utils/backend');
vi.mock('../schemas/client');
vi.mock('./util', () => ({
  isActive: isActiveMock,
  throwTrcpError: throwTrcpErrorMock,
}));

describe('client', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  describe('getClientQueryFunction', () => {
    it('returns values from getClient response', async () => {
      // Arrange
      const mockClientInfo = vi.mocked(getClient, { partial: true });
      mockClientInfo.mockResolvedValueOnce({
        data: {
          _links: {
            self: {
              href: 'https://example.com/foo',
            },
          },
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          name: 'ABC',
          organizationId: '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
          role: 'consumer',
          created: '2024-02-02T15:08:24Z',
          updated: '2024-02-02T15:08:24Z',
        },
      });

      // Act
      const result = await getClientQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(mockClientInfo).toHaveBeenCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(result?.clientId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(result?.name).toBe('ABC');
      expect(result?.organizationId).toBe(
        '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getClient response error is "404 Not Found"', async () => {
      // Arrange
      const mockClientInfo = vi.mocked(getClient, { partial: true });
      mockClientInfo.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act
      await getClientQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(throwTrcpErrorMock).toBeCalledWith(
        'getClient',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        404,
        'Failed to fetch client details.',
      );
      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
    });

    it('throws TRPCError when getClient response error is other than "404 Not found"', async () => {
      // Arrange
      const mockClientInfo = vi.mocked(getClient, { partial: true });
      mockClientInfo.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act
      await getClientQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(throwTrcpErrorMock).toBeCalledWith(
        'getClient',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        418,
        'Failed to fetch client details.',
      );
      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
    });

    it('throws TRPCError if client is revoked"', async () => {
      // Arrange
      const mockClientInfo = vi.mocked(getClient, { partial: true });
      mockClientInfo.mockResolvedValueOnce({
        data: {
          _links: {},
          clientId: 'clientId',
          organizationId: 'organizationId',
          name: 'name',
          role: 'consumer',
          created: '',
          updated: '',
          revoked: 'revoked',
        },
      });

      isActiveMock.mockReturnValue(false);

      // Act
      await getClientQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(throwTrcpErrorMock).toBeCalledWith(
        'getClient',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        404,
        'Client is revoked',
      );
      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(isActiveMock).toHaveBeenCalledOnce();
      expect(isActiveMock).toHaveBeenCalledWith('revoked');
      expect(throwTrcpErrorMock).toHaveBeenCalledWith(
        'getClient',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        404,
        'Client is revoked',
      );
    });
  });

  describe('getClientRedirectsQueryFunction', () => {
    it('returns values from getClientRedirects response', async () => {
      // Arrange
      const mockClientRedirectsInfo = vi.mocked(getClientRedirects, {
        partial: true,
      });
      mockClientRedirectsInfo.mockResolvedValueOnce({
        data: [
          {
            _links: {
              self: {
                href: 'https://example.com/foo',
              },
            },
            redirectId: '73e3f902-5c12-49c4-9a6f-757303f6c768',
            clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
            redirectUrl: 'http://example.se/redirect',
            created: '2024-02-02T15:08:24Z',
            updated: '2024-02-02T16:08:24Z',
            deleted: '2024-02-02T17:08:24Z',
          },
        ],
      });

      // Act
      const result = await getClientRedirectsQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(mockClientRedirectsInfo).toHaveBeenCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(result![0]!.redirectId).toBe(
        '73e3f902-5c12-49c4-9a6f-757303f6c768',
      );
      expect(result![0]!.clientId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(result![0]!.redirectUrl).toBe('http://example.se/redirect');
      expect(result![0]!.created).toBe('2024-02-02T15:08:24Z');
      expect(result![0]!.updated).toBe('2024-02-02T16:08:24Z');
      expect(result![0]!.deleted).toBe('2024-02-02T17:08:24Z');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientRedirectsQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getClient response error is "404 Not Found"', async () => {
      // Arrange
      const mockClientRedirectsInfo = vi.mocked(getClientRedirects, {
        partial: true,
      });
      mockClientRedirectsInfo.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act
      await getClientRedirectsQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(throwTrcpErrorMock).toBeCalledWith(
        'getClientRedirects',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        404,
        'Failed to fetch client redirects.',
      );
      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientRedirectsQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
    });

    it('throws TRPCError when getClient response error is other than "404 Not found"', async () => {
      // Arrange
      const mockClientRedirectsInfo = vi.mocked(getClientRedirects, {
        partial: true,
      });
      mockClientRedirectsInfo.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act
      await getClientRedirectsQueryFunction({
        input: {
          clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(throwTrcpErrorMock).toBeCalledWith(
        'getClientRedirects',
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        418,
        'Failed to fetch client redirects.',
      );
      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getClientRedirectsQueryFunction',
        input: { clientId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
    });
  });
});
