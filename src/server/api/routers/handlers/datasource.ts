import {
  getDatasource,
  getDatasourceFaqs,
  getDatasourceUserData,
} from '@app/utils/backend';
import {
  type GetDatasourceFaqsInputSchema,
  type GetDatasourceInputSchema,
  type GetDatasourceUserDataInputSchema,
} from '../schemas/datasource';
import type { CreateContextOptions } from '../../trpc';
import { createLogger } from '@app/utils/logger';
import { throwTrcpError } from './util';

const logger = createLogger('api/routers/handlers/datasource');

export const getDatasourceQueryFunction = async ({
  input,
}: {
  input: GetDatasourceInputSchema;
}) => {
  logger.info({ function: 'getDatasourceQueryFunction', input });

  const { datasourceId } = input;
  const response = await getDatasource(datasourceId);
  if (response.error) {
    throwTrcpError(
      'getDatasourceQueryFunction',
      input,
      response.error.status,
      'Failed to fetch datasource information.',
    );
  }

  const { data } = response;
  return data;
};

export const getDatasourceFaqsQueryFunction = async ({
  input,
}: {
  input: GetDatasourceFaqsInputSchema;
}) => {
  logger.info({ function: 'getDatasourceFaqsQueryFunction', input });

  const { datasourceId } = input;
  const response = await getDatasourceFaqs(datasourceId);
  if (response.error) {
    throwTrcpError(
      'getDatasourceFaqsQueryFunction',
      input,
      response.error.status,
      'Failed to fetch datasource faqs.',
    );
  }

  const { data } = response;
  return data;
};

export const getDatasourceUserDataQueryFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: GetDatasourceUserDataInputSchema;
}) => {
  logger.info({ function: 'getDatasourceUserDataQueryFunction', input });

  if (!ctx.user)
    throwTrcpError(
      'getDatasourceUserDataQueryFunction',
      input,
      401,
      'User is not authorized',
    );

  const response = await getDatasourceUserData(input.datasourceId, ctx.user!);

  if (response.error) {
    if (response.error.status != 451) {
      throwTrcpError(
        'getDatasourceUserDataQueryFunction',
        input,
        response.error.status,
        'Failed to fetch preview of sharing data, invalid response from back end.',
      );
    } else {
      const rv = response.error;
      return rv;
    }
  }

  const rv = response.data;

  return rv;
};
