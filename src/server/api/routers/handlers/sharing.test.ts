import type { components } from '@dist/spec';
import {
  createSharing,
  getClient,
  getClientRedirects,
  getDataAccesses,
  getDatasource,
  getDatatype,
  getUserSharing,
  getUserSharings,
  revokeSharing,
} from '@app/utils/backend';
import {
  afterEach,
  beforeEach,
  describe,
  expect,
  it,
  type Mocked,
  vi,
} from 'vitest';
import type {
  GetDataAccessesInputSchema,
  GetUserSharingInputSchema,
  GetUserSharingsInputSchema,
  ApproveSharingInputSchema,
  RevokeSharingInputSchema,
} from '../schemas/sharing';
import {
  getDataAccessesQueryFunction,
  getUserSharingQueryFunction,
  getUserSharingsQueryFunction,
  approveSharingMutationFunction,
  revokeSharingMutationFunction,
} from './sharing';
import { TRPCError } from '@trpc/server';

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));
vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

vi.mock('@app/utils/backend');
vi.mock('../schemas/sharing');

describe('getUserSharingsQueryFunction', () => {
  beforeEach(() => {
    vi.useFakeTimers();
    vi.clearAllMocks();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  const now = new Date(2024, 9, 4);

  const sharing = {
    _links: {
      self: {
        href: 'https://example.com/foo',
      },
    },
    sharingId: 'sharingId',
    userId: 'userId',
    datasourceId: 'datasourceId',
    clientId: 'clientId',
    datasourceName: 'name',
    public: false,
    created: '2024-02-02T15:08:24Z',
    updated: '2024-02-02T15:08:24Z',
  };

  const publicSharing = {
    _links: {
      self: {
        href: 'https://example.com/foo',
      },
    },
    sharingId: 'sharingId',
    userId: 'userId',
    datasourceId: 'datasourceId',
    clientId: undefined,
    datasourceName: 'name',
    public: true,
    created: '2024-02-02T15:08:24Z',
    updated: '2024-02-02T15:08:24Z',
  };

  const mockGetDatasourceResponse = {
    data: {
      _links: {},
      datasourceId: 'datasourceId',
      organizationId: 'organizationId',
      name: 'name',
      datatypeUrl: 'datatypeUrl',
      created: 'created',
      updated: 'updated',
    },
  };

  const mockGetDatatypeResponse = {
    data: {
      _links: {},
      datatypeId: 'datatypeId',
      name: 'name',
      humanFriendlyName: 'humanFriendlyName',
      humanFriendlyDescription: 'humanFriendlyDescription',
      created: 'created',
      updated: 'updated',
    },
  };

  it('throws an error if "user" in context is undefined', async () => {
    // Act
    await expect(() =>
      getUserSharingsQueryFunction({
        ctx: {
          user: undefined,
        },
        input: {} as GetUserSharingsInputSchema,
      }),
    ).rejects.toThrowError('User is not authorized');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingsQueryFunction',
      input: {},
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingsQueryFunction',
        input: {},
        trpcError: new TRPCError({
          code: 'UNAUTHORIZED',
          message: 'User is not authorized',
        }),
      },
      'UNAUTHORIZED - User is not authorized',
    );
  });

  it('throws an error if fetching user sharings fails', async () => {
    // Arrange

    const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
    getDatatypeMock.mockResolvedValue(mockGetDatatypeResponse);

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      error: { error: 'Not found', status: 404 },
    });

    // Act
    await expect(() =>
      getUserSharingsQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          clientId: 'clientId',
          datasourceId: 'datasourceId',
          isActive: true,
        } as GetUserSharingsInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch userSharings details, invalid response from back end.',
    );

    // Assert

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingsQueryFunction',
      input: {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        isActive: true,
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingsQueryFunction',
        input: {
          clientId: 'clientId',
          datasourceId: 'datasourceId',
          isActive: true,
        },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch userSharings details, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch userSharings details, invalid response from back end.',
    );
  });

  it('throws an error if fetching client fails', async () => {
    // Arrange
    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [sharing],
    });

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      error: { error: 'Not found', status: 404 },
    });

    // Act
    await expect(() =>
      getUserSharingsQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          clientId: 'clientId',
          datasourceId: 'datasourceId',
          isActive: true,
        } as GetUserSharingsInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch client details, invalid response from back end.',
    );

    // Assert

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingsQueryFunction',
      input: {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        isActive: true,
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getClientName',
        input: 'clientId',
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch client details, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch client details, invalid response from back end.',
    );
  });

  it('throws an error if fetching datasource fails', async () => {
    // Arrange
    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [sharing],
    });

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue({
      error: { error: 'Not found', status: 404 },
    });

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    // Act
    await expect(() =>
      getUserSharingsQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          clientId: 'clientId',
          datasourceId: 'datasourceId',
          isActive: true,
        } as GetUserSharingsInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch datasource details, invalid response from back end.',
    );

    // Assert

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingsQueryFunction',
      input: {
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        isActive: true,
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingsQueryFunction',
        input: {
          clientId: 'clientId',
          datasourceId: 'datasourceId',
          isActive: true,
        },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch datasource details, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch datasource details, invalid response from back end.',
    );
  });

  it('returns sharings not yet expired from getUserSharings response', async () => {
    // Arrange
    const expires = '2024-10-05T00:00:00.000Z';

    const stillNotExpiredSharing = { ...sharing, expires };

    vi.setSystemTime(now);

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue(mockGetDatasourceResponse);

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [stillNotExpiredSharing],
    });

    const expectedResult = {
      clientName: 'clientName',
      created: '2024-02-02T15:08:24Z',
      datasourceName: 'name',
      expires: '2024-10-05T00:00:00.000Z',
      revoked: undefined,
      sharingId: 'sharingId',
      unmapped: {
        _links: {
          self: {
            href: 'https://example.com/foo',
          },
        },
        clientId: 'clientId',
        created: '2024-02-02T15:08:24Z',
        datasourceId: 'datasourceId',
        datasourceName: 'name',
        expires: '2024-10-05T00:00:00.000Z',
        public: false,
        sharingId: 'sharingId',
        updated: '2024-02-02T15:08:24Z',
        userId: 'userId',
      },
    };

    // Act
    const response = await getUserSharingsQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        isActive: true,
      },
    });

    // Assert
    expect(response).toStrictEqual([expectedResult]);

    expect(getClientMock).toBeCalledTimes(1);
    expect(getClientMock).toBeCalledWith('clientId');
    expect(getUserSharingsMock).toBeCalledTimes(1);
    expect(getUserSharingsMock).toBeCalledWith('19730113-6273');
  });

  it('do not return sharings that has expired from getUserSharings response', async () => {
    // Arrange
    const expires = new Date(2024, 9, 3).toISOString();

    const expiredSharing = { ...sharing, expires };

    vi.setSystemTime(now);

    const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
    getDatatypeMock.mockResolvedValue(mockGetDatatypeResponse);

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [expiredSharing],
    });

    // Act
    const response = await getUserSharingsQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        isActive: true,
      },
    });

    // Assert
    expect(response).toStrictEqual([]);

    expect(getUserSharingsMock).toBeCalledTimes(1);
    expect(getUserSharingsMock).toBeCalledWith('19730113-6273');
  });

  it('returns all sharings from getUserSharings response', async () => {
    // Arrange
    const expires = '2024-10-03T00:00:00.000Z';

    const expiredSharing = { ...sharing, expires };

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue(mockGetDatasourceResponse);

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [expiredSharing],
    });

    const expectedResult = {
      clientName: 'clientName',
      created: '2024-02-02T15:08:24Z',
      datasourceName: 'name',
      expires: '2024-10-03T00:00:00.000Z',
      revoked: undefined,
      sharingId: 'sharingId',
      unmapped: {
        _links: {
          self: {
            href: 'https://example.com/foo',
          },
        },
        clientId: 'clientId',
        created: '2024-02-02T15:08:24Z',
        datasourceId: 'datasourceId',
        datasourceName: 'name',
        expires: '2024-10-03T00:00:00.000Z',
        public: false,
        sharingId: 'sharingId',
        updated: '2024-02-02T15:08:24Z',
        userId: 'userId',
      },
    };

    // Act
    const response = await getUserSharingsQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        isActive: false,
      },
    });

    // Assert
    expect(response).toStrictEqual([expectedResult]);

    expect(getClientMock).toBeCalledTimes(1);
    expect(getClientMock).toBeCalledWith('clientId');
    expect(getUserSharingsMock).toBeCalledTimes(1);
    expect(getUserSharingsMock).toBeCalledWith('19730113-6273');
  });

  it('returns all sharings from getUserSharings response - public', async () => {
    // Arrange
    const expires = '2024-10-10T00:00:00.000Z';

    const expiredSharing = { ...publicSharing, expires };

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue(mockGetDatasourceResponse);

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: [expiredSharing],
    });

    const expectedResult = {
      clientName: '<publikt tillgänglig>',
      created: '2024-02-02T15:08:24Z',
      datasourceName: 'name',
      expires: '2024-10-10T00:00:00.000Z',
      revoked: undefined,
      sharingId: 'sharingId',
      unmapped: {
        _links: {
          self: {
            href: 'https://example.com/foo',
          },
        },
        clientId: undefined,
        created: '2024-02-02T15:08:24Z',
        datasourceId: 'datasourceId',
        datasourceName: 'name',
        expires: '2024-10-10T00:00:00.000Z',
        public: true,
        sharingId: 'sharingId',
        updated: '2024-02-02T15:08:24Z',
        userId: 'userId',
      },
    };

    // Act
    const response = await getUserSharingsQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        isActive: false,
      },
    });

    // Assert
    expect(response).toStrictEqual([expectedResult]);

    expect(getClientMock).toBeCalledTimes(0);
    expect(getUserSharingsMock).toBeCalledTimes(1);
    expect(getUserSharingsMock).toBeCalledWith('19730113-6273');
  });
});

describe('getUserSharingQueryFunction', () => {
  beforeEach(() => {
    vi.useFakeTimers();
    vi.clearAllMocks();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  const now = new Date(2024, 9, 4);

  const sharing = {
    _links: {
      self: {
        href: 'https://example.com/foo',
      },
    },
    sharingId: 'sharingId',
    userId: 'userId',
    datasourceId: 'datasourceId',
    clientId: 'clientId',
    public: false,
    created: '2024-02-02T15:08:24Z',
    updated: '2024-02-02T15:08:24Z',
  };

  const mockGetDatasourceResponse = {
    data: {
      _links: {},
      datasourceId: 'datasourceId',
      organizationId: 'organizationId',
      name: 'name',
      datatypeUrl: 'datatypeUrl',
      created: 'created',
      updated: 'updated',
    },
  };

  const mockGetDatatypeResponse = {
    data: {
      _links: {},
      datatypeId: 'datatypeId',
      name: 'name',
      humanFriendlyName: 'humanFriendlyName',
      humanFriendlyDescription: 'humanFriendlyDescription',
      created: 'created',
      updated: 'updated',
    },
  };

  it('throws an error if "user" in context is undefined', async () => {
    // Act
    await expect(() =>
      getUserSharingQueryFunction({
        ctx: {
          user: undefined,
        },
        input: {} as GetUserSharingInputSchema,
      }),
    ).rejects.toThrowError('User is not authorized');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingQueryFunction',
      input: {},
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingQueryFunction',
        input: {},
        trpcError: new TRPCError({
          code: 'UNAUTHORIZED',
          message: 'User is not authorized',
        }),
      },
      'UNAUTHORIZED - User is not authorized',
    );
  });

  it('throws an error if fetching user sharing fails', async () => {
    // Arrange

    const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
    getDatatypeMock.mockResolvedValue(mockGetDatatypeResponse);

    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValueOnce({
      error: { error: 'Not found', status: 404 },
    });

    // Act
    await expect(() =>
      getUserSharingQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          sharingId: 'sharingId',
        } as GetUserSharingInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch userSharing, invalid response from back end.',
    );

    // Assert

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingQueryFunction',
      input: {
        sharingId: 'sharingId',
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingQueryFunction',
        input: { sharingId: 'sharingId' },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch userSharing, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch userSharing, invalid response from back end.',
    );
  });

  it('throws an error if fetching datasource fails', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValueOnce({
      data: sharing,
    });

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue({
      error: { error: 'Not found', status: 404 },
    });

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    // Act
    await expect(() =>
      getUserSharingQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          sharingId: 'sharingId',
        } as GetUserSharingInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch datasource details, invalid response from back end.',
    );

    // Assert

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getUserSharingQueryFunction',
      input: {
        sharingId: 'sharingId',
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getUserSharingQueryFunction',
        input: {
          sharingId: 'sharingId',
        },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch datasource details, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch datasource details, invalid response from back end.',
    );
  });

  it('returns sharing not yet expired from getUserSharing response', async () => {
    // Arrange
    const expires = '2024-10-05T00:00:00.000Z';

    const stillNotExpiredSharing = { ...sharing, expires };

    vi.setSystemTime(now);

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue(mockGetDatasourceResponse);

    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        _links: {},
        clientId: 'clientId',
        name: 'clientName',
        organizationId: 'organizationId',
        role: 'consumer',
        created: '2024-02-02T15:08:24Z',
        updated: '2024-02-02T15:08:24Z',
      },
    });

    const getUserSharingsMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingsMock.mockResolvedValueOnce({
      data: stillNotExpiredSharing,
    });

    const expectedResult = {
      clientId: 'clientId',
      created: '2024-02-02T15:08:24Z',
      expires: '2024-10-05T00:00:00.000Z',
      revoked: undefined,
      datasourceId: 'datasourceId',
      sharingId: 'sharingId',
      unmapped: {
        _links: {
          self: {
            href: 'https://example.com/foo',
          },
        },
        clientId: 'clientId',
        created: '2024-02-02T15:08:24Z',
        datasourceId: 'datasourceId',
        expires: '2024-10-05T00:00:00.000Z',
        public: false,
        sharingId: 'sharingId',
        updated: '2024-02-02T15:08:24Z',
        userId: 'userId',
      },
    };

    // Act
    const response = await getUserSharingQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        sharingId: 'sharingId',
      },
    });

    // Assert
    expect(response).toStrictEqual(expectedResult);

    expect(getUserSharingsMock).toBeCalledTimes(1);
    expect(getUserSharingsMock).toBeCalledWith('19730113-6273', 'sharingId');
  });

  it('do not return sharings that has expired from getUserSharing response', async () => {
    // Arrange
    const expires = '2024-10-03T00:00:00.000Z';

    const expiredSharing = { ...sharing, expires };

    vi.setSystemTime(now);

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue(mockGetDatasourceResponse);

    const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
    getDatatypeMock.mockResolvedValue(mockGetDatatypeResponse);

    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValueOnce({
      data: expiredSharing,
    });

    const expected = {
      clientId: 'clientId',
      created: '2024-02-02T15:08:24Z',
      expires: '2024-10-03T00:00:00.000Z',
      revoked: undefined,
      datasourceId: 'datasourceId',
      sharingId: 'sharingId',
      unmapped: {
        _links: {
          self: {
            href: 'https://example.com/foo',
          },
        },
        clientId: 'clientId',
        created: '2024-02-02T15:08:24Z',
        datasourceId: 'datasourceId',
        expires: '2024-10-03T00:00:00.000Z',
        public: false,
        sharingId: 'sharingId',
        updated: '2024-02-02T15:08:24Z',
        userId: 'userId',
      },
    };

    // Act
    const response = await getUserSharingQueryFunction({
      ctx: { user: '19730113-6273' },
      input: {
        sharingId: 'sharingId',
      },
    });

    // Assert
    expect(response).toStrictEqual(expected);

    expect(getUserSharingMock).toBeCalledTimes(1);
    expect(getUserSharingMock).toBeCalledWith('19730113-6273', 'sharingId');
  });
});

describe('getDataAccessesQueryFunction', () => {
  beforeEach(() => {
    vi.useFakeTimers();
    vi.clearAllMocks();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('throws an error if "user" in context is undefined', async () => {
    // Act
    await expect(() =>
      getDataAccessesQueryFunction({
        ctx: {
          user: undefined,
        },
        input: {} as GetDataAccessesInputSchema,
      }),
    ).rejects.toThrowError('User is not authorized');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: {},
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getDataAccessesQueryFunction',
        input: {},
        trpcError: new TRPCError({
          code: 'UNAUTHORIZED',
          message: 'User is not authorized',
        }),
      },
      'UNAUTHORIZED - User is not authorized',
    );
  });

  it('throws an error if fetching sharing fails', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValue({
      error: { error: 'Not found', status: 404 },
    });

    // Act
    await expect(() =>
      getDataAccessesQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          sharingId: 'sharingId',
        } as GetDataAccessesInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch userSharing details, invalid response from back end.',
    );

    // Assert
    expect(getUserSharingMock).toHaveBeenCalledWith('user', 'sharingId');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: { sharingId: 'sharingId' },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getDataAccessesQueryFunction',
        input: { sharingId: 'sharingId' },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch userSharing details, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch userSharing details, invalid response from back end.',
    );
  });

  it('throws an error if user sharings user does not match actual user', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValue({
      data: {
        _links: {},
        sharingId: 'sharingId',
        datasourceId: 'datasourceId',
        userId: 'userId',
        public: false,
        created: ',',
        updated: 'updated',
      },
    });

    // Act
    await expect(() =>
      getDataAccessesQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          sharingId: 'sharingId',
        } as GetDataAccessesInputSchema,
      }),
    ).rejects.toThrowError('Sharings userId does not match actual userId');

    // Assert
    expect(getUserSharingMock).toHaveBeenCalledWith('user', 'sharingId');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: { sharingId: 'sharingId' },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getDataAccessesQueryFunction',
        input: { sharingId: 'sharingId' },
        trpcError: new TRPCError({
          code: 'INTERNAL_SERVER_ERROR',
          message: 'Sharings userId does not match actual userId',
        }),
      },
      'INTERNAL_SERVER_ERROR - Sharings userId does not match actual userId',
    );
  });

  it('throws an error if fetching data accesses fails', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValue({
      data: {
        _links: {},
        sharingId: 'sharingId',
        datasourceId: 'datasourceId',
        userId: 'userId',
        public: false,
        created: ',',
        updated: 'updated',
      },
    });

    const getDataAccessesMock = vi.mocked(getDataAccesses, { partial: true });
    getDataAccessesMock.mockResolvedValue({
      error: { error: 'Not found', status: 404 },
    });

    // Act
    await expect(() =>
      getDataAccessesQueryFunction({
        ctx: {
          user: 'userId',
        },
        input: {
          sharingId: 'sharingId',
        } as GetDataAccessesInputSchema,
      }),
    ).rejects.toThrowError(
      'Failed to fetch data accesses, invalid response from back end.',
    );

    // Assert
    expect(getUserSharingMock).toHaveBeenCalledWith('userId', 'sharingId');

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: { sharingId: 'sharingId' },
    });
    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'getDataAccessesQueryFunction',
        input: { sharingId: 'sharingId' },
        trpcError: new TRPCError({
          code: 'NOT_FOUND',
          message:
            'Failed to fetch data accesses, invalid response from back end.',
        }),
      },
      'NOT_FOUND - Failed to fetch data accesses, invalid response from back end.',
    );
  });

  it('returns all data accesses for the given sharing', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValue({
      data: {
        _links: {},
        sharingId: 'sharingId',
        datasourceId: 'datasourceId',
        userId: 'userId',
        public: false,
        created: ',',
        updated: 'updated',
      },
    });

    const getDataAccessesMock = vi.mocked(getDataAccesses, { partial: true });
    getDataAccessesMock.mockResolvedValue({
      data: [
        {
          _links: {},
          accessId: 'accessId',
          sharingId: 'sharingId',
          created: 'created',
          updated: 'updated',
          accessed: 'accessed',
        },
      ],
    });

    const expectedResult = [
      {
        _links: {},
        accessId: 'accessId',
        sharingId: 'sharingId',
        created: 'created',
        updated: 'updated',
        accessed: 'accessed',
      },
    ];

    // Act
    const result = await getDataAccessesQueryFunction({
      ctx: {
        user: 'userId',
      },
      input: {
        sharingId: 'sharingId',
      } as GetDataAccessesInputSchema,
    });

    // Assert
    expect(result).toStrictEqual(expectedResult);

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: { sharingId: 'sharingId' },
    });
  });

  it('returns an empty array if getDataAccesses.data is undefined', async () => {
    // Arrange
    const getUserSharingMock = vi.mocked(getUserSharing, { partial: true });
    getUserSharingMock.mockResolvedValue({
      data: {
        _links: {},
        sharingId: 'sharingId',
        datasourceId: 'datasourceId',
        userId: 'userId',
        public: false,
        created: ',',
        updated: 'updated',
      },
    });

    const getDataAccessesMock = vi.mocked(getDataAccesses, { partial: true });
    getDataAccessesMock.mockResolvedValue({
      data: undefined,
    });

    const expectedResult: components['schemas']['GetDataAccessesResponse'] = [];

    // Act
    const result = await getDataAccessesQueryFunction({
      ctx: {
        user: 'userId',
      },
      input: {
        sharingId: 'sharingId',
      } as GetDataAccessesInputSchema,
    });

    // Assert
    expect(result).toStrictEqual(expectedResult);

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'getDataAccessesQueryFunction',
      input: { sharingId: 'sharingId' },
    });
  });
});

describe('sharing', () => {
  beforeEach(() => {
    vi.useFakeTimers();
    vi.clearAllMocks();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  describe('approveSharingMutationFunction', () => {
    it('throws an error if "user" in context is undefined', async () => {
      // Act
      await expect(() =>
        approveSharingMutationFunction({
          ctx: {
            user: undefined,
          },
          input: {} as ApproveSharingInputSchema,
        }),
      ).rejects.toThrowError('User is not authorized');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: {},
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'approveSharingMutationFunction',
          input: {},
          trpcError: new TRPCError({
            code: 'UNAUTHORIZED',
            message: 'User is not authorized',
          }),
        },
        'UNAUTHORIZED - User is not authorized',
      );
    });

    it('throws an error if fetching client fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        error: { error: 'Not found', status: 404 },
      });

      // Act
      await expect(() =>
        approveSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
          } as ApproveSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch client details, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: { clientId: 'clientId' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'approveSharingMutationFunction',
          input: { clientId: 'clientId' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message:
              'Failed to fetch client details, invalid response from back end.',
          }),
        },
        'NOT_FOUND - Failed to fetch client details, invalid response from back end.',
      );
    });

    it('throws an error if fetching client redirects fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        error: { error: 'Not found', status: 404 },
      });

      // Act
      await expect(() =>
        approveSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
          } as ApproveSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch client redirects, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: { clientId: 'clientId' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'approveSharingMutationFunction',
          input: { clientId: 'clientId' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message:
              'Failed to fetch client redirects, invalid response from back end.',
          }),
        },
        'NOT_FOUND - Failed to fetch client redirects, invalid response from back end.',
      );
    });

    it('throws an error if the input redirectUrl does not exist in the whitelisted client redirect urls', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      // Act
      await expect(() =>
        approveSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/unknown/redirect',
          } as ApproveSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to approve sharing, redirect url not allowed.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/unknown/redirect',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'approveSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/unknown/redirect',
          },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to approve sharing, redirect url not allowed.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to approve sharing, redirect url not allowed.',
      );
    });

    it('throws an error if create sharing fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      const createSharingMock = vi.mocked(createSharing, { partial: true });
      createSharingMock.mockResolvedValue({
        error: { error: 'Bad request', status: 400 },
      });

      // Act
      await expect(() =>
        approveSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          } as ApproveSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to approve sharing, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      //expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
      expect(createSharingMock).toHaveBeenCalledWith(
        'user',
        'clientId',
        'datasourceId',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'approveSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message:
              'Failed to approve sharing, invalid response from back end.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to approve sharing, invalid response from back end.',
      );
    });

    it('throws an error if create sharing fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      const createSharingMock = vi.mocked(createSharing, { partial: true });
      createSharingMock.mockResolvedValue({
        data: {
          _links: {},
          sharingId: 'sharingId',
          userId: 'userId',
          clientId: 'clientId',
          public: false,
          datasourceId: 'datasourceId',
          created: 'created',
          updated: 'updated',
        },
      });

      // Act
      const response = await approveSharingMutationFunction({
        ctx: {
          user: 'user',
        },
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
          foreignUserId: 'foreignUserId',
        } as ApproveSharingInputSchema,
      });

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      //expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
      expect(createSharingMock).toHaveBeenCalledWith(
        'user',
        'clientId',
        'datasourceId',
      );
      expect(response).toStrictEqual({
        redirectUrl:
          'http://example.com/known/redirect?sharingId=sharingId&state=foreignUserId&message=OK',
      });

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'approveSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
          foreignUserId: 'foreignUserId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('revokeSharingMutationFunction', () => {
    it('throws an error if "user" in context is undefined', async () => {
      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: undefined,
          },
          input: {} as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError('User is not authorized');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {},
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {},
          trpcError: new TRPCError({
            code: 'UNAUTHORIZED',
            message: 'User is not authorized',
          }),
        },
        'UNAUTHORIZED - User is not authorized',
      );
    });

    it('throws an error if fetching client fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        error: { error: 'Not found', status: 404 },
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch client details, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: { clientId: 'clientId' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: { clientId: 'clientId' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message:
              'Failed to fetch client details, invalid response from back end.',
          }),
        },
        'NOT_FOUND - Failed to fetch client details, invalid response from back end.',
      );
    });

    it('throws an error if fetching client redirect fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        error: { error: 'Not found', status: 404 },
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/return',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch client redirects, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/return',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/return',
          },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message:
              'Failed to fetch client redirects, invalid response from back end.',
          }),
        },
        'NOT_FOUND - Failed to fetch client redirects, invalid response from back end.',
      );
    });

    it('throws an error if the input redirectUrl does not exist in the whitelisted client redirect urls', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/unknown/redirect',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to revoke sharing, redirect url not allowed.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/unknown/redirect',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/unknown/redirect',
          },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to revoke sharing, redirect url not allowed.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to revoke sharing, redirect url not allowed.',
      );
    });

    it('throws an error if fetching datasource fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        error: { error: 'Not found', status: 404 },
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch datasource details, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message:
              'Failed to fetch datasource details, invalid response from back end.',
          }),
        },
        'NOT_FOUND - Failed to fetch datasource details, invalid response from back end.',
      );
    });

    it('throws an error if fetching user sharings fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
      getUserSharingsMock.mockResolvedValue({
        error: { error: 'Internal server error', status: 500 },
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to fetch userSharings details, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message:
              'Failed to fetch userSharings details, invalid response from back end.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to fetch userSharings details, invalid response from back end.',
      );
    });

    it('throws an error if revokation of a user sharing fails', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
      getUserSharingsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            sharingId: 'sharingId',
            datasourceId: 'datasourceId',
            userId: 'userId',
            clientId: 'clientId',
            public: false,
            created: 'created',
            updated: 'updated',
            expires: 'expires',
          },
        ],
      });

      const revokeSharingMock = vi.mocked(revokeSharing, { partial: true });
      revokeSharingMock.mockResolvedValue({
        error: { error: 'Internal server error', status: 500 },
      });

      // Act
      await expect(() =>
        revokeSharingMutationFunction({
          ctx: {
            user: 'user',
          },
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          } as RevokeSharingInputSchema,
        }),
      ).rejects.toThrowError(
        'Failed to revoke sharing, invalid response from back end.',
      );

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
      expect(revokeSharingMock).toHaveBeenCalledWith('user', 'sharingId');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'revokeSharingMutationFunction',
          input: {
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            datasourceId: 'datasourceId',
          },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message:
              'Failed to revoke sharing, invalid response from back end.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to revoke sharing, invalid response from back end.',
      );
    });

    it('responds with an object containing a redirect url if revokation was successful', async () => {
      // Arrange
      const getClientMock = vi.mocked(getClient, { partial: true });
      getClientMock.mockResolvedValue({
        data: {
          clientId: 'clientId',
        } as Mocked<components['schemas']['GetClientResponse']>,
      });

      const getClientRedirectsMock = vi.mocked(getClientRedirects, {
        partial: true,
      });
      getClientRedirectsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            redirectId: 'redirectId',
            clientId: 'clientId',
            redirectUrl: 'http://example.com/known/redirect',
            created: 'created',
            updated: 'updated',
            deleted: 'deleted',
          },
        ],
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
      getUserSharingsMock.mockResolvedValue({
        data: [
          {
            _links: {},
            sharingId: 'sharingId',
            datasourceId: 'datasourceId',
            userId: 'userId',
            clientId: 'clientId',
            public: false,
            created: 'created',
            updated: 'updated',
            expires: 'expires',
          },
        ],
      });

      const revokeSharingMock = vi.mocked(revokeSharing, { partial: true });
      revokeSharingMock.mockResolvedValue({});

      // Act
      const response = await revokeSharingMutationFunction({
        ctx: {
          user: 'user',
        },
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
          foreignUserId: 'foreignUserId',
        } as RevokeSharingInputSchema,
      });

      // Assert
      expect(getClientMock).toHaveBeenCalledWith('clientId');
      expect(getClientRedirectsMock).toHaveBeenCalledWith('clientId');
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
      expect(revokeSharingMock).toHaveBeenCalledWith('user', 'sharingId');
      expect(response).toStrictEqual({
        redirectUrl:
          'http://example.com/known/redirect?state=foreignUserId&message=Denied',
      });

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'revokeSharingMutationFunction',
        input: {
          clientId: 'clientId',
          redirectUrl: 'http://example.com/known/redirect',
          datasourceId: 'datasourceId',
          foreignUserId: 'foreignUserId',
        },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });
  });

  it('responds with an object containing an undefined redirect url if called with an undefined redirect url in the input', async () => {
    // Arrange
    const getClientMock = vi.mocked(getClient, { partial: true });
    getClientMock.mockResolvedValue({
      data: {
        clientId: 'clientId',
      } as Mocked<components['schemas']['GetClientResponse']>,
    });

    const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
    getDatasourceMock.mockResolvedValue({
      data: {
        _links: {},
        datasourceId: 'datasourceId',
        organizationId: 'organizationId',
        name: 'name',
        datatypeUrl: 'http://example.com/known/redirect',
        created: 'created',
        updated: 'updated',
      },
    });

    const getUserSharingsMock = vi.mocked(getUserSharings, { partial: true });
    getUserSharingsMock.mockResolvedValue({
      data: [
        {
          _links: {},
          sharingId: 'sharingId',
          datasourceId: 'datasourceId',
          userId: 'userId',
          clientId: 'clientId',
          public: false,
          created: 'created',
          updated: 'updated',
          expires: 'expires',
        },
      ],
    });

    const revokeSharingMock = vi.mocked(revokeSharing, { partial: true });
    revokeSharingMock.mockResolvedValue({});

    // Act
    const response = await revokeSharingMutationFunction({
      ctx: {
        user: 'user',
      },
      input: {
        clientId: 'clientId',
        redirectUrl: undefined,
        datasourceId: 'datasourceId',
        foreignUserId: undefined,
      } as RevokeSharingInputSchema,
    });

    // Assert
    expect(getClientMock).toHaveBeenCalledWith('clientId');
    expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
    expect(revokeSharingMock).toHaveBeenCalledWith('user', 'sharingId');
    expect(response).toStrictEqual({
      redirectUrl: undefined,
    });

    expect(logger.info).toHaveBeenCalledTimes(1);
    expect(logger.info).toHaveBeenCalledWith({
      function: 'revokeSharingMutationFunction',
      input: {
        clientId: 'clientId',
        redirectUrl: undefined,
        datasourceId: 'datasourceId',
        foreignUserId: undefined,
      },
    });
    expect(logger.error).toHaveBeenCalledTimes(0);
  });
});
