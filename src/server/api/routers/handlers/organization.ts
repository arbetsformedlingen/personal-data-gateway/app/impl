import { getOrganization, getOrganizationFaqs } from '@app/utils/backend';
import type {
  GetOrganizationFaqsInputSchema,
  GetOrganizationInputSchema,
} from '../schemas/organization';
import { createLogger } from '@app/utils/logger';
import { throwTrcpError } from './util';

const logger = createLogger('api/routers/handlers/organization');

export const getOrganizationQueryFunction = async ({
  input,
}: {
  input: GetOrganizationInputSchema;
}) => {
  logger.info({ function: 'getOrganizationQueryFunction', input });

  const { organizationId } = input;
  const response = await getOrganization(organizationId);
  if (response.error) {
    throwTrcpError(
      'getOrganizationQueryFunction',
      input,
      response.error.status,
      'Failed to fetch organization information.',
    );
  }

  const { data } = response;
  return data;
};

export const getOrganizationFaqsQueryFunction = async ({
  input,
}: {
  input: GetOrganizationFaqsInputSchema;
}) => {
  logger.info({ function: 'getOrganizationFaqsQueryFunction', input });

  const { organizationId } = input;
  const response = await getOrganizationFaqs(organizationId);

  if (response.error) {
    throwTrcpError(
      'getOrganizationFaqsQueryFunction',
      input,
      response.error.status,
      'Failed to fetch organization FAQs.',
    );
  }

  const { data } = response;
  return data;
};
