import {
  getDatasource,
  getDatasourceFaqs,
  getDatasourceUserData,
} from '@app/utils/backend';
import { afterEach, describe, expect, it, vi } from 'vitest';
import type { GetDatasourceInputSchema } from '../schemas/datasource';
import {
  getDatasourceFaqsQueryFunction,
  getDatasourceQueryFunction,
  getDatasourceUserDataQueryFunction,
} from './datasource';
import { type components } from '@dist/spec';
import { throwTrcpError } from '@app/server/api/routers/handlers/util';

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));
vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

vi.mock('@app/utils/backend');
vi.mock('../schemas/datasource');
vi.mock('@app/server/api/routers/handlers/util');

describe('datasource', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  describe('getDatasourceQueryFunction', () => {
    it('invokes throwTrpcError if getDatasource returns an object where "error"-property is defined', async () => {
      // Arrange
      const throwTrpcErrorMock = vi.mocked(throwTrcpError);
      throwTrpcErrorMock.mockImplementationOnce(() => {
        throw new Error('some error');
      });

      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        error: { error: "I'm a teapot", status: 418 },
      });

      // Act
      await expect(() =>
        getDatasourceQueryFunction({
          input: {
            datasourceId: 'datasourceId',
          } as GetDatasourceInputSchema,
        }),
      ).rejects.toThrowError('some error');

      // Assert
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');

      expect(throwTrpcErrorMock).toHaveBeenCalledTimes(1);
      expect(throwTrpcErrorMock).toHaveBeenCalledWith(
        'getDatasourceQueryFunction',
        {
          datasourceId: 'datasourceId',
        },
        418,
        'Failed to fetch datasource information.',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
    });

    it('responds with a datasource if successfully fetched from PDG API', async () => {
      // Arrange
      const getDatasourceMock = vi.mocked(getDatasource, { partial: true });
      getDatasourceMock.mockResolvedValue({
        data: {
          _links: {},
          datasourceId: 'datasourceId',
          organizationId: 'organizationId',
          name: 'name',
          datatypeUrl: 'http://example.com/known/redirect',
          created: 'created',
          updated: 'updated',
        },
      });

      // Act
      const response = await getDatasourceQueryFunction({
        input: {
          datasourceId: 'datasourceId',
        } as GetDatasourceInputSchema,
      });

      // Assert
      expect(getDatasourceMock).toHaveBeenCalledWith('datasourceId');
      expect(response).toStrictEqual({
        _links: {},
        datasourceId: 'datasourceId',
        organizationId: 'organizationId',
        name: 'name',
        datatypeUrl: 'http://example.com/known/redirect',
        created: 'created',
        updated: 'updated',
      });

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('getDatasourceFaqs', () => {
    it('returns values from getDatasourceFaqs response', async () => {
      // Arrange
      const getDatasourceFaqsMock = vi.mocked(getDatasourceFaqs, {
        partial: true,
      });
      getDatasourceFaqsMock.mockResolvedValueOnce({
        data: [
          {
            _links: {
              self: {
                href: 'https://example.com/foo',
              },
            },
            faqId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
            datasourceId: '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
            lang: 'sv',
            title: 'title',
            content: 'content',
            created: '2024-02-02T15:08:24Z',
            updated: '2024-02-02T15:08:24Z',
          },
        ],
      });

      // Act
      const response = await getDatasourceFaqsQueryFunction({
        input: {
          datasourceId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(getDatasourceFaqsMock).toBeCalledTimes(1);
      expect(getDatasourceFaqsMock).toBeCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.length).toBe(1);
      const faqArray =
        response as unknown as components['schemas']['GetDatasourceFaqsResponse'];
      const faq = faqArray[0];

      expect(faq?.faqId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(faq?.lang).toBe('sv');
      expect(faq?.title).toBe('title');
      expect(faq?.content).toBe('content');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceFaqsQueryFunction',
        input: { datasourceId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('invokes throwTrcpError when getDatatypeFaqs response error is defined', async () => {
      // Arrange
      const throwTrpcErrorMock = vi.mocked(throwTrcpError);
      throwTrpcErrorMock.mockImplementationOnce(() => {
        throw new Error('some error');
      });

      const getDatasourceFaqsMock = vi.mocked(getDatasourceFaqs, {
        partial: true,
      });
      getDatasourceFaqsMock.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getDatasourceFaqsQueryFunction({
          input: {
            datasourceId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('some error');

      expect(throwTrpcErrorMock).toHaveBeenCalledTimes(1);
      expect(throwTrpcErrorMock).toHaveBeenCalledWith(
        'getDatasourceFaqsQueryFunction',
        {
          datasourceId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
        404,
        'Failed to fetch datasource faqs.',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceFaqsQueryFunction',
        input: { datasourceId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
    });
  });

  describe('getDatasourceUserDataQueryFunction', () => {
    it('throws an error if "user" in context is undefined', async () => {
      // Arrange
      const throwTrpcErrorMock = vi.mocked(throwTrcpError);
      throwTrpcErrorMock.mockImplementationOnce(() => {
        throw new Error('some error');
      });

      // Act
      await expect(() =>
        getDatasourceUserDataQueryFunction({
          ctx: {
            user: undefined,
          },
          input: {
            datasourceId: 'datasourceId',
          },
        }),
      ).rejects.toThrowError('some error');

      expect(throwTrpcErrorMock).toHaveBeenCalledTimes(1);
      expect(throwTrpcErrorMock).toHaveBeenCalledWith(
        'getDatasourceUserDataQueryFunction',
        {
          datasourceId: 'datasourceId',
        },
        401,
        'User is not authorized',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceUserDataQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
    });

    it('throws an error if the preview request to PDG API fails with any status code other than 451', async () => {
      // Arrange
      const throwTrpcErrorMock = vi.mocked(throwTrcpError);
      throwTrpcErrorMock.mockImplementationOnce(() => {
        throw new Error('some error');
      });

      const getDatasourceUserDataMock = vi.mocked(getDatasourceUserData, {
        partial: true,
      });
      getDatasourceUserDataMock.mockResolvedValue({
        error: { status: 418, error: "I'm a teapot" },
      });

      // Act
      await expect(() =>
        getDatasourceUserDataQueryFunction({
          ctx: {
            user: 'user',
          },
          input: {
            datasourceId: 'datasourceId',
          },
        }),
      ).rejects.toThrowError('some error');

      // Assert
      expect(throwTrpcErrorMock).toHaveBeenCalledTimes(1);
      expect(throwTrpcErrorMock).toHaveBeenCalledWith(
        'getDatasourceUserDataQueryFunction',
        {
          datasourceId: 'datasourceId',
        },
        418,
        'Failed to fetch preview of sharing data, invalid response from back end.',
      );

      expect(getDatasourceUserDataMock).toHaveBeenCalledWith(
        'datasourceId',
        'user',
      );

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceUserDataQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
    });

    it('returns the error(status, code, message) to the caller if the preview request to PDG API fails with a status code 451', async () => {
      // Arrange
      const getDatasourceUserDataMock = vi.mocked(getDatasourceUserData, {
        partial: true,
      });
      getDatasourceUserDataMock.mockResolvedValue({
        error: { status: 451, error: 'Unavailable for Legal Reasons' },
      });

      // Act
      const response = await getDatasourceUserDataQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          datasourceId: 'datasourceId',
        },
      });

      // Assert
      expect(getDatasourceUserDataMock).toHaveBeenCalledWith(
        'datasourceId',
        'user',
      );
      expect(response).toStrictEqual({
        error: 'Unavailable for Legal Reasons',
        status: 451,
      });

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceUserDataQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('returns the data to the caller if the preview request to PDG API succeeds', async () => {
      // Arrange
      const getDatasourceUserDataMock = vi.mocked(getDatasourceUserData, {
        partial: true,
      });
      getDatasourceUserDataMock.mockResolvedValue({ data: 'data' });

      // Act
      const response = await getDatasourceUserDataQueryFunction({
        ctx: {
          user: 'user',
        },
        input: {
          datasourceId: 'datasourceId',
        },
      });

      // Assert
      expect(getDatasourceUserDataMock).toHaveBeenCalledWith(
        'datasourceId',
        'user',
      );
      expect(response).toBe('data');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatasourceUserDataQueryFunction',
        input: { datasourceId: 'datasourceId' },
      });
    });
  });
});
