import { afterEach, describe, expect, it, vi } from 'vitest';
import { getDatatype, getDatatypeFaqs } from '@app/utils/backend';
import {
  getDatatypeFaqsQueryFunction,
  getDatatypeQueryFunction,
} from './datatype';
import { TRPCError } from '@trpc/server';
import { type components } from '@dist/spec';

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));
vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

vi.mock('@app/utils/backend');
vi.mock('../schemas/datatype');

describe('datatype', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  describe('getDatatype', () => {
    it('returns values from getDatatype response', async () => {
      // Arrange
      const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
      getDatatypeMock.mockResolvedValueOnce({
        data: {
          _links: {
            self: {
              href: 'https://example.com/foo',
            },
          },
          datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          name: 'ABC',
          humanFriendlyName: 'human friandle name',
          humanFriendlyDescription: 'human friendly description',
          created: '2024-02-02T15:08:24Z',
          updated: '2024-02-02T15:08:24Z',
        },
      });

      // Act
      const response = await getDatatypeQueryFunction({
        input: {
          datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(getDatatypeMock).toBeCalledTimes(1);
      expect(getDatatypeMock).toBeCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.datatypeId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(response?.name).toBe('ABC');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeQueryFunction',
        input: { datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getDatatype response error is "404 Not Found"', async () => {
      // Arrange
      const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
      getDatatypeMock.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getDatatypeQueryFunction({
          input: {
            datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch datatype information.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeQueryFunction',
        input: { datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getDatatypeQueryFunction',
          input: { datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message: 'Failed to fetch datatype information.',
          }),
        },
        'NOT_FOUND - Failed to fetch datatype information.',
      );
    });

    it('throws TRPCError when getDatatype response error is other than "404 Not found"', async () => {
      // Arrange
      const getDatatypeMock = vi.mocked(getDatatype, { partial: true });
      getDatatypeMock.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getDatatypeQueryFunction({
          input: {
            datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch datatype information.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeQueryFunction',
        input: { datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getDatatypeQueryFunction',
          input: { datatypeUrl: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to fetch datatype information.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to fetch datatype information.',
      );
    });
  });

  describe('getDatatypeFaqs', () => {
    it('returns values from getDatatypeFaqs response', async () => {
      // Arrange
      const getDatatypeFaqsMock = vi.mocked(getDatatypeFaqs, { partial: true });
      getDatatypeFaqsMock.mockResolvedValueOnce({
        data: [
          {
            _links: {
              self: {
                href: 'https://example.com/foo',
              },
            },
            faqId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
            datatypeId: '2e3f741d-aa84-40e8-90e5-999b0a6531f6',
            lang: 'sv',
            title: 'title',
            content: 'content',
            created: '2024-02-02T15:08:24Z',
            updated: '2024-02-02T15:08:24Z',
          },
        ],
      });

      // Act
      const response = await getDatatypeFaqsQueryFunction({
        input: {
          datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
        },
      });

      // Assert
      expect(getDatatypeFaqsMock).toBeCalledTimes(1);
      expect(getDatatypeFaqsMock).toBeCalledWith(
        '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
      );
      expect(response?.length).toBe(1);
      const faqArray =
        response as unknown as components['schemas']['GetDatatypeFaqResponse'][];
      const faq = faqArray[0];

      expect(faq?.faqId).toBe('1e3f741d-aa84-40e8-90e5-999b0a6531f6');
      expect(faq?.lang).toBe('sv');
      expect(faq?.title).toBe('title');
      expect(faq?.content).toBe('content');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeFaqsQueryFunction',
        input: { datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(0);
    });

    it('throws TRPCError when getDatatypeFaqs response error is "404 Not Found"', async () => {
      // Arrange
      const getDatatypeFaqsMock = vi.mocked(getDatatypeFaqs, { partial: true });
      getDatatypeFaqsMock.mockResolvedValueOnce({
        error: {
          status: 404,
          error: 'Not Found',
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getDatatypeFaqsQueryFunction({
          input: {
            datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch datatype FAQs.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeFaqsQueryFunction',
        input: { datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getDatatypeFaqsQueryFunction',
          input: { datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'NOT_FOUND',
            message: 'Failed to fetch datatype FAQs.',
          }),
        },
        'NOT_FOUND - Failed to fetch datatype FAQs.',
      );
    });

    it('throws TRPCError when getDatatype response error is other than "404 Not found"', async () => {
      // Arrange
      const getDatatypeFaqsMock = vi.mocked(getDatatypeFaqs, { partial: true });
      getDatatypeFaqsMock.mockResolvedValueOnce({
        error: {
          status: 418,
          error: "I'm a teapot",
          message: 'Error message',
        },
      });

      // Act & Assert
      await expect(() =>
        getDatatypeFaqsQueryFunction({
          input: {
            datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6',
          },
        }),
      ).rejects.toThrowError('Failed to fetch datatype FAQs.');

      expect(logger.info).toHaveBeenCalledTimes(1);
      expect(logger.info).toHaveBeenCalledWith({
        function: 'getDatatypeFaqsQueryFunction',
        input: { datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
      });
      expect(logger.error).toHaveBeenCalledTimes(1);
      expect(logger.error).toHaveBeenCalledWith(
        {
          function: 'getDatatypeFaqsQueryFunction',
          input: { datatypeId: '1e3f741d-aa84-40e8-90e5-999b0a6531f6' },
          trpcError: new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: 'Failed to fetch datatype FAQs.',
          }),
        },
        'INTERNAL_SERVER_ERROR - Failed to fetch datatype FAQs.',
      );
    });
  });
});
