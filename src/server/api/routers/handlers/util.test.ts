import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { isActive, throwTrcpError } from './util';
import { TRPCError } from '@trpc/server';

const { logger } = vi.hoisted(() => ({
  logger: {
    debug: vi.fn(),
    info: vi.fn(),
    warn: vi.fn(),
    error: vi.fn(),
    fatal: vi.fn(),
  },
}));

vi.mock('@app/utils/logger', () => ({ createLogger: () => logger }));

describe('isActive', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('returns true if sharing not yet expired or revoked', () => {
    // Arrange
    const revoked = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const expires = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(revoked.toISOString(), expires.toISOString());

    // Assert
    expect(result).toBeTruthy();
  });

  it('returns false if sharing has been revoked', () => {
    // Arrange
    const revoked = new Date(Date.UTC(2024, 1, 1, 10, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(revoked.toISOString(), undefined);

    // Assert
    expect(result).toBeFalsy();
  });

  it('returns false if sharing has expired', () => {
    // Arrange
    const expires = new Date(Date.UTC(2024, 1, 1, 10, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(undefined, expires.toISOString());

    // Assert
    expect(result).toBeFalsy();
  });

  it('returns true if sharing has not been revoked or expired', () => {
    // Act
    const result = isActive(undefined, undefined);

    // Assert
    expect(result).toBeTruthy();
  });
});

describe('throwTrcpError', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  it('Should throw trpcError with error code UNAUTHORIZED if called with status 401', () => {
    // Act, Assert
    expect(() =>
      throwTrcpError('function', 'some input', 401, 'message'),
    ).toThrowError();

    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'function',
        input: 'some input',
        trpcError: new TRPCError({ code: 'UNAUTHORIZED', message: 'message' }),
      },
      'UNAUTHORIZED - message',
    );
  });

  it('Should throw trpcError with error code FORBIDDEN if called with status 403', () => {
    // Act, Assert
    expect(() =>
      throwTrcpError('function', 'some input', 403, 'message'),
    ).toThrowError();

    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'function',
        input: 'some input',
        trpcError: new TRPCError({ code: 'FORBIDDEN', message: 'message' }),
      },
      'FORBIDDEN - message',
    );
  });

  it('Should throw trpcError with error code NOT_FOUND if called with status 401', () => {
    // Act, Assert
    expect(() =>
      throwTrcpError('function', 'some input', 404, 'message'),
    ).toThrowError();

    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'function',
        input: 'some input',
        trpcError: new TRPCError({ code: 'NOT_FOUND', message: 'message' }),
      },
      'NOT_FOUND - message',
    );
  });

  it('Should throw trpcError with error code INTERNAL_SERVER_ERROR if called with status 401', () => {
    // Act, Assert
    expect(() =>
      throwTrcpError('function', 'some input', 418, 'message'),
    ).toThrowError();

    expect(logger.error).toHaveBeenCalledTimes(1);
    expect(logger.error).toHaveBeenCalledWith(
      {
        function: 'function',
        input: 'some input',
        trpcError: new TRPCError({
          code: 'INTERNAL_SERVER_ERROR',
          message: 'message',
        }),
      },
      'INTERNAL_SERVER_ERROR - message',
    );
  });
});
