import { getDatatype, getDatatypeFaqs } from '@app/utils/backend';
import type {
  GetDatatypeFaqsInputSchema,
  GetDatatypeInputSchema,
} from '../schemas/datatype';
import { createLogger } from '@app/utils/logger';
import { throwTrcpError } from './util';

const logger = createLogger('api/routers/handlers/datatype');

export const getDatatypeQueryFunction = async ({
  input,
}: {
  input: GetDatatypeInputSchema;
}) => {
  logger.info({ function: 'getDatatypeQueryFunction', input });

  const { datatypeUrl } = input;
  const response = await getDatatype(datatypeUrl);

  if (response.error) {
    throwTrcpError(
      'getDatatypeQueryFunction',
      input,
      response.error.status,
      'Failed to fetch datatype information.',
    );
  }

  const { data } = response;
  return data;
};

export const getDatatypeFaqsQueryFunction = async ({
  input,
}: {
  input: GetDatatypeFaqsInputSchema;
}) => {
  logger.info({ function: 'getDatatypeFaqsQueryFunction', input });

  const { datatypeId } = input;
  const response = await getDatatypeFaqs(datatypeId);

  if (response.error) {
    throwTrcpError(
      'getDatatypeFaqsQueryFunction',
      input,
      response.error.status,
      'Failed to fetch datatype FAQs.',
    );
  }
  const { data } = response;
  return data;
};
