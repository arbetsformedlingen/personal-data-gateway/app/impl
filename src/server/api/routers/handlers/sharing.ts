import {
  createSharing,
  getClient,
  getClientRedirects,
  getDataAccesses,
  getDatasource,
  getUserSharing,
  getUserSharings,
  revokeSharing,
} from '@app/utils/backend';
import type { CreateContextOptions } from '../../trpc';
import type {
  GetDataAccessesInputSchema,
  GetUserSharingInputSchema,
  GetUserSharingsInputSchema,
  ApproveSharingInputSchema,
  RevokeSharingInputSchema,
} from '../schemas/sharing';
import { createLogger } from '@app/utils/logger';
import { isActive, throwTrcpError } from './util';

const logger = createLogger('api/routers/handlers/sharing');

const getClientName = async (clientId: string) => {
  const client = await getClient(clientId);

  if (client.error)
    throwTrcpError(
      'getClientName',
      clientId,
      client.error.status,
      'Failed to fetch client details, invalid response from back end.',
    );

  return client.data!.name;
};

export const getUserSharingsQueryFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: GetUserSharingsInputSchema;
}) => {
  logger.info({ function: 'getUserSharingsQueryFunction', input });

  const user = ctx.user;
  if (!ctx.user)
    throwTrcpError(
      'getUserSharingsQueryFunction',
      input,
      401,
      'User is not authorized',
    );

  const userSharingsResponse = await getUserSharings(user!);
  if (userSharingsResponse.error)
    throwTrcpError(
      'getUserSharingsQueryFunction',
      input,
      userSharingsResponse.error.status,
      'Failed to fetch userSharings details, invalid response from back end.',
    );
  const userSharings = userSharingsResponse.data!;

  const clientUserSharings = input.clientId
    ? userSharings.filter(
        (userSharing) => userSharing.clientId === input.clientId,
      )
    : userSharings;
  const datasourceUserSharings = input.datasourceId
    ? clientUserSharings.filter(
        (userSharing) => userSharing.datasourceId === input.datasourceId,
      )
    : clientUserSharings;
  const activeUserSharings = input.isActive
    ? datasourceUserSharings.filter((userSharing) =>
        isActive(userSharing.revoked, userSharing.expires),
      )
    : datasourceUserSharings;

  return await Promise.all(
    activeUserSharings.map(async (sharing) => {
      const clientName = await (sharing.clientId != undefined
        ? getClientName(sharing.clientId)
        : Promise.resolve('<publikt tillgänglig>'));

      const datasource = await getDatasource(sharing.datasourceId);

      if (datasource.error) {
        throwTrcpError(
          'getUserSharingsQueryFunction',
          input,
          datasource.error.status,
          'Failed to fetch datasource details, invalid response from back end.',
        );
      }

      const datasourceName = datasource.data?.name;

      return {
        unmapped: sharing,
        sharingId: sharing.sharingId,
        clientName: clientName,
        datasourceName,
        created: sharing.created,
        revoked: sharing.revoked,
        expires: sharing.expires,
      };
    }),
  );
};

export const getUserSharingQueryFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: GetUserSharingInputSchema;
}) => {
  logger.info({ function: 'getUserSharingQueryFunction', input });

  const user = ctx.user;
  if (!ctx.user)
    throwTrcpError(
      'getUserSharingQueryFunction',
      input,
      401,
      'User is not authorized',
    );

  const userSharingResponse = await getUserSharing(user!, input.sharingId);
  if (userSharingResponse.error) {
    throwTrcpError(
      'getUserSharingQueryFunction',
      input,
      userSharingResponse.error.status,
      'Failed to fetch userSharing, invalid response from back end.',
    );
  }
  const userSharing = userSharingResponse.data!;

  const datasourceId = userSharing.datasourceId;
  const datasourceResponse = await getDatasource(datasourceId);
  if (datasourceResponse.error)
    throwTrcpError(
      'getUserSharingQueryFunction',
      input,
      datasourceResponse.error.status,
      'Failed to fetch datasource details, invalid response from back end.',
    );

  return {
    unmapped: userSharing,
    sharingId: userSharing.sharingId,
    clientId: userSharing.clientId,
    datasourceId: userSharing.datasourceId,
    created: userSharing.created,
    revoked: userSharing.revoked,
    expires: userSharing.expires,
  };
};

export const getDataAccessesQueryFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: GetDataAccessesInputSchema;
}) => {
  logger.info({ function: 'getDataAccessesQueryFunction', input });

  const user = ctx.user;
  if (!ctx.user)
    throwTrcpError(
      'getDataAccessesQueryFunction',
      input,
      401,
      'User is not authorized',
    );

  const userSharingResponse = await getUserSharing(user!, input.sharingId);
  if (userSharingResponse.error)
    throwTrcpError(
      'getDataAccessesQueryFunction',
      input,
      userSharingResponse.error.status,
      'Failed to fetch userSharing details, invalid response from back end.',
    );
  const userSharing = userSharingResponse.data!;

  if (userSharing.userId !== user)
    throwTrcpError(
      'getDataAccessesQueryFunction',
      input,
      500,
      'Sharings userId does not match actual userId',
    );

  const dataAccessesResponse = await getDataAccesses(input.sharingId);
  if (dataAccessesResponse.error)
    throwTrcpError(
      'getDataAccessesQueryFunction',
      input,
      dataAccessesResponse.error.status,
      'Failed to fetch data accesses, invalid response from back end.',
    );

  return dataAccessesResponse.data! ?? [];
};

export const approveSharingMutationFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: ApproveSharingInputSchema;
}) => {
  logger.info({ function: 'approveSharingMutationFunction', input });

  if (!ctx.user)
    throwTrcpError(
      'approveSharingMutationFunction',
      input,
      401,
      'User is not authorized',
    );

  const client = await getClient(input.clientId);

  if (client.error)
    throwTrcpError(
      'approveSharingMutationFunction',
      input,
      client.error.status,
      'Failed to fetch client details, invalid response from back end.',
    );

  const redirects = await getClientRedirects(client.data!.clientId);

  if (redirects.error)
    throwTrcpError(
      'approveSharingMutationFunction',
      input,
      redirects.error.status,
      'Failed to fetch client redirects, invalid response from back end.',
    );

  const clientHasInputRedirect =
    redirects.data!.filter(
      (redirect) => redirect.redirectUrl === input.redirectUrl,
    ).length > 0;

  if (!clientHasInputRedirect)
    throwTrcpError(
      'approveSharingMutationFunction',
      input,
      500,
      'Failed to approve sharing, redirect url not allowed.',
    );

  const sharing = await createSharing(
    ctx.user!,
    input.clientId,
    input.datasourceId,
  );

  if (sharing.error)
    throwTrcpError(
      'approveSharingMutationFunction',
      input,
      sharing.error.status,
      'Failed to approve sharing, invalid response from back end.',
    );

  // Add parameters to provided redirectUrl.
  const redirectUrl = new URL(input.redirectUrl);
  redirectUrl.searchParams.set('sharingId', sharing.data!.sharingId);
  redirectUrl.searchParams.set('state', input.foreignUserId);
  redirectUrl.searchParams.set('message', 'OK');

  const rv = {
    redirectUrl: redirectUrl.toString(),
  };
  return rv;
};

export const revokeSharingMutationFunction = async ({
  ctx,
  input,
}: {
  ctx: CreateContextOptions;
  input: RevokeSharingInputSchema;
}) => {
  logger.info({ function: 'revokeSharingMutationFunction', input });

  if (!ctx.user)
    throwTrcpError(
      'revokeSharingMutationFunction',
      input,
      401,
      'User is not authorized',
    );

  const client = await getClient(input.clientId);

  if (client.error)
    throwTrcpError(
      'revokeSharingMutationFunction',
      input,
      client.error.status,
      'Failed to fetch client details, invalid response from back end.',
    );

  if (input.redirectUrl !== undefined) {
    const redirects = await getClientRedirects(client.data!.clientId);

    if (redirects.error)
      throwTrcpError(
        'revokeSharingMutationFunction',
        input,
        redirects.error.status,
        'Failed to fetch client redirects, invalid response from back end.',
      );

    const clientHasInputRedirect =
      redirects.data!.filter(
        (redirect) => redirect.redirectUrl === input.redirectUrl,
      ).length > 0;

    if (!clientHasInputRedirect)
      throwTrcpError(
        'revokeSharingMutationFunction',
        input,
        500,
        'Failed to revoke sharing, redirect url not allowed.',
      );
  }

  const datasource = await getDatasource(input.datasourceId);

  if (datasource.error)
    throwTrcpError(
      'revokeSharingMutationFunction',
      input,
      datasource.error.status,
      'Failed to fetch datasource details, invalid response from back end.',
    );

  const userSharings = await getUserSharings(ctx.user!);

  if (userSharings.error)
    throwTrcpError(
      'revokeSharingMutationFunction',
      input,
      userSharings.error.status,
      'Failed to fetch userSharings details, invalid response from back end.',
    );

  const userSharingsToRevoke = userSharings.data!.reduce(
    (acc: typeof userSharings.data, userSharing) => {
      if (
        userSharing.revoked === undefined &&
        userSharing.datasourceId === datasource.data!.datasourceId &&
        userSharing.clientId === input.clientId
      ) {
        acc!.push(userSharing);
      }
      return acc;
    },
    [],
  );

  for (const userSharing of userSharingsToRevoke!) {
    const sharing = await revokeSharing(ctx.user!, userSharing.sharingId);

    if (sharing.error)
      throwTrcpError(
        'revokeSharingMutationFunction',
        input,
        sharing.error.status,
        'Failed to revoke sharing, invalid response from back end.',
      );
  }

  if (input.redirectUrl != undefined) {
    // Add parameters to provided redirectUrl
    const redirectUrl = new URL(input.redirectUrl);
    if (input.foreignUserId != undefined)
      redirectUrl.searchParams.set('state', input.foreignUserId);
    redirectUrl.searchParams.set('message', 'Denied');

    return {
      redirectUrl: redirectUrl.toString(),
    };
  } else {
    return {
      redirectUrl: undefined,
    };
  }
};
