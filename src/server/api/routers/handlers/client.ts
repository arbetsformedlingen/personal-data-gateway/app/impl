import { getClient, getClientRedirects } from '@app/utils/backend';
import type {
  GetClientInputSchema,
  GetClientRedirectsInputSchema,
} from '../schemas/client';
import { createLogger } from '@app/utils/logger';
import { isActive, throwTrcpError } from './util';

const logger = createLogger('api/routers/handlers/client');

export const getClientQueryFunction = async ({
  input,
}: {
  input: GetClientInputSchema;
}) => {
  logger.info({ function: 'getClientQueryFunction', input });

  const response = await getClient(input.clientId);

  if (response.error)
    throwTrcpError(
      'getClient',
      input.clientId,
      response.error.status,
      'Failed to fetch client details.',
    );
  else {
    if (!isActive(response.data.revoked))
      throwTrcpError('getClient', input.clientId, 404, 'Client is revoked');

    const rv = {
      clientId: response.data.clientId,
      name: response.data.name,
      organizationId: response.data.organizationId,
    };
    return rv;
  }
};

export const getClientRedirectsQueryFunction = async ({
  input,
}: {
  input: GetClientRedirectsInputSchema;
}) => {
  logger.info({ function: 'getClientRedirectsQueryFunction', input });

  const response = await getClientRedirects(input.clientId);

  if (response.error)
    throwTrcpError(
      'getClientRedirects',
      input.clientId,
      response.error.status,
      'Failed to fetch client redirects.',
    );
  else {
    const { data } = response;
    return data;
  }
};
