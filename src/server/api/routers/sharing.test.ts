import { describe, expect, it, vi, beforeEach } from 'vitest';
import { sharingRouterFactory } from './sharing';

const { createTRPCRouter, privateProcedure } = vi.hoisted(() => ({
  createTRPCRouter: vi.fn().mockReturnValue('trpc router'),
  privateProcedure: {
    input: vi.fn().mockReturnThis(),
    query: vi.fn().mockReturnValueOnce('getUserSharings query result'),
    mutation: vi
      .fn()
      .mockReturnValueOnce('approveSharing mutation result')
      .mockReturnValueOnce('revokeSharing mutation result'),
  },
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCRouter,
  privateProcedure,
}));

const {
  getUserSharingsInputSchema,
  getUserSharingsQueryFunction,
  getUserSharingInputSchema,
  getUserSharingQueryFunction,
  getDataAccessesInputSchema,
  getDataAccessesQueryFunction,
  approveSharingInputSchema,
  revokeSharingInputSchema: revokeSharingInputSchema,
  approveSharingMutationFunction: approveSharingMutationFunction,
  revokeSharingMutationFunction: revokeSharingMutationFunction,
} = vi.hoisted(() => ({
  getUserSharingsInputSchema: 'getUserSharingsInputSchema',
  getUserSharingInputSchema: 'getUserSharingInputSchema',
  getDataAccessesInputSchema: 'getDataAccessesInputSchema',
  approveSharingInputSchema: 'approveSharingInputSchema',
  revokeSharingInputSchema: 'revokeSharingInputSchema',
  getUserSharingsQueryFunction: vi.fn(),
  getUserSharingQueryFunction: vi.fn(),
  getDataAccessesQueryFunction: vi.fn(),
  approveSharingMutationFunction: vi.fn(),
  revokeSharingMutationFunction: vi.fn(),
}));

vi.mock('./schemas/sharing', () => ({
  getUserSharingsInputSchema,
  getUserSharingInputSchema,
  getDataAccessesInputSchema,
  approveSharingInputSchema,
  revokeSharingInputSchema: revokeSharingInputSchema,
}));
vi.mock('./handlers/sharing', () => ({
  getUserSharingsQueryFunction,
  getUserSharingQueryFunction,
  getDataAccessesQueryFunction,
  approveSharingMutationFunction: approveSharingMutationFunction,
  revokeSharingMutationFunction: revokeSharingMutationFunction,
}));

vi.mock('@app/utils/backend');

describe('sharing', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  describe('sharingRouterFactory', () => {
    it('returns a router', () => {
      // Act
      const result = sharingRouterFactory();

      // Assert
      expect(privateProcedure.input).toHaveBeenCalledWith(
        approveSharingInputSchema,
      );
      expect(privateProcedure.input).toHaveBeenCalledWith(
        revokeSharingInputSchema,
      );
      expect(privateProcedure.mutation).toHaveBeenCalledWith(
        approveSharingMutationFunction,
      );
      expect(privateProcedure.mutation).toHaveBeenCalledWith(
        revokeSharingMutationFunction,
      );
      expect(createTRPCRouter).toHaveBeenCalledWith({
        getUserSharings: 'getUserSharings query result',
        approveSharing: 'approveSharing mutation result',
        revokeSharing: 'revokeSharing mutation result',
      });
      expect(result).toBe('trpc router');
    });
  });
});
