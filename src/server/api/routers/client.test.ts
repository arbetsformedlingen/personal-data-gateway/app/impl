/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { describe, expect, it, vi, beforeEach } from 'vitest';
import { clientRouterFactory } from './client';

const { createTRPCRouter, publicProcedure } = vi.hoisted(() => ({
  createTRPCRouter: vi.fn().mockReturnValue('trpc router'),
  publicProcedure: {
    input: vi.fn().mockReturnThis(),
    query: vi
      .fn()
      .mockReturnValueOnce('getClientQueryFunction')
      .mockReturnValueOnce('getClientRedirectsQueryFunction'),
  },
}));

vi.mock('@app/server/api/trpc', () => ({
  createTRPCRouter,
  publicProcedure,
}));

const {
  getClientInputSchema,
  getClientQueryFunction,
  getClientRedirectsInputSchema,
  getClientRedirectsQueryFunction,
} = vi.hoisted(() => ({
  getClientInputSchema: 'getClientInputSchema',
  getClientQueryFunction: vi.fn(),
  getClientRedirectsInputSchema: 'getClientRedirectsInputSchema',
  getClientRedirectsQueryFunction: vi.fn(),
}));

vi.mock('./schemas/client', () => ({
  getClientInputSchema,
  getClientRedirectsInputSchema,
}));
vi.mock('./handlers/client', () => ({
  getClientQueryFunction,
  getClientRedirectsQueryFunction,
}));

vi.mock('@app/utils/backend');

describe('client', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('runs', () => {
    // Act
    const result = clientRouterFactory();

    // Assert
    expect(publicProcedure.input).toHaveBeenCalledWith('getClientInputSchema');
    expect(publicProcedure.input).toHaveBeenCalledWith(
      'getClientRedirectsInputSchema',
    );
    expect(publicProcedure.query).toHaveBeenCalledWith(getClientQueryFunction);
    expect(publicProcedure.query).toHaveBeenCalledWith(
      getClientRedirectsQueryFunction,
    );
    expect(createTRPCRouter).toHaveBeenCalledWith({
      getClient: 'getClientQueryFunction',
      getClientRedirects: 'getClientRedirectsQueryFunction',
    });
    expect(result).toBe('trpc router');
  });
});
