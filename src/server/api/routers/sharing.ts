import { createTRPCRouter, privateProcedure } from '@app/server/api/trpc';
import {
  getDataAccessesInputSchema,
  getUserSharingInputSchema,
  getUserSharingsInputSchema,
  approveSharingInputSchema,
  revokeSharingInputSchema,
} from './schemas/sharing';
import {
  getDataAccessesQueryFunction,
  getUserSharingQueryFunction,
  getUserSharingsQueryFunction,
  approveSharingMutationFunction,
  revokeSharingMutationFunction,
} from './handlers/sharing';

export const sharingRouterFactory = () =>
  createTRPCRouter({
    getUserSharings: privateProcedure
      .input(getUserSharingsInputSchema)
      .query(getUserSharingsQueryFunction),

    getUserSharing: privateProcedure
      .input(getUserSharingInputSchema)
      .query(getUserSharingQueryFunction),

    getDataAccesses: privateProcedure
      .input(getDataAccessesInputSchema)
      .query(getDataAccessesQueryFunction),

    approveSharing: privateProcedure
      .input(approveSharingInputSchema)
      .mutation(approveSharingMutationFunction),

    revokeSharing: privateProcedure
      .input(revokeSharingInputSchema)
      .mutation(revokeSharingMutationFunction),
  });
