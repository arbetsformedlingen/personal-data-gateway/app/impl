import { describe, expect, it, vi, beforeEach } from 'vitest';
import { datasourceRouterFactory } from './datasource';

const { createTRPCRouter, publicProcedure, privateProcedure } = vi.hoisted(
  () => ({
    createTRPCRouter: vi.fn().mockReturnValue('trpc router'),
    publicProcedure: {
      input: vi.fn().mockReturnThis(),
      query: vi.fn().mockReturnValue('publicProcedure query result'),
    },
    privateProcedure: {
      input: vi.fn().mockReturnThis(),
      query: vi.fn().mockReturnValue('privateProcedure query result'),
    },
  }),
);

vi.mock('@app/server/api/trpc', () => ({
  createTRPCRouter,
  publicProcedure,
  privateProcedure,
}));

const {
  getDatasourceInputSchema,
  getDatasourceQueryFunction,
  getDatasourceFaqsInputSchema,
  getDatasourceFaqsQueryFunction,
  getDatasourceUserDataInputSchema,
  getDatasourceUserDataQueryFunction,
} = vi.hoisted(() => ({
  getDatasourceInputSchema: 'getDatasourceInputSchema',
  getDatasourceQueryFunction: vi.fn(),
  getDatasourceFaqsInputSchema: 'getDatasourceFaqsInputSchema',
  getDatasourceFaqsQueryFunction: vi.fn(),
  getDatasourceUserDataInputSchema: 'getDatasourceUserDataInputSchema',
  getDatasourceUserDataQueryFunction: vi.fn(),
}));

vi.mock('./schemas/datasource', () => ({
  getDatasourceInputSchema,
  getDatasourceFaqsInputSchema,
  getDatasourceUserDataInputSchema,
}));
vi.mock('./handlers/datasource', () => ({
  getDatasourceQueryFunction,
  getDatasourceFaqsQueryFunction,
  getDatasourceUserDataQueryFunction,
}));

vi.mock('@app/utils/backend');

describe('sharing', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  describe('datasourceRouterFactory', () => {
    it('returns a router', () => {
      // Act
      const result = datasourceRouterFactory();

      // Assert
      expect(publicProcedure.input).toHaveBeenCalledTimes(2);
      expect(publicProcedure.input).toHaveBeenCalledWith(
        getDatasourceInputSchema,
      );
      expect(publicProcedure.input).toHaveBeenCalledWith(
        getDatasourceFaqsInputSchema,
      );

      expect(publicProcedure.query).toHaveBeenCalledTimes(2);
      expect(publicProcedure.query).toHaveBeenCalledWith(
        getDatasourceQueryFunction,
      );
      expect(publicProcedure.query).toHaveBeenCalledWith(
        getDatasourceFaqsQueryFunction,
      );

      expect(privateProcedure.input).toHaveBeenCalledTimes(1);
      expect(privateProcedure.input).toHaveBeenCalledWith(
        getDatasourceUserDataInputSchema,
      );

      expect(privateProcedure.query).toHaveBeenCalledTimes(1);
      expect(privateProcedure.query).toHaveBeenCalledWith(
        getDatasourceUserDataQueryFunction,
      );

      expect(createTRPCRouter).toHaveBeenCalledWith({
        getDatasource: 'publicProcedure query result',
        getDatasourceFaqs: 'publicProcedure query result',
        getDatasourceUserData: 'privateProcedure query result',
      });
      expect(result).toBe('trpc router');
    });
  });
});
