import { clientRouterFactory } from './routers/client';
import { datasourceRouterFactory } from './routers/datasource';
import { datatypeRouterFactory } from './routers/datatype';
import { organizationRouterFactory } from './routers/organization';
import { sharingRouterFactory } from './routers/sharing';
import { createTRPCRouter } from './trpc';

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const createAppRouter = () =>
  createTRPCRouter({
    client: clientRouterFactory(),
    datasource: datasourceRouterFactory(),
    datatype: datatypeRouterFactory(),
    organization: organizationRouterFactory(),
    sharing: sharingRouterFactory(),
  });

// export type definition of API
export type AppRouter = ReturnType<typeof createAppRouter>;
