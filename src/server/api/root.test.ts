// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import { createTRPCRouter } from './trpc';

import { createAppRouter } from './root';

const {
  sharingRouterMock,
  clientRouterMock,
  datasourceRouterMock,
  datatypeRouterMock,
  organizationRouterMock,
} = vi.hoisted(() => ({
  clientRouterMock: { id: 'example service router' },
  datasourceRouterMock: { id: 'example datasource router' },
  datatypeRouterMock: { id: 'example datatype router' },
  organizationRouterMock: { id: 'example organization router' },
  sharingRouterMock: { id: 'example sharing router' },
}));

vi.mock('./trpc');
vi.mock('./routers/client', () => ({
  clientRouterFactory: () => clientRouterMock,
}));
vi.mock('./routers/datasource', () => ({
  datasourceRouterFactory: () => datasourceRouterMock,
}));
vi.mock('./routers/datatype', () => ({
  datatypeRouterFactory: () => datatypeRouterMock,
}));
vi.mock('./routers/organization', () => ({
  organizationRouterFactory: () => organizationRouterMock,
}));
vi.mock('./routers/sharing', () => ({
  sharingRouterFactory: () => sharingRouterMock,
}));

describe('AppRouter', () => {
  it('initializes root', () => {
    vi.mocked(createTRPCRouter, { partial: true }).mockReturnValue({});

    createAppRouter();

    expect(createTRPCRouter).toBeCalledWith({
      client: clientRouterMock,
      datasource: datasourceRouterMock,
      datatype: datatypeRouterMock,
      organization: organizationRouterMock,
      sharing: sharingRouterMock,
    });
  });
});
