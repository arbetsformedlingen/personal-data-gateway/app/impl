// @vitest-environment jsdom
import { describe, expect, it, type Mocked, vi } from 'vitest';
import { screen, render } from '@testing-library/react';
import DataPreview from './DataPreview';
import type { components } from '@dist/spec';
import useDatasource from '@app/hooks/useDatasource';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

vi.mock('@digi/arbetsformedlingen-react', () => ({
  DigiLayoutBlock: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
  DigiLayoutContainer: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
  DigiTypography: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
}));

vi.mock('@app/hooks/useDatasource');
vi.mock('@app/hooks/useDatatype');

describe('DataPreview', () => {
  it('renders datasource name when datasource.name is present', () => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {
        name: 'example-datasource-name',
      } as Mocked<components['schemas']['GetDatasourceResponse']>,
    });

    render(
      <DataPreview
        datasourceId="example-source-id"
        previewIsLoading={false}
        preview={'example-data'}
      />,
    );
    const component = screen.getByTestId('datasourceName');
    expect(component.innerHTML).toEqual('example-datasource-name');
  });

  it('renders data when data is present', () => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as Mocked<components['schemas']['GetDatasourceResponse']>,
    });

    render(
      <DataPreview
        datasourceId="example-source-id"
        previewIsLoading={false}
        preview={'example-data'}
      />,
    );
    const component = screen.getByTestId('previewData');
    expect(component.innerHTML).toEqual('example-data');
  });

  it('renders ... when data is not present', () => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as Mocked<components['schemas']['GetDatasourceResponse']>,
    });

    render(
      <DataPreview
        datasourceId="example-source-id"
        previewIsLoading={true}
        preview={undefined}
      />,
    );
    const component = screen.getByTestId('previewData');
    expect(component.innerHTML).toEqual('Laddar...');
  });

  it('renders error when there is an error', () => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as Mocked<components['schemas']['GetDatasourceResponse']>,
    });

    render(
      <DataPreview
        datasourceId="example-source-id"
        previewIsLoading={false}
        preview={undefined}
      />,
    );
    const component = screen.getByTestId('previewData');
    expect(component.innerHTML).toEqual('example-load-data-preview-error');
  });

  it('renders "example-data-render-error" when fetched previewData is defined but not a string', () => {
    vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as Mocked<components['schemas']['GetDatasourceResponse']>,
    });

    render(
      <DataPreview
        datasourceId="example-source-id"
        previewIsLoading={false}
        preview={{} as Blob}
      />,
    );
    const component = screen.getByTestId('previewData');
    expect(component.innerHTML).toEqual('example-data-render-error');
  });
});
