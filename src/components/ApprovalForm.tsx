import { createPortal } from 'react-dom';
import {
  DigiButton,
  DigiFormCheckbox,
  DigiFormValidationMessage,
  DigiLayoutContainer,
  DigiTypography,
} from '@digi/arbetsformedlingen-react';
import {
  ButtonVariation,
  FormCheckboxValidation,
  FormCheckboxVariation,
  FormValidationMessageVariation,
} from '@digi/arbetsformedlingen';
import Error from './Modal';
import useSharing, { SharingChoice } from '@app/hooks/useSharing';
import useErrorState from '@app/hooks/useErrorState';
import useError from '@app/hooks/useError';
import { useIntl } from 'react-intl';

type Props = {
  variant: 'approve' | 'change';
  sharingId?: string;
  clientId: string;
  datasourceId: string;
  foreignUserId: string | undefined;
  redirectUrl: string | undefined;
};

function ApprovalForm({
  variant,
  sharingId,
  clientId,
  datasourceId,
  foreignUserId,
  redirectUrl,
}: Props) {
  const intl = useIntl();
  const { errorState, setErrorState } = useErrorState();
  const { error, setError } = useError();
  const { userSharing, setUserSharing, handleAbort, handleSend } = useSharing(
    { clientId, datasourceId, foreignUserId, redirectUrl },
    setErrorState,
    setError,
    variant === 'approve' ? SharingChoice.Give : SharingChoice.Unknown,
  );

  return (
    <DigiLayoutContainer
      data-testid="approvalForm"
      afNoGutter={true}
      className="mb-6"
    >
      {errorState.show &&
        createPortal(
          <Error
            onClose={() => setErrorState({ show: false })}
            title={errorState.title}
            message={errorState.message}
          />,
          document.body,
        )}
      <DigiTypography>
        <h2 className="mt-6">
          {intl.formatMessage({ id: `sharing-${variant}-action-header` })}
        </h2>
        {variant === 'change' && (
          <DigiFormCheckbox
            data-testid="changeCheckBox"
            afLabel={intl.formatMessage({ id: 'check-reject-digital-sharing' })}
            afVariation={FormCheckboxVariation.PRIMARY}
            afValidation={error ? FormCheckboxValidation.ERROR : undefined}
            onAfOnChange={() => {
              setError(false);
              setUserSharing(
                userSharing === SharingChoice.Unknown
                  ? SharingChoice.Revoke
                  : SharingChoice.Unknown,
              );
            }}
          />
        )}
      </DigiTypography>
      <div className="my-6">
        {error && (
          <DigiFormValidationMessage
            afVariation={FormValidationMessageVariation.ERROR}
            className="flex flex-row self-center"
            data-testid="formValidationError"
          >
            <p>{intl.formatMessage({ id: 'check-to-continue' })}</p>
          </DigiFormValidationMessage>
        )}
      </div>
      <div className="flex flex-col gap-4 sm:flex-row">
        <DigiButton
          onAfOnClick={() => handleAbort(sharingId)}
          afVariation={ButtonVariation.SECONDARY}
          afFullWidth
          data-testid="abortApprovalForm"
        >
          {intl.formatMessage({ id: `dismiss-${variant}` })}
        </DigiButton>
        <DigiButton
          onAfOnClick={() => handleSend()}
          afVariation={ButtonVariation.PRIMARY}
          afFullWidth
          data-testid="sendApprovalForm"
        >
          {intl.formatMessage({ id: `confirm-${variant}` })}
        </DigiButton>
      </div>
    </DigiLayoutContainer>
  );
}

export default ApprovalForm;
