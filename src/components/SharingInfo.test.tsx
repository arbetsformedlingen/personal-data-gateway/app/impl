// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import SharingInfo from './SharingInfo';
import { screen, render } from '@testing-library/react';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

describe('SharingInfo', () => {
  it('passes with default properties to SharingInfo', () => {
    // Act
    render(
      <SharingInfo
        datasourceName="datasourceName"
        consumer="consumer"
        created="created"
        href="href"
      />,
    );

    // Assert
    expect(screen.getByTestId('sharingInfo')).toBeInTheDocument();
  });
});
