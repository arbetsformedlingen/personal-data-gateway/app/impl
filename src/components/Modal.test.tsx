// @vitest-environment jsdom

import { describe, expect, it, vi } from 'vitest';
import { render } from '@testing-library/react';

import Modal from './Modal';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

const { DialogSize, DigiDialog } = vi.hoisted(() => ({
  DialogSize: { SMALL: 'small' },
  DigiDialog: vi.fn(),
}));

vi.mock('@digi/arbetsformedlingen-react', () => ({
  DialogSize,
  DigiDialog,
}));

describe('ErrorModal', () => {
  it('passes with default properties to DigiDialog', () => {
    const onClose = vi.fn();

    render(<Modal onClose={onClose} />);

    expect(DigiDialog).toHaveBeenCalledWith(
      {
        'data-testid': 'modal',
        onAfOnClose: onClose,
        afHeading: 'example-generic-error',
        afShowDialog: true,
        afSize: 'small',
        children: 'example-please-try-again-later',
        afCloseButtonText: undefined,
        afHeadingLevel: 'h2',
      },
      {},
    );
  });

  it('passes optional properties to DigiDialog', () => {
    const onClose = vi.fn();

    render(
      <Modal
        onClose={onClose}
        title="Specific title"
        message="Specific message"
      />,
    );

    expect(DigiDialog).toHaveBeenCalledWith(
      {
        'data-testid': 'modal',
        onAfOnClose: onClose,
        afHeading: 'Specific title',
        afShowDialog: true,
        afSize: 'small',
        children: 'Specific message',
        fCloseButtonText: undefined,
        afHeadingLevel: 'h2',
      },
      {},
    );
  });
});
