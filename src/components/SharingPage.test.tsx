// @vitest-environment jsdom
import { render } from '@testing-library/react';
import { vi, it, describe, expect } from 'vitest';
import SharingPage from '@app/components/SharingPage';
import useSharingPageData from '@app/hooks/useSharingPageData';

const { mockComponent } = vi.hoisted(() => ({
  mockComponent: vi.fn(
    ({
      ['data-testid']: dataTestId,
      children,
    }: {
      'data-testid': string;
      children: React.ReactNode;
    }) => <div data-testid={dataTestId}>{children}</div>,
  ),
}));

const {
  ConsumerInfoMock,
  DataPreviewMock,
  SharingStatusMock,
  ApprovalFormMock,
  FAQMock,
} = vi.hoisted(() => ({
  ConsumerInfoMock: mockComponent,
  DataPreviewMock: mockComponent,
  SharingStatusMock: mockComponent,
  ApprovalFormMock: mockComponent,
  FAQMock: mockComponent,
}));

vi.mock('@app/hooks/useSharingPageData');

vi.mock('@app/components/ConsumerInfo', () => ({
  default: ConsumerInfoMock,
}));

vi.mock('@app/components/DataPreview', () => ({
  default: DataPreviewMock,
}));

vi.mock('./SharingStatus', () => ({
  default: SharingStatusMock,
}));

vi.mock('@app/components/ApprovalForm', () => ({
  default: ApprovalFormMock,
}));

vi.mock('@app/components/FAQ', () => ({
  default: FAQMock,
}));

describe('SharingPage', () => {
  it('passes expected properties to nested components', () => {
    // Arrange
    vi.mocked(useSharingPageData).mockReturnValue({
      header: 'header',
      paragraph: 'paragraph',
      created: 'created',
      revoked: 'revoked',
      expires: 'expires',
      active: true,
      faqItems: [
        {
          key: 'faq-key',
          heading: 'faq-heading',
          itemContent: [],
        },
      ],
      variant: 'approve',
    });

    // Act
    render(
      <SharingPage
        consumerClientId="consumerClientId"
        consumerName="consumerName"
        consumerOrgNr="consumerOrgNr"
        datasourceId="datasourceId"
        foreignUserId="foreignUserId"
        redirectUrl="redirectUrl"
        previewIsLoading={true}
        preview="preview"
        providerName="providerName"
        sharing={{
          _links: {},
          sharingId: 'sharingId',
          datasourceId: 'datasourceId',
          created: 'created',
          updated: 'updated',
          userId: 'userId',
          public: false,
          clientId: 'clientId',
        }}
        dataLastAccess={{
          _links: {},
          accessId: 'accessId',
          sharingId: 'sharingId',
          created: 'created',
          updated: 'updated',
          accessed: 'accessed',
        }}
      />,
    );

    // Assert
    expect(ConsumerInfoMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'sharing-consumerinfo',
        dataConsumerName: 'consumerName',
        dataConsumerOrgNr: 'consumerOrgNr',
      },
      {},
    );

    expect(DataPreviewMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'sharing-datapreview',
        datasourceId: 'datasourceId',
        previewIsLoading: true,
        preview: 'preview',
      },
      {},
    );

    expect(SharingStatusMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'sharing-status',
        created: 'created',
        active: true,
        fetched: 'accessed',
      },
      {},
    );

    expect(ApprovalFormMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'sharing-approvalform',
        variant: 'approve',
        clientId: 'consumerClientId',
        datasourceId: 'datasourceId',
        foreignUserId: 'foreignUserId',
        redirectUrl: 'redirectUrl',
        sharingId: 'sharingId',
      },
      {},
    );

    expect(FAQMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'sharing-faq',
        items: [
          {
            key: 'faq-key',
            heading: 'faq-heading',
            itemContent: [],
          },
        ],
      },
      {},
    );
  });
});
