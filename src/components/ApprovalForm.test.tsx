// @vitest-environment jsdom
import { screen, render } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';
import useErrorState from '@app/hooks/useErrorState';
import useError from '@app/hooks/useError';
import useSharing, { SharingChoice } from '@app/hooks/useSharing';
import ApprovalForm from '@app/components/ApprovalForm';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

vi.mock('@app/hooks/useErrorState');
vi.mock('@app/hooks/useError');
vi.mock('@app/hooks/useSharing');

type DigiButtonProps = {
  'data-testid': string;
  onAfOnClick: () => void;
};

type Props = {
  'data-testid': string;
  onAfOnInput: () => void;
};

type CheckboxProps = {
  'data-testid': string;
  onAfOnChange: () => void;
};

const { DigiButtonMock, DigiFormRadiobuttonMock, DigiFormCheckboxMock } =
  vi.hoisted(() => ({
    DigiButtonMock: vi.fn((props: DigiButtonProps) => {
      onAfOnInputHandles.set(props['data-testid'], props.onAfOnClick);
    }),
    DigiFormRadiobuttonMock: vi.fn((props: Props) => {
      onAfOnInputHandles.set(props['data-testid'], props.onAfOnInput);
    }),
    DigiFormCheckboxMock: vi.fn((props: CheckboxProps) => {
      onAfOnInputHandles.set(props['data-testid'], props.onAfOnChange);
    }),
  }));

const onAfOnInputHandles = new Map<string, () => void>();

vi.mock('@digi/arbetsformedlingen-react', async (original) => {
  return {
    // eslint-disable-next-line @typescript-eslint/consistent-type-imports
    ...(await original<typeof import('@digi/arbetsformedlingen-react')>()),
    DigiButton: DigiButtonMock,
    DigiFormRadiobutton: DigiFormRadiobuttonMock,
    DigiFormCheckbox: DigiFormCheckboxMock,
  };
});

const { createPortal } = vi.hoisted(() => ({
  createPortal: vi.fn(),
}));

vi.mock('react-dom', async (original) => {
  return {
    // eslint-disable-next-line @typescript-eslint/consistent-type-imports
    ...(await original<typeof import('react-dom')>()),
    createPortal,
  };
});

describe('ApprovalForm redirect flow', () => {
  it('should render', () => {
    vi.mocked(useErrorState, { partial: true }).mockReturnValue({
      errorState: { show: false },
    });
    vi.mocked(useError).mockReturnValue({ error: false, setError: vi.fn() });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const component = screen.getByTestId('approvalForm');
    expect(component).toBeDefined();
  });

  it('should render formValidationError when validationError is true', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: true });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const component = screen.getByTestId('formValidationError');
    expect(component).toBeDefined();
  });

  it('should call setError when createPortal Error onClose is called', () => {
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});
    const useErrorStateMock = vi.mocked(useErrorState);
    const setErrorStateMock = vi.fn();
    useErrorStateMock.mockReturnValue({
      errorState: { show: true, title: 'title', message: 'message' },
      setErrorState: setErrorStateMock,
    });

    let onCloseHandle = () => {
      return;
    };
    createPortal.mockImplementation(
      (node: { props: { onClose: () => void } }) => {
        onCloseHandle = node.props.onClose;
      },
    );

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(setErrorStateMock).not.toHaveBeenCalled();
    onCloseHandle();
    expect(setErrorStateMock).toHaveBeenCalledWith({ show: false });
  });

  it('should call handleAbort when abortApprovalForm is checked', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const handleAbortMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleAbort: handleAbortMock,
    });

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(handleAbortMock).not.toHaveBeenCalled();

    const onHandler = onAfOnInputHandles.get('abortApprovalForm');
    onHandler!();

    expect(handleAbortMock).toHaveBeenCalled();
  });

  it('should call handleSend when sendApprovalForm is checked', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const handleSendMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleSend: handleSendMock,
    });

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(handleSendMock).not.toHaveBeenCalled();

    const onHandler = onAfOnInputHandles.get('sendApprovalForm');
    onHandler!();

    expect(handleSendMock).toHaveBeenCalled();
  });
});

describe('ApprovalForm navigation flow', () => {
  it('should render', () => {
    vi.mocked(useErrorState, { partial: true }).mockReturnValue({
      errorState: { show: false },
    });
    vi.mocked(useError).mockReturnValue({ error: false, setError: vi.fn() });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const component = screen.getByTestId('approvalForm');
    expect(component).toBeDefined();
  });

  it('should render formValidationError when validationError is true', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: true });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const component = screen.getByTestId('formValidationError');
    expect(component).toBeDefined();
  });

  it('should call setError when createPortal Error onClose is called', () => {
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    vi.mocked(useSharing, { partial: true }).mockReturnValue({});
    const useErrorStateMock = vi.mocked(useErrorState);
    const setErrorStateMock = vi.fn();
    useErrorStateMock.mockReturnValue({
      errorState: { show: true, title: 'title', message: 'message' },
      setErrorState: setErrorStateMock,
    });

    let onCloseHandle = () => {
      return;
    };
    createPortal.mockImplementation(
      (node: { props: { onClose: () => void } }) => {
        onCloseHandle = node.props.onClose;
      },
    );

    render(
      <ApprovalForm
        variant={'approve'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(setErrorStateMock).not.toHaveBeenCalled();
    onCloseHandle();
    expect(setErrorStateMock).toHaveBeenCalledWith({ show: false });
  });

  it('should call setError(false) and setUserSharing(SharingChoice.Revoke) when DigiFormCheckbox onAfOnChange is called and userSharing is SharingChoice.Unknown', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const setUserSharingMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      userSharing: SharingChoice.Unknown,
      setUserSharing: setUserSharingMock,
    });
    const useErrorMock = vi.mocked(useError);
    const setErrorMock = vi.fn();
    useErrorMock.mockReturnValue({
      error: false,
      setError: setErrorMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const onHandler = onAfOnInputHandles.get('changeCheckBox');
    onHandler!();

    expect(setErrorMock).toHaveBeenCalledWith(false);
    expect(setUserSharingMock).toHaveBeenCalledWith(SharingChoice.Revoke);
  });

  it('should call setError(false) and setUserSharing(SharingChoice.Unknown) when DigiFormCheckbox onAfOnChange is called and userSharing is SharingChoice.Revoke', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const setUserSharingMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      userSharing: SharingChoice.Revoke,
      setUserSharing: setUserSharingMock,
    });
    const useErrorMock = vi.mocked(useError);
    const setErrorMock = vi.fn();
    useErrorMock.mockReturnValue({
      error: false,
      setError: setErrorMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    const onHandler = onAfOnInputHandles.get('changeCheckBox');
    onHandler!();

    expect(setErrorMock).toHaveBeenCalledWith(false);
    expect(setUserSharingMock).toHaveBeenCalledWith(SharingChoice.Unknown);
  });

  it('should call handleAbort when abort button is clicked and sharingId prop is undefined', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const handleAbortMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleAbort: handleAbortMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(handleAbortMock).not.toHaveBeenCalled();

    const onHandler = onAfOnInputHandles.get('abortApprovalForm');
    onHandler!();

    expect(handleAbortMock).toHaveBeenCalled();
  });

  it('should call handleAbort when abort button is clicked and sharingId prop is defined', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const handleAbortMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleAbort: handleAbortMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        sharingId="sharingId"
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(handleAbortMock).not.toHaveBeenCalled();

    const onHandler = onAfOnInputHandles.get('abortApprovalForm');
    onHandler!();

    expect(handleAbortMock).toHaveBeenCalled();
    expect(handleAbortMock).toHaveBeenCalledWith('sharingId');
  });

  it('should call handleSend when sendApprovalForm is checked', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: false });
    const handleSendMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleSend: handleSendMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    expect(handleSendMock).not.toHaveBeenCalled();

    const onHandler = onAfOnInputHandles.get('sendApprovalForm');
    onHandler!();

    expect(handleSendMock).toHaveBeenCalled();
  });

  it('should mark DigiFormCheckbox afValidation when there is an error', () => {
    vi.mocked(useErrorState).mockReturnValue({
      errorState: { show: false },
      setErrorState: vi.fn(),
    });
    vi.mocked(useError, { partial: true }).mockReturnValue({ error: true });
    const handleSendMock = vi.fn();
    vi.mocked(useSharing, { partial: true }).mockReturnValue({
      handleSend: handleSendMock,
    });

    render(
      <ApprovalForm
        variant={'change'}
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );
  });
});
