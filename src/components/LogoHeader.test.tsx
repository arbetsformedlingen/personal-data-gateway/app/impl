// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import { render, screen, within } from '@testing-library/react';

import LogoHeader from '@app/components/LogoHeader';

const { DigiLogo } = vi.hoisted(() => ({
  DigiLogo: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
}));

vi.mock('@digi/arbetsformedlingen-react', () => ({
  DigiLogo,
}));

const { LogoColor, LogoVariation } = vi.hoisted(() => ({
  LogoColor: { PRIMARY: 'primary' },
  LogoVariation: { LARGE: 'large' },
}));

vi.mock('@digi/arbetsformedlingen', () => ({
  LogoColor,
  LogoVariation,
}));

describe('LogoHeader', () => {
  it('renders with partner URL', () => {
    render(<LogoHeader />);

    const logoHeader = screen.queryByTestId('logoHeader');
    expect(logoHeader).toBeInTheDocument();

    const afLogo = within(logoHeader!).queryByTestId('afLogo');
    expect(afLogo).toBeInTheDocument();

    expect(DigiLogo).toHaveBeenCalledWith(
      {
        ['data-testid']: 'afLogo',
        afVariation: 'large',
        afColor: 'primary',
      },
      {},
    );
  });
});
