import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import RedirectFlow from './RedirectFlow';

import { getClient } from 'tests/msw/handlers/client';

export default {
  title: 'Components/ServiceDetails',
  component: RedirectFlow,
  args: {
    clientId: 'abcd',
    scope: 'query-unemployment',
    foreignUserId: 'efgh',
  },
  parameters: {
    msw: {
      handlers: [getClient()],
    },
    layout: 'fullscreen',
  },
} as Meta<typeof RedirectFlow>;

const Template: StoryFn<typeof RedirectFlow> = (args) => (
  <RedirectFlow {...args} />
);

export const Default = Template.bind({});

export const Error = Template.bind({});
Error.parameters = {
  msw: {
    handlers: [getClient()],
  },
};
