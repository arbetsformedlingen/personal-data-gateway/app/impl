// @vitest-environment jsdom
import React from 'react';
import { render } from '@testing-library/react';
import Index from '@app/components/Index';
import { vi, describe, expect, it, test } from 'vitest';

const { env } = vi.hoisted(() => ({
  env: {
    getEnv: vi.fn().mockReturnValue({
      MAINTENANCE_MODE: undefined,
    }),
  },
}));

vi.mock('@app/env.mjs', () => env);

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

const { mockUseSearchParams, mockApplication, mockHome } = vi.hoisted(() => ({
  mockUseSearchParams: vi.fn(),
  mockApplication: vi.fn(),
  mockHome: vi.fn(),
}));

vi.mock('next/navigation', () => ({
  useSearchParams: mockUseSearchParams,
}));

vi.mock('@app/components/Application', () => ({
  default: mockApplication,
}));

vi.mock('@app/components/Home', () => ({
  default: mockHome,
}));

describe('Index', () => {
  it('renders Application navigation flow if sharingId present as single query parameter', () => {
    mockUseSearchParams.mockImplementationOnce(() => ({
      size: 1,
      get: (key: string) => new Map([['sharingId', 'sharingId']]).get(key),
    }));
    vi.spyOn(URLSearchParams.prototype, 'get').mockImplementation((key) => key);

    const { queryByTestId } = render(<Index />);

    expect(queryByTestId('application-home-navigation-flow'));
    expect(queryByTestId('maintenance-page')).not.toBeInTheDocument();
  });

  it('renders Application redirect flow if the four expected query parameters are present', () => {
    mockUseSearchParams.mockImplementationOnce(() => ({
      size: 4,
      get: (key: string) =>
        new Map([
          ['clientId', 'clientId'],
          ['datasourceId', 'datasourceId'],
          ['state', 'state'],
          ['redirectUrl', 'redirectUrl'],
        ]).get(key),
    }));
    vi.spyOn(URLSearchParams.prototype, 'get').mockImplementation((key) => {
      switch (key) {
        case 'clientId':
          return 'clientId';
        case 'datasourceId':
          return 'datasourceId';
        case 'state':
          return 'state';
        case 'redirectUrl':
          return 'redirectUrl';
        default:
          return null;
      }
    });

    const { queryByTestId } = render(<Index />);

    expect(queryByTestId('application-home-redirect-flow'));
    expect(queryByTestId('maintenance-page')).not.toBeInTheDocument();
  });

  it('renders home if no parameters in query string', () => {
    const params = new Map([]);
    mockUseSearchParams.mockImplementationOnce(() => ({
      size: 0,
      get: (key: string) => params.get(key),
      params,
    }));

    const { queryByTestId } = render(<Index />);

    expect(queryByTestId('index-home'));
    expect(queryByTestId('maintenance-page')).not.toBeInTheDocument();
  });

  test.each([
    ['clientId', 'datasourceId', 'state', undefined],
    ['clientId', 'datasourceId', undefined, 'redirectUrl'],
    ['clientId', 'datasourceId', undefined, undefined],
    ['clientId', undefined, 'state', 'redirectUrl'],
    ['clientId', undefined, 'state', undefined],
    ['clientId', undefined, undefined, 'redirectUrl'],
    ['clientId', undefined, undefined, undefined],
    [undefined, 'datasourceId', 'state', 'redirectUrl'],
    [undefined, 'datasourceId', 'state', undefined],
    [undefined, 'datasourceId', undefined, 'redirectUrl'],
    [undefined, 'datasourceId', undefined, undefined],
    [undefined, undefined, 'state', 'redirectUrl'],
    [undefined, undefined, 'state', undefined],
    [undefined, undefined, undefined, 'redirectUrl'],
  ])(
    'renders home if expected parameters in query string are missing',
    (clientId, datasourceId, state, redirectUrl) => {
      mockUseSearchParams.mockImplementation(() => ({
        size: 4,
        get: (key: string) =>
          new Map([
            ['clientId', clientId],
            ['datasourceId', datasourceId],
            ['state', state],
            ['redirectUrl', redirectUrl],
          ]).get(key),
      }));
      vi.spyOn(URLSearchParams.prototype, 'get').mockImplementation((key) => {
        switch (key) {
          case 'clientId':
            return clientId ? clientId : null;
          case 'datasourceId':
            return datasourceId ? datasourceId : null;
          case 'state':
            return state ? state : null;
          case 'redirectUrl':
            return redirectUrl ? redirectUrl : null;
          default:
            return null;
        }
      });

      const { queryByTestId } = render(<Index />);

      expect(queryByTestId('index-home'));
      expect(queryByTestId('maintenance-page')).not.toBeInTheDocument();
    },
  );
});
