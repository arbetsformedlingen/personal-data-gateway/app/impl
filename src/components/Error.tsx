import { useIntl } from 'react-intl';

export type ErrorProps = {
  headerId: string;
  bodyIds: string[];
};

function Error({ headerId, bodyIds }: ErrorProps) {
  const intl = useIntl();

  return (
    <div>
      <h2>{intl.formatMessage({ id: headerId })}</h2>
      {bodyIds.map((id) => {
        return <p key={id}>{intl.formatMessage({ id })}</p>;
      })}
    </div>
  );
}

export default Error;
