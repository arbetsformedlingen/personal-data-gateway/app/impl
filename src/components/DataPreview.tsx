import useDatasource from '@app/hooks/useDatasource';
import { LayoutBlockVariation } from '@digi/arbetsformedlingen';
import {
  DigiLayoutBlock,
  DigiLayoutContainer,
  DigiTypography,
} from '@digi/arbetsformedlingen-react';
import { useIntl } from 'react-intl';

export type Props = {
  datasourceId?: string;
  previewIsLoading: boolean;
  preview:
    | string
    | {
        status: number;
        error: string;
        message?: string | undefined;
      }
    | Blob
    | ArrayBuffer
    | ReadableStream<Uint8Array>
    | null
    | undefined;
};

const narrow = (previewData: unknown, fallback: string) =>
  typeof previewData === 'string' ? previewData : fallback;

function DataPreview({ datasourceId, previewIsLoading, preview }: Props) {
  const intl = useIntl();
  const { datasource } = useDatasource(datasourceId);

  const data =
    !previewIsLoading && preview !== undefined
      ? narrow(preview, intl.formatMessage({ id: 'data-render-error' }))
      : undefined;
  const error =
    !previewIsLoading && preview === undefined
      ? intl.formatMessage({ id: 'load-data-preview-error' })
      : undefined;

  let text = 'Laddar...';
  if (error) {
    text = error;
  } else if (data) {
    text = data;
  }

  const datasourceName = datasource?.name;

  return (
    <DigiLayoutContainer afNoGutter={true} className="mb-6">
      <div>
        <DigiTypography>
          <h2>{intl.formatMessage({ id: 'information-that-is-fetched' })}</h2>
        </DigiTypography>
        <DigiLayoutBlock
          afVariation={LayoutBlockVariation.SECONDARY}
          className="mb-6"
          afVerticalPadding={true}
        >
          <DigiTypography>
            {datasourceName && (
              <div data-testid="datasourceName">{datasourceName}</div>
            )}
            <span data-testid="previewData" className="font-bold">
              {text}
            </span>
          </DigiTypography>
        </DigiLayoutBlock>
      </div>
    </DigiLayoutContainer>
  );
}

export default DataPreview;
