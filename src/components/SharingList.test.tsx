// @vitest-environment jsdom
import { describe, it, vi } from 'vitest';
import SharingList from './SharingList';
import { render } from '@testing-library/react';
import { type UserSharing } from '@app/hooks/useUserSharings';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

const { mockSharingInfo } = vi.hoisted(() => ({
  mockSharingInfo: vi.fn(),
}));

vi.mock('@app/components/SharingInfo', () => ({
  default: mockSharingInfo,
}));

describe('SharingList', () => {
  it('passes with empty array of user sharings to SharingList', () => {
    // Arrange
    const userSharings: UserSharing[] = [];

    // Act
    render(<SharingList userSharings={userSharings} />);

    // Assert
    //expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
  });

  it('passes with array of user sharings to SharingList', () => {
    // Arrange
    const userSharings = [
      {
        sharingId: 'sharingId',
        clientName: 'clientName',
        datasourceName: 'datasourceName',
        created: '',
      },
    ];

    // Act
    render(<SharingList userSharings={userSharings} />);

    // Assert
    //expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
  });
});
