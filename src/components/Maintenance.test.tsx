// @vitest-environment jsdom

import { render, screen, within } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';
import Maintenance from './Maintenance';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

describe('Maintenance', () => {
  it('renders component', () => {
    // Act
    render(<Maintenance />);

    // Assert
    const maintenancePage = screen.queryByTestId('maintenance-page');
    expect(maintenancePage).toBeInTheDocument();
    expect(
      within(maintenancePage!).queryByTestId('maintenance-page-title'),
    ).toBeInTheDocument();
    expect(
      within(maintenancePage!).queryByTestId('maintenance-page-description'),
    ).toBeInTheDocument();
  });
});
