// @vitest-environment jsdom
import { screen, render } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import FAQ from './FAQ';

describe('FAQ', () => {
  it('should render', () => {
    render(
      <FAQ
        items={[{ heading: 'heading', key: 'key', itemContent: ['content'] }]}
      />,
    );

    const component = screen.getByTestId('faq');

    expect(component).toBeDefined();
  });
});
