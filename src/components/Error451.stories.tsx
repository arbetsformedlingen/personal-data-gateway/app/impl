import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import Error451 from './Error451';

import { getClient } from 'tests/msw/handlers/client';

export default {
  title: 'Components/Error451',
  component: Error451,
  args: {
    message: 'message',
    redirectUrl: 'redirectUrl',
    orgNr: 'orgnr',
    orgName: 'orgName',
  },
  parameters: {
    msw: {
      handlers: [getClient()],
    },
    layout: 'fullscreen',
  },
} as Meta<typeof Error451>;

const Template: StoryFn<typeof Error451> = (args) => <Error451 {...args} />;

export const Default = Template.bind({});
