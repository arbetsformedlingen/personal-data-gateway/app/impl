// @vitest-environment jsdom
import { screen, render } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';
import Application from './Application';

const { RedirectFlowMock, NavigationFlowMock, HomeMock } = vi.hoisted(() => ({
  RedirectFlowMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  NavigationFlowMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  HomeMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
}));

vi.mock('@app/components/RedirectFlow', () => ({ default: RedirectFlowMock }));
vi.mock('@app/components/NavigationFlow', () => ({
  default: NavigationFlowMock,
}));
vi.mock('@app/components/Home', () => ({ default: HomeMock }));

describe('Application', () => {
  it('should render Home if neither redirectFlow or navigationFlow is defined', () => {
    render(<Application redirectFlow={undefined} navigationFlow={undefined} />);

    const component = screen.getByTestId('application-home');
    expect(component).toBeDefined();
  });

  it('should render RedirectFlow if redirectFlow is defined', () => {
    const redirectFlow = {
      clientId: 'clientId',
      datasourceId: 'datasourceId',
      foreignUserId: 'foreignUserId',
      redirectUrl: 'redirectUrl',
    };
    render(
      <Application redirectFlow={redirectFlow} navigationFlow={undefined} />,
    );

    const component = screen.getByTestId('application-redirectFlow');
    expect(component).toBeDefined();
  });

  it('should render NavigationFlow if redirectFlow is undefined and navigationFlow is defined', () => {
    const navigationFlow = {
      sharingId: 'sharingId',
    };
    render(
      <Application redirectFlow={undefined} navigationFlow={navigationFlow} />,
    );

    const component = screen.getByTestId('application-navigationFlow');
    expect(component).toBeDefined();
  });
});
