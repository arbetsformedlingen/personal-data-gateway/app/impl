import type { Meta, StoryObj } from '@storybook/react';

import Sharings from './Sharings';
import { type FAQItem } from './FAQ';
import { type UserSharing } from '@app/hooks/useUserSharings';

const meta: Meta<typeof Sharings> = {
  component: Sharings,
  title: 'Components/Sharings',
};

export default meta;

type Story = StoryObj<typeof Sharings>;

export const Default: Story = {
  args: {
    userSharings: [],
    faqItems: [],
  },
};

const faqItems: FAQItem[] = [
  {
    key: 'key1',
    heading: 'FAQ 1',
    itemContent: ['Paragraph 1', 'Paragraph 2'],
  },
  {
    key: 'key2',
    heading: 'FAQ 2',
    itemContent: ['Paragraph 1', 'Paragraph 2'],
  },
];

const userSharings1: UserSharing[] = [
  {
    sharingId: '1',
    clientName: 'CLient 1',
    datasourceName: 'Datasource 1',
    created: '2024-10-12',
  },
];

export const Example: Story = {
  args: {
    userSharings: userSharings1,
    faqItems: faqItems,
  },
};

const userSharings2: UserSharing[] = [
  {
    sharingId: '1',
    clientName: 'CLient 1',
    datasourceName: 'Datasource 1',
    created: '2024-10-12',
  },
  {
    sharingId: '2',
    clientName: 'CLient 2',
    datasourceName: 'Datasource 2',
    created: '2024-09-22',
  },
];

export const Publik: Story = {
  args: {
    userSharings: userSharings2,
    faqItems: faqItems,
  },
};
