// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import Sharings from './Sharings';
import { render, screen } from '@testing-library/react';
import { type UserSharing } from '@app/hooks/useUserSharings';
import { type FAQItem } from './FAQ';

const { mockSharingList, mockFAQ } = vi.hoisted(() => ({
  mockSharingList: vi.fn(),
  mockFAQ: vi.fn(),
}));

vi.mock('@app/components/SharingList', () => ({
  default: mockSharingList,
}));

vi.mock('@app/components/FAQ', () => ({
  default: mockFAQ,
}));

describe('Sharings', () => {
  it('passes with default properties to Sharings', () => {
    // Arrange
    const userSharings = [] as UserSharing[];
    const faqItems = [] as FAQItem[];

    // Act
    render(<Sharings userSharings={userSharings} faqItems={faqItems} />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
  });
});
