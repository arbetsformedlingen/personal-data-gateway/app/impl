// @vitest-environment jsdom
import React from 'react';
import { render, screen, within } from '@testing-library/react';
import ConsumerInfo from './ConsumerInfo';
import { describe, expect, it, vi } from 'vitest';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

vi.mock('@digi/arbetsformedlingen-react', () => ({
  DigiLayoutBlock: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
  DigiLayoutContainer: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
  DigiLoaderSpinner: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
  DigiTypography: vi.fn(({ children, ['data-testid']: dataTestId }) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    <div data-testid={dataTestId}>{children}</div>
  )),
}));

describe('ConsumerInfo component', () => {
  it('renders data information correctly when provided with data', () => {
    render(
      <ConsumerInfo
        dataConsumerName="Test Consumer"
        dataConsumerOrgNr="123456-1234"
      />,
    );

    const contentContainer = screen.getByTestId('ConsumerInfo-content');
    const consumerInfo = within(contentContainer).getByTestId(
      'ConsumerInfo-consumerInfo',
    );
    const consumerName = within(consumerInfo).getByTestId(
      'ConsumerInfo-consumerInfo-consumerName',
    );
    const consumerOrgNr = within(consumerInfo).getByTestId(
      'ConsumerInfo-consumerInfo-orgNr',
    );

    expect(
      within(contentContainer).queryByTestId('ConsumerInfo-loader'),
    ).toBeNull();
    expect(consumerName).toContainHTML('Test Consumer');
    expect(consumerOrgNr).toContainHTML(
      'example-organization-number: 123456-1234',
    );
  });

  it('renders loader when "dataConsumerName" is undefined', () => {
    render(
      <ConsumerInfo
        dataConsumerName={undefined}
        dataConsumerOrgNr="123456-1234"
      />,
    );

    const contentContainer = screen.getByTestId('ConsumerInfo-content');
    const loader = within(contentContainer).getByTestId('ConsumerInfo-loader');
    expect(
      within(contentContainer).queryByTestId('ConsumerInfo-consumerInfo'),
    ).toBeNull();
    expect(loader).toBeInTheDocument();
  });

  it('renders loader when "dataConsumerOrgNr" is undefined', () => {
    render(
      <ConsumerInfo
        dataConsumerName="Test Consumer"
        dataConsumerOrgNr={undefined}
      />,
    );

    const contentContainer = screen.getByTestId('ConsumerInfo-content');
    const loader = within(contentContainer).getByTestId('ConsumerInfo-loader');
    expect(
      within(contentContainer).queryByTestId('ConsumerInfo-consumerInfo'),
    ).toBeNull();
    expect(loader).toBeInTheDocument();
  });

  it('renders loader when both "dataConsumerName" and "dataConsumerOrgNr" is undefined', () => {
    render(
      <ConsumerInfo
        dataConsumerName={undefined}
        dataConsumerOrgNr={undefined}
      />,
    );

    const contentContainer = screen.getByTestId('ConsumerInfo-content');
    const loader = within(contentContainer).getByTestId('ConsumerInfo-loader');
    expect(
      within(contentContainer).queryByTestId('ConsumerInfo-consumerInfo'),
    ).toBeNull();
    expect(loader).toBeInTheDocument();
  });
});
