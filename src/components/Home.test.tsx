// @vitest-environment jsdom
import { screen, render, within } from '@testing-library/react';
import { vi, describe, it, expect } from 'vitest';
import Home from './Home';
import useHomeData, { type FaqItem } from '@app/hooks/useHomeData';
import { type UserSharing } from '@app/hooks/useUserSharings';

const { mockComponent } = vi.hoisted(() => ({
  mockComponent: vi.fn(
    ({
      ['data-testid']: dataTestId,
      children,
    }: {
      'data-testid': string;
      children: React.ReactNode;
    }) => <div data-testid={dataTestId}>{children}</div>,
  ),
}));

const {
  DigiLayoutContainerMock,
  DigiTypographyMock,
  SharingListMock,
  FAQMock,
} = vi.hoisted(() => ({
  DigiLayoutContainerMock: mockComponent,
  DigiTypographyMock: mockComponent,
  SharingListMock: mockComponent,
  FAQMock: mockComponent,
}));

vi.mock('@digi/arbetsformedlingen-react', () => ({
  DigiLayoutContainer: DigiLayoutContainerMock,
  DigiTypography: DigiTypographyMock,
}));

vi.mock('@app/components/SharingList', () => ({
  default: SharingListMock,
}));

vi.mock('@app/components/FAQ', () => ({
  default: FAQMock,
}));

vi.mock('@app/hooks/useHomeData');

describe('Home', () => {
  it('renders', () => {
    // Arrange
    const userSharingMock = { label: 'sharing' } as unknown as UserSharing;
    const faqItemMock = { label: 'faqItem' } as unknown as FaqItem;

    vi.mocked(useHomeData).mockReturnValue({
      header: 'header',
      paragraph: 'paragraph',
      faqItems: [faqItemMock],
      userSharings: [userSharingMock],
    });

    // Act
    render(<Home />);

    // Assert
    const container = screen.queryByTestId('Home-container');
    expect(container).toBeInTheDocument();

    const innerContainer = within(container!).queryByTestId(
      'Home-innerContainer',
    );
    expect(innerContainer).toBeInTheDocument();

    const typography = within(innerContainer!).queryByTestId('Home-typography');
    expect(typography).toBeInTheDocument();

    const header = within(typography!).queryByTestId('Home-header');
    expect(header).toBeInTheDocument();
    expect(header).toHaveTextContent('header');

    const paragraph = within(typography!).queryByTestId('Home-paragraph');
    expect(paragraph).toBeInTheDocument();
    expect(paragraph).toHaveTextContent('paragraph');

    const sharings = within(innerContainer!).queryByTestId('Home-sharings');
    expect(sharings).toBeInTheDocument();

    const faq = within(innerContainer!).queryByTestId('Home-faq');
    expect(faq).toBeInTheDocument();

    expect(SharingListMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'Home-sharings',
        userSharings: [userSharingMock],
      },
      {},
    );

    expect(FAQMock).toHaveBeenCalledWith(
      {
        ['data-testid']: 'Home-faq',
        items: [faqItemMock],
      },
      {},
    );
  });
});
