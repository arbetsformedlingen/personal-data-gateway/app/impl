import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import ApprovalForm from './ApprovalForm';

import { getClient } from 'tests/msw/handlers/client';

export default {
  title: 'Components/ApprovalForm',
  component: ApprovalForm,
  args: {
    clientId: 'abcd',
    datasourceId: 'datasourceId',
    foreignUserId: 'foreignUserId',
    redirectUrl: 'redirectUrl',
  },
  parameters: {
    msw: {
      handlers: [getClient()],
    },
    layout: 'fullscreen',
  },
} as Meta<typeof ApprovalForm>;

const Template: StoryFn<typeof ApprovalForm> = (args) => (
  <ApprovalForm {...args} />
);

export const Default = Template.bind({});
