// @vitest-environment jsdom
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { render, fireEvent, waitFor } from '@testing-library/react';
import { expect, describe, it, vi } from 'vitest';
import Error451 from '@app/components/Error451';
import type { Dispatch, ReactNode, SetStateAction } from 'react';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

vi.mock('next/router', () => ({ useRouter: vi.fn() }));

vi.mock('@app/utils/api', () => ({
  api: {
    service: {
      getInfo: {
        useQuery: vi.fn(() => ({
          data: {
            humanFriendlyName: 'Test Service',
            orgNr: '1234567890',
          },
        })),
      },
    },
  },
}));

type DigiButtonProps = {
  'data-testid': string;
  children: ReactNode | ReactNode[];
  onAfOnClick: () => void;
};

const setTargetUrlMock: Dispatch<SetStateAction<boolean>> = vi.fn();
const { useTargetUrl } = vi.hoisted(() => ({
  useTargetUrl: vi.fn((_state) => ({
    setTargetUrl: setTargetUrlMock,
  })),
}));

vi.mock('../hooks/useTargetUrl', () => ({
  default: useTargetUrl,
}));

const { DigiButton } = vi.hoisted(() => ({
  DigiButton: vi.fn((props: DigiButtonProps) => {
    return (
      <div
        data-testid={props['data-testid']}
        onClick={() => props.onAfOnClick()}
      >
        {props.children}
      </div>
    );
  }),
}));

vi.mock('@digi/arbetsformedlingen-react', async (original) => {
  return {
    // eslint-disable-next-line @typescript-eslint/consistent-type-imports
    ...(await original<typeof import('@digi/arbetsformedlingen-react')>()),
    DigiButton,
  };
});

// TODO: Mock usePreventLeaving

vi.mock('../hooks/usePreventLeaving', () => ({
  default: vi.fn(() => ({
    preventLeaving: vi.fn(),
  })),
}));

const { DataInfo } = vi.hoisted(() => ({
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  DataInfo: (props: any) => <div {...props} />,
}));

vi.mock('@app/components/DataInfo', () => ({ default: DataInfo }));

describe('Error451', () => {
  it('should render with provided props', () => {
    const message = 'Test error message';
    const redirectUrl = '/redirect';

    const { getByText, getByTestId } = render(
      <Error451
        message={message}
        redirectUrl={redirectUrl}
        foreignUserId="abc123"
      />,
    );

    expect(getByText(message)).toBeDefined();
    expect(getByText(/example-action-close-and-return/i)).toBeDefined();

    expect(getByTestId('abortError451')).toBeDefined();
  });

  it('should call handleAbort when button is clicked', async () => {
    const message = 'Test error message';
    const redirectUrl = '/redirect';

    const { getByTestId } = render(
      <Error451
        message={message}
        redirectUrl={redirectUrl}
        foreignUserId="abc123"
      />,
    );

    fireEvent.click(getByTestId('abortError451'));

    await waitFor(() => {
      expect(setTargetUrlMock).toHaveBeenCalledWith(
        `${redirectUrl}?state=abc123&message=Denied`,
      );
    });
  });
});
