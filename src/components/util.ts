export const isActive = (
  created?: string,
  revoked?: string,
  expires?: string,
) => {
  const now = new Date().getTime();
  return (
    created != undefined &&
    (revoked === undefined || Date.parse(revoked) > now) && // in practice revocations timestamps will always be in the past
    (expires === undefined || Date.parse(expires) > now)
  ); // only sharings that has not yet expired are considered active
};
