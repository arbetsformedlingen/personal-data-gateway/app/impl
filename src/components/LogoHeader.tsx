import React from 'react';

import { DigiLogo } from '@digi/arbetsformedlingen-react';
import { LogoColor, LogoVariation } from '@digi/arbetsformedlingen';

const LogoHeader = () => {
  return (
    <div
      className="flex flex-col items-center md:flex-row md:justify-center"
      data-testid="logoHeader"
    >
      <div className="my-8 mr-2">
        <DigiLogo
          afVariation={LogoVariation.LARGE}
          afColor={LogoColor.PRIMARY}
          data-testid="afLogo"
        />
      </div>
    </div>
  );
};

export default LogoHeader;
