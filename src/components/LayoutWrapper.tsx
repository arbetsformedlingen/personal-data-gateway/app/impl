import {
  DigiLayoutContainer,
  DigiTypography,
} from '@digi/arbetsformedlingen-react';
import type { ReactNode } from 'react';

const LayoutWrapper = (children: ReactNode | ReactNode[]) => (
  <DigiLayoutContainer afVerticalPadding afNoGutter data-testid="LayoutWrapper">
    <DigiTypography>
      <div>{children}</div>
    </DigiTypography>
  </DigiLayoutContainer>
);

export default LayoutWrapper;
