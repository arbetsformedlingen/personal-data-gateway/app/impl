import {
  LayoutBlockVariation,
  LoaderSpinnerSize,
} from '@digi/arbetsformedlingen';
import {
  DigiLayoutBlock,
  DigiLayoutContainer,
  DigiLoaderSpinner,
  DigiTypography,
} from '@digi/arbetsformedlingen-react';
import { useIntl } from 'react-intl';

export type Props = {
  dataConsumerName: string | undefined;
  dataConsumerOrgNr: string | undefined;
};

function ConsumerInfo({ dataConsumerName, dataConsumerOrgNr }: Props) {
  const intl = useIntl();
  const isLoading = !dataConsumerName || !dataConsumerOrgNr;

  const loader = (
    <DigiLoaderSpinner
      data-testid="ConsumerInfo-loader"
      afSize={LoaderSpinnerSize.MEDIUM}
      afText={intl.formatMessage({ id: 'loading' })}
    />
  );
  const consumerInfo = (
    <DigiTypography data-testid="ConsumerInfo-consumerInfo">
      <div data-testid="ConsumerInfo-consumerInfo-consumerName">
        {dataConsumerName}
      </div>
      <div data-testid="ConsumerInfo-consumerInfo-orgNr">
        {intl.formatMessage({ id: 'organization-number' })}: {dataConsumerOrgNr}
      </div>
    </DigiTypography>
  );

  const content = isLoading ? loader : consumerInfo;

  return (
    <DigiLayoutContainer afNoGutter={true} className="mb-6">
      <div>
        <DigiTypography>
          <h2>{intl.formatMessage({ id: 'receiver' })}</h2>
        </DigiTypography>
        <DigiLayoutBlock
          afVariation={LayoutBlockVariation.SECONDARY}
          className="mb-6"
          afVerticalPadding={true}
        >
          <div data-testid="ConsumerInfo-content">{content}</div>
        </DigiLayoutBlock>
      </div>
    </DigiLayoutContainer>
  );
}

export default ConsumerInfo;
