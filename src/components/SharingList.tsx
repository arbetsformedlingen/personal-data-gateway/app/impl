import { type UserSharing } from '@app/hooks/useUserSharings';
import SharingInfo from './SharingInfo';
import { usePathname } from 'next/navigation';
import { useIntl } from 'react-intl';

type Props = {
  userSharings: UserSharing[];
};

const SharingList = ({ userSharings }: Props) => {
  const intl = useIntl();
  const pathname = usePathname();

  return userSharings.map((sharing) => {
    const params = new URLSearchParams([['sharingId', sharing.sharingId]]);
    const href = `${pathname}${intl.locale}?${params.toString()}`;

    return (
      <SharingInfo
        key={sharing.sharingId}
        consumer={sharing.clientName}
        datasourceName={sharing.datasourceName}
        created={sharing.created}
        href={href}
      />
    );
  });
};

export default SharingList;
