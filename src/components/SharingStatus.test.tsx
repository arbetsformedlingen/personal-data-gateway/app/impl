// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import SharingStatus from './SharingStatus';
import { screen, render } from '@testing-library/react';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

describe('SharingStatus', () => {
  it('does not render when "created"-property is undefined', () => {
    // Act
    render(<SharingStatus created={undefined} active={true} />);

    // Assert
    expect(screen.queryByTestId('sharing-status')).not.toBeInTheDocument();
  });

  it('does not render when "created"-property is defined but active is false', () => {
    // Act
    render(<SharingStatus created="2024-08-11" active={false} />);

    // Assert
    expect(screen.queryByTestId('sharing-status')).not.toBeInTheDocument();
  });

  it('passes without fetched information', () => {
    // Act
    render(<SharingStatus created="2024-08-11" active={true} />);

    // Assert
    expect(screen.queryByTestId('sharing-status')).toBeInTheDocument();
  });

  it('passes with fetched informations', () => {
    // Act
    render(
      <SharingStatus created="2024-08-11" active={true} fetched="2024-09-02" />,
    );

    // Assert
    expect(screen.queryByTestId('sharing-status')).toBeInTheDocument();
    expect(screen.queryByTestId('sharing-fetched')).toBeInTheDocument();
  });
});
