import SharingPage from './SharingPage';
import { useIntl } from 'react-intl';
import LayoutWrapper from './LayoutWrapper';
import Error from './Error';
import useNavigationFlowData from '@app/hooks/useNavigationFlowData';

export type Props = {
  sharingId: string;
};

const getErrorId = (error: {
  clientError: boolean;
  datasourceError: boolean;
  clientOrganizationError: boolean;
  datasourceOrganizationError: boolean;
}) => {
  if (error.clientError) return 'error-invalid-client-id';
  else if (error.datasourceError) return 'error-invalid-datasource-id';
  else if (error.clientOrganizationError)
    return 'error-invalid-client-organization';
  else return 'error-invalid-datasource-organization';
};

const showError = (errorId: string) =>
  LayoutWrapper(
    <Error
      key={errorId}
      data-testid={errorId}
      headerId={errorId}
      bodyIds={[
        'contact-dataconsumer-for-more-info',
        'you-can-now-close-window',
      ]}
    />,
  );

function NavigationFlow({ sharingId }: Props) {
  const intl = useIntl();

  const {
    isLoading,
    isError,
    previewIsLoading,
    error,
    data,
    userSharing,
    dataLastAccess,
  } = useNavigationFlowData(sharingId);

  if (isLoading) {
    return LayoutWrapper(
      <div key="loadingComponent" data-testid="loading-component">
        {intl.formatMessage({ id: 'loading' })}...
      </div>,
    );
  }

  if (isError) {
    return showError(getErrorId(error));
  }

  const {
    consumerName,
    consumerClientId,
    consumerOrgNr,
    providerName,
    datasourceId,
  } = data!;

  if (providerName === undefined)
    return (
      <div data-testid="providerName-error">
        Provider name must not be undefined
      </div>
    );

  return LayoutWrapper(
    <SharingPage
      data-testid="NavigationFlow-SharingPage"
      consumerName={consumerName}
      consumerClientId={consumerClientId}
      consumerOrgNr={consumerOrgNr}
      providerName={providerName}
      datasourceId={datasourceId!}
      foreignUserId={undefined}
      redirectUrl={undefined}
      previewIsLoading={previewIsLoading}
      preview={data?.preview}
      sharing={userSharing?.unmapped}
      dataLastAccess={dataLastAccess}
    />,
  );
}

export default NavigationFlow;
