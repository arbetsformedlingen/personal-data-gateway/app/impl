// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import { screen, render } from '@testing-library/react';
import NavigationFlow from './NavigationFlow';
import useNavigationFlowData from '@app/hooks/useNavigationFlowData';
import { type components } from '@dist/spec';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

const {
  ErrorMock,
  Error451Mock,
  ErrorInvalidClientMock,
  ErrorTechnicalProblemMock,
  DataInfoMock,
  DataPreviewMock,
  ApprovalFormMock,
  FAQMock,
  SharingPageMock,
  Error451ComponentMock,
} = vi.hoisted(() => ({
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  ErrorMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  Error451Mock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  ErrorInvalidClientMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  ErrorTechnicalProblemMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  DataInfoMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  DataPreviewMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  ApprovalFormMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  FAQMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  SharingPageMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  Error451ComponentMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
}));

vi.mock('@app/hooks/useNavigationFlowData');
vi.mock('@app/components/Error', () => ({ default: ErrorMock }));
vi.mock('@app/components/Error451', () => ({ default: Error451Mock }));
vi.mock('@app/components/ErrorInvalidClient', () => ({
  default: ErrorInvalidClientMock,
}));
vi.mock('@app/components/ErrorTechnicalProblem', () => ({
  default: ErrorTechnicalProblemMock,
}));
vi.mock('@app/components/DataInfo', () => ({ default: DataInfoMock }));
vi.mock('@app/components/DataPreview', () => ({ default: DataPreviewMock }));
vi.mock('@app/components/ApprovalForm', () => ({ default: ApprovalFormMock }));
vi.mock('@app/components/FAQ', () => ({ default: FAQMock }));
vi.mock('@app/components/SharingPage', () => ({ default: SharingPageMock }));
vi.mock('@app/components/Error451Component', () => ({
  default: Error451ComponentMock,
}));

describe('NavigationFlow', () => {
  it('displays loading component while consumer client is loading', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: true,
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('loading-component')).toBeInTheDocument();
  });

  it('displays Error when there is a clientError', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: true,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('error-invalid-client-id')).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-client-id',
        headerId: 'error-invalid-client-id',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a datasourceError', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        datasourceError: true,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-datasource-id'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-datasource-id',
        headerId: 'error-invalid-datasource-id',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a clientOrganizationError', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: true,
        userSharingError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-client-organization'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-client-organization',
        headerId: 'error-invalid-client-organization',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a datasourceOrganizationError', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        datasourceError: false,
        datasourceOrganizationError: true,
        clientOrganizationError: false,
        userSharingError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-datasource-organization'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-datasource-organization',
        headerId: 'error-invalid-datasource-organization',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays "Provider name must not be undefined" when "providerName"-property is undefined', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      previewIsLoading: false,
      isError: false,
      error: {
        clientError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: undefined,
        datasourceId: 'datasourceId',
        preview: 'preview',
        sharingId: 'sharingId',
        sharingCreated: 'sharingCreated',
        sharingRevoked: 'sharingRevoked',
        sharingExpires: 'sharingExpires',
        sharingLastAccessed: 'sharingLastAccessed',
      },
      userSharing: {
        unmapped: {} as components['schemas']['GetUserSharingResponse'],
        sharingId: 'sharingId',
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        created: 'created',
        revoked: 'revoked',
        expires: 'expires',
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    const element = screen.queryByTestId('providerName-error');
    expect(element).toBeInTheDocument();

    expect(element).toHaveTextContent('Provider name must not be undefined');
  });

  it('renders component', () => {
    // Arrange
    vi.mocked(useNavigationFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      previewIsLoading: false,
      isError: false,
      error: {
        clientError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        datasourceId: 'datasourceId',
        preview: 'preview',
        sharingId: 'sharingId',
        sharingCreated: 'sharingCreated',
        sharingRevoked: 'sharingRevoked',
        sharingExpires: 'sharingExpires',
        sharingLastAccessed: 'sharingLastAccessed',
      },
      userSharing: {
        unmapped: {} as components['schemas']['GetUserSharingResponse'],
        sharingId: 'sharingId',
        clientId: 'clientId',
        datasourceId: 'datasourceId',
        created: 'created',
        revoked: 'revoked',
        expires: 'expires',
      },
    });

    // Act
    render(<NavigationFlow sharingId="sharingId" />);

    // Assert
    expect(
      screen.getByTestId('NavigationFlow-SharingPage'),
    ).toBeInTheDocument();
  });
});
