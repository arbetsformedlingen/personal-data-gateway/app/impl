// @vitest-environment jsdom
import AlertBanner from '@app/components/AlertBanner';
import { render, screen } from '@testing-library/react';
import { describe, expect, it, vi } from 'vitest';

const { env } = vi.hoisted(() => ({
  env: {
    getEnv: vi.fn().mockReturnValue({
      NEXT_PUBLIC_ALERT_LEVEL: 'WARNING',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    }),
  },
}));

vi.mock('@app/env.mjs', () => env);

describe('AlertBanner', () => {
  it('is not displayed when NEXT_PUBLIC_ALERT_LEVEL is undefined', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: undefined,
      NEXT_PUBLIC_ALERT_TITLE: undefined,
      NEXT_PUBLIC_ALERT_TEXT: undefined,
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).not.toBeInTheDocument();
  });

  it('is displayed when NEXT_PUBLIC_ALERT_LEVEL is defined', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: 'SOMETHING',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).toBeInTheDocument();
  });

  it('is displayed when NEXT_PUBLIC_ALERT_LEVEL is "SUCCESS"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: 'SUCCESS',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).toBeInTheDocument();
  });

  it('is displayed when NEXT_PUBLIC_ALERT_LEVEL is "INFO"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: 'INFO',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).toBeInTheDocument();
  });

  it('is displayed when NEXT_PUBLIC_ALERT_LEVEL is "WARNING"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: 'WARNING',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).toBeInTheDocument();
  });

  it('is displayed when NEXT_PUBLIC_ALERT_LEVEL is "DANGER"', () => {
    // Arrange
    env.getEnv.mockReturnValueOnce({
      NEXT_PUBLIC_ALERT_LEVEL: 'DANGER',
      NEXT_PUBLIC_ALERT_TITLE: 'Foo',
      NEXT_PUBLIC_ALERT_TEXT: 'Bar',
    });

    // Act
    render(<AlertBanner />);

    // Assert
    const alertBanner = screen.queryByTestId('alert-banner');
    expect(alertBanner).toBeInTheDocument();
  });
});
