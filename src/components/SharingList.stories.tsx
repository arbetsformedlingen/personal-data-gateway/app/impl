import type { Meta, StoryObj } from '@storybook/react';

import SharingList from './SharingList';
import { type UserSharing as UserSharing } from '@app/hooks/useUserSharings';

const meta: Meta<typeof SharingList> = {
  component: SharingList,
  title: 'Components/SharingList',
};

export default meta;

type Story = StoryObj<typeof SharingList>;

export const Default: Story = {
  args: {
    userSharings: [],
  },
};

const userSharings: UserSharing[] = [
  {
    sharingId: '1',
    clientName: 'CLient 1',
    datasourceName: 'Datasource 1',
    created: '2024-10-12',
  },
  {
    sharingId: '2',
    clientName: 'CLient 2',
    datasourceName: 'Datasource 2',
    created: '2024-09-22',
  },
];

export const Example: Story = {
  args: {
    userSharings: userSharings,
  },
};
