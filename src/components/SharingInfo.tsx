import {
  FormValidationMessageVariation,
  LayoutBlockVariation,
  LayoutColumnsElement,
  LayoutColumnsVariation,
  LinkVariation,
  TypographyTimeVariation,
  TypographyVariation,
} from '@digi/arbetsformedlingen';
import {
  DigiFormValidationMessage,
  DigiLayoutBlock,
  DigiLayoutColumns,
  DigiLinkInternal,
  DigiTypography,
  DigiTypographyTime,
} from '@digi/arbetsformedlingen-react';
type Props = {
  consumer: string;
  datasourceName: string;
  created: string | number | Date;
  href: string;
};
import { useIntl } from 'react-intl';

const SharingInfo = ({ consumer, datasourceName, created, href }: Props) => {
  const intl = useIntl();
  return (
    <DigiLayoutBlock
      afVariation={LayoutBlockVariation.SECONDARY}
      data-testid="sharingInfo"
      className="pt-4"
    >
      <DigiTypography>
        <DigiLayoutColumns
          afElement={LayoutColumnsElement.DIV}
          afVariation={LayoutColumnsVariation.TWO}
        >
          <DigiTypography
            afVariation={TypographyVariation.SMALL}
            className="py-4"
          >
            <h6>
              {intl.formatMessage({ id: 'sharing-info-datatype-header' })}
            </h6>
            <p>{datasourceName}</p>
          </DigiTypography>
          <DigiTypography
            afVariation={TypographyVariation.SMALL}
            className="py-4"
          >
            <h6>
              {intl.formatMessage({ id: 'sharing-info-consumer-header' })}
            </h6>
            <p>{consumer}</p>
          </DigiTypography>
        </DigiLayoutColumns>
        <DigiLayoutColumns
          afElement={LayoutColumnsElement.DIV}
          afVariation={LayoutColumnsVariation.ONE}
        >
          <DigiFormValidationMessage
            afVariation={FormValidationMessageVariation.SUCCESS}
          >
            <div className="flex items-center justify-between py-4">
              <span>
                {intl.formatMessage({ id: 'sharing-info-approved' })}{' '}
                <DigiTypographyTime
                  afVariation={TypographyTimeVariation.PRIMARY}
                  afDateTime={created}
                ></DigiTypographyTime>
              </span>
              <DigiLinkInternal afHref={href} afVariation={LinkVariation.SMALL}>
                {intl.formatMessage({ id: 'sharing-info-open-action' })}
              </DigiLinkInternal>
            </div>
          </DigiFormValidationMessage>
        </DigiLayoutColumns>
      </DigiTypography>
    </DigiLayoutBlock>
  );
};

export default SharingInfo;
