import useDatasource from '@app/hooks/useDatasource';
import useFaqItems from '@app/hooks/useFaqItems';
import useOrganizationFaqs from '@app/hooks/useOrganizationFaqs';
import Error451 from './Error451';
import FAQ from './FAQ';

type Props = {
  datasourceId: string;
  message: string;
  consumerName: string;
  consumerOrgNr?: string;
  redirectUrl: string;
  foreignUserId: string;
};

function Error451Component({
  datasourceId,
  message,
  consumerName,
  consumerOrgNr,
  redirectUrl,
  foreignUserId,
}: Props) {
  const { datasource } = useDatasource(datasourceId);
  const { organizationFaqs } = useOrganizationFaqs(datasource?.organizationId);
  const { faqItems: organizationFaqItems } = useFaqItems(organizationFaqs);

  return (
    <>
      <Error451
        key="error451Component"
        message={message}
        redirectUrl={redirectUrl}
        foreignUserId={foreignUserId}
        orgNr={consumerOrgNr}
        orgName={consumerName}
        data-testid="error451-component"
      />
      <FAQ
        key="FAQComponent"
        data-testid="error451-faq"
        items={organizationFaqItems}
      />
    </>
  );
}

export default Error451Component;
