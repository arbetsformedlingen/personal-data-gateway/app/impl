import { TypographyTimeVariation } from '@digi/arbetsformedlingen';
import {
  DigiTypography,
  DigiTypographyTime,
} from '@digi/arbetsformedlingen-react';
import { useIntl } from 'react-intl';

export type Props = {
  fetched: string;
};

const SharingFetched = ({ fetched }: Props) => {
  const intl = useIntl();

  return (
    <DigiTypography data-testid="sharing-fetched">
      <div className="py-4">
        <div>{intl.formatMessage({ id: 'sharing-fetched-text' })}</div>
        <DigiTypographyTime
          afVariation={TypographyTimeVariation.PRIMARY}
          afDateTime={fetched}
        />
      </div>
    </DigiTypography>
  );
};

export default SharingFetched;
