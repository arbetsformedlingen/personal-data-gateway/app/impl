import {
  DigiExpandableFaq,
  DigiExpandableFaqItem,
} from '@digi/arbetsformedlingen-react';

export type FAQItem = {
  key: string;
  heading: string;
  itemContent: string[];
};

type Props = {
  items: FAQItem[];
};

function FAQ({ items }: Props) {
  const faqItems = items.map(({ key, heading, itemContent }) => {
    const paragraphs = itemContent.map((paragraph, idx) => (
      <p key={`${key}-${idx}}`}>{paragraph}</p>
    ));
    return (
      <DigiExpandableFaqItem key={key} afHeading={heading}>
        {paragraphs}
      </DigiExpandableFaqItem>
    );
  });

  return <DigiExpandableFaq data-testid="faq">{faqItems}</DigiExpandableFaq>;
}

export default FAQ;
