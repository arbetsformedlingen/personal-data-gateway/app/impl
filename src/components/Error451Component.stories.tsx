import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import Error451Component from './Error451Component';

import { getClient } from 'tests/msw/handlers/client';

export default {
  title: 'Components/Error451Component',
  component: Error451Component,
  args: {
    datasourceId: 'datasourceId',
    message: 'message',
    consumerName: 'consumerName',
    consumerOrgNr: 'consumerOrgNr',
    redirectUrl: 'redirectUrl',
  },
  parameters: {
    msw: {
      handlers: [getClient()],
    },
    layout: 'fullscreen',
  },
} as Meta<typeof Error451Component>;

const Template: StoryFn<typeof Error451Component> = (args) => (
  <Error451Component {...args} />
);

export const Default = Template.bind({});
