import SharingPage from './SharingPage';
import Error451Component from './Error451Component';
import Error from './Error';
import { useIntl } from 'react-intl';
import LayoutWrapper from './LayoutWrapper';
import useRedirectFlowData from '@app/hooks/useRedirectFlowData';

export type Props = {
  clientId: string;
  datasourceId: string;
  foreignUserId: string;
  redirectUrl: string;
};

const getErrorId = (error: {
  clientError: boolean;
  clientRedirectsError: boolean;
  datasourceError: boolean;
  clientOrganizationError: boolean;
  datasourceOrganizationError: boolean;
}) => {
  if (error.clientError) return 'error-invalid-client-id';
  else if (error.clientRedirectsError) return 'error-invalid-redirect-url';
  else if (error.datasourceError) return 'error-invalid-datasource-id';
  else if (error.clientOrganizationError)
    return 'error-invalid-client-organization';
  else return 'error-invalid-datasource-organization';
};

const showError = (errorId: string) =>
  LayoutWrapper(
    <Error
      key={errorId}
      data-testid={errorId}
      headerId={errorId}
      bodyIds={[
        'contact-dataconsumer-for-more-info',
        'you-can-now-close-window',
      ]}
    />,
  );

function RedirectFlow({
  clientId,
  datasourceId,
  foreignUserId,
  redirectUrl,
}: Props) {
  const intl = useIntl();

  const {
    isLoading,
    isError,
    previewIsLoading,
    error,
    data,
    userSharing,
    dataLastAccess,
  } = useRedirectFlowData(clientId, datasourceId);

  if (isLoading) {
    return LayoutWrapper(
      <div key="loadingComponent" data-testid="loading-component">
        {intl.formatMessage({ id: 'loading' })}...
      </div>,
    );
  }

  if (isError) {
    return showError(getErrorId(error));
  }

  const {
    consumerName,
    consumerClientId,
    consumerOrgNr,
    providerName,
    clientRedirects,
    unavailableForLegalReasons,
  } = data!;

  const redirectIsAcceptable = clientRedirects?.includes(redirectUrl);
  // handle redirect mismatch
  if (!redirectIsAcceptable) {
    return showError('error-invalid-redirect-url');
  }

  if (unavailableForLegalReasons) {
    return LayoutWrapper(
      <Error451Component
        key="RedirectFlow-error451Component"
        data-testid="RedirectFlow-error451Component"
        datasourceId={datasourceId}
        message={data?.unavailableForLegalReasonsMessage ?? ''}
        consumerName={data!.consumerName}
        consumerOrgNr={data?.consumerOrgNr}
        redirectUrl={redirectUrl}
        foreignUserId={foreignUserId}
      />,
    );
  }

  if (providerName === undefined)
    return (
      <div data-testid="providerName-error">
        Provider name must not be undefined
      </div>
    );

  return LayoutWrapper(
    <SharingPage
      data-testid="RedirectFlow-SharingPage"
      consumerName={consumerName}
      consumerClientId={consumerClientId}
      consumerOrgNr={consumerOrgNr}
      providerName={providerName}
      datasourceId={datasourceId}
      foreignUserId={foreignUserId}
      redirectUrl={redirectUrl}
      previewIsLoading={previewIsLoading}
      preview={data?.preview}
      sharing={userSharing?.unmapped}
      dataLastAccess={dataLastAccess}
    />,
  );
}

export default RedirectFlow;
