import type { Meta, StoryObj } from '@storybook/react';
import ConsumerInfo from './ConsumerInfo';

const meta: Meta<typeof ConsumerInfo> = {
  component: ConsumerInfo,
  title: 'Components/DataInfo',
  parameters: { layout: 'fullscreen' },
};

export default meta;

type Story = StoryObj<typeof ConsumerInfo>;

export const Default: Story = {
  args: {
    dataConsumerName: 'Consumer name',
    dataConsumerOrgNr: '123456-1234',
  },
};

export const Loading: Story = {};
