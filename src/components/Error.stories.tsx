import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import Error from './Error';

export default {
  title: 'Components/Error',
  component: Error,
  parameters: {
    layout: 'fullscreen',
  },
} as Meta<typeof Error>;

const Template: StoryFn<typeof Error> = () => (
  <Error headerId={'header'} bodyIds={['body1', 'body2']} />
);

export const Default = Template.bind({});
