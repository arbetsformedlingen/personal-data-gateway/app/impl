// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import { screen, render } from '@testing-library/react';
import RedirectFlow from '@app/components/RedirectFlow';
import useRedirectFlowData from '@app/hooks/useRedirectFlowData';
import { type components } from '@dist/spec';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

const {
  ErrorMock,
  Error451Mock,
  ErrorInvalidClientMock,
  ErrorTechnicalProblemMock,
  DataInfoMock,
  DataPreviewMock,
  ApprovalFormMock,
  FAQMock,
  SharingPageMock,
  Error451ComponentMock,
} = vi.hoisted(() => ({
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  ErrorMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  Error451Mock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  ErrorInvalidClientMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  ErrorTechnicalProblemMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  DataInfoMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  DataPreviewMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  ApprovalFormMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  FAQMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  SharingPageMock: vi.fn((props) => <div data-testid={props['data-testid']} />),
  Error451ComponentMock: vi.fn((props) => (
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
    <div data-testid={props['data-testid']} />
  )),
}));

vi.mock('@app/hooks/useRedirectFlowData');
vi.mock('@app/components/Error', () => ({ default: ErrorMock }));
vi.mock('@app/components/Error451', () => ({ default: Error451Mock }));
vi.mock('@app/components/ErrorInvalidClient', () => ({
  default: ErrorInvalidClientMock,
}));
vi.mock('@app/components/ErrorTechnicalProblem', () => ({
  default: ErrorTechnicalProblemMock,
}));
vi.mock('@app/components/DataInfo', () => ({ default: DataInfoMock }));
vi.mock('@app/components/DataPreview', () => ({ default: DataPreviewMock }));
vi.mock('@app/components/ApprovalForm', () => ({ default: ApprovalFormMock }));
vi.mock('@app/components/FAQ', () => ({ default: FAQMock }));
vi.mock('@app/components/SharingPage', () => ({ default: SharingPageMock }));
vi.mock('@app/components/Error451Component', () => ({
  default: Error451ComponentMock,
}));

describe('RedirectFlow', () => {
  it('displays loading component while consumer client is loading', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: true,
    });

    // Act
    render(
      <RedirectFlow
        clientId="clientId"
        datasourceId="datasourceId"
        foreignUserId="foreignUserId"
        redirectUrl="redirectUrl"
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('loading-component')).toBeInTheDocument();
  });

  it('displays Error when there is a clientError', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: true,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('error-invalid-client-id')).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-client-id',
        headerId: 'error-invalid-client-id',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a clientRedirectsError', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        clientRedirectsError: true,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-redirect-url'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-redirect-url',
        headerId: 'error-invalid-redirect-url',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when the redirect URL is not among the returned client redirects', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        clientRedirects: ['redirectUrl1', 'redirectUrl2'],
        preview: 'preview',
        sharingCreated: 'sharingCreated',
        sharingRevoked: 'sharingRevoked',
        sharingExpires: 'sharingExpires',
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: false,
        unavailableForLegalReasonsMessage: undefined,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-redirect-url'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-redirect-url',
        headerId: 'error-invalid-redirect-url',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a datasourceError', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: true,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-datasource-id'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-datasource-id',
        headerId: 'error-invalid-datasource-id',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a clientOrganizationError', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: true,
        userSharingsError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-client-organization'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-client-organization',
        headerId: 'error-invalid-client-organization',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error when there is a datasourceOrganizationError', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: true,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: true,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('error-invalid-datasource-organization'),
    ).toBeInTheDocument();
    expect(ErrorMock).toHaveBeenCalledWith(
      {
        'data-testid': 'error-invalid-datasource-organization',
        headerId: 'error-invalid-datasource-organization',
        bodyIds: [
          'contact-dataconsumer-for-more-info',
          'you-can-now-close-window',
        ],
      },
      {},
    );
  });

  it('displays Error451Component when preview could not be fetched due to legal reasons', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        clientRedirects: ['redirectUrl'],
        preview: undefined,
        sharingCreated: undefined,
        sharingRevoked: 'sharingRevoked',
        sharingExpires: 'sharingExpires',
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: true,
        unavailableForLegalReasonsMessage: 'unavailableForLegalReasonsMessage',
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('RedirectFlow-error451Component'),
    ).toBeInTheDocument();
    expect(Error451ComponentMock).toHaveBeenCalledWith(
      {
        consumerName: 'consumerName',
        consumerOrgNr: 'consumerOrgNr',
        'data-testid': 'RedirectFlow-error451Component',
        datasourceId: 'datasourceId',
        message: 'unavailableForLegalReasonsMessage',
        redirectUrl: 'redirectUrl',
        foreignUserId: 'foreignUserId',
      },
      {},
    );
  });

  it('passes an empty string to the Error451Component "message" prop when the preview "unavailableForLegalReasonsMessage" is undefined', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        clientRedirects: ['redirectUrl'],
        preview: undefined,
        sharingCreated: undefined,
        sharingRevoked: 'sharingRevoked',
        sharingExpires: 'sharingExpires',
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: true,
        unavailableForLegalReasonsMessage: undefined,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(
      screen.getByTestId('RedirectFlow-error451Component'),
    ).toBeInTheDocument();
    expect(Error451ComponentMock).toHaveBeenCalledWith(
      {
        consumerName: 'consumerName',
        consumerOrgNr: 'consumerOrgNr',
        'data-testid': 'RedirectFlow-error451Component',
        datasourceId: 'datasourceId',
        message: '',
        redirectUrl: 'redirectUrl',
        foreignUserId: 'foreignUserId',
      },
      {},
    );
  });

  it('displays "Provider name must not be undefined" when "providerName"-property is undefined', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      previewIsLoading: true,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: undefined,
        clientRedirects: ['redirectUrl'],
        preview: undefined,
        sharingCreated: undefined,
        sharingRevoked: undefined,
        sharingExpires: undefined,
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: false,
        unavailableForLegalReasonsMessage: undefined,
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    const element = screen.queryByTestId('providerName-error');
    expect(element).toBeInTheDocument();

    expect(element).toHaveTextContent('Provider name must not be undefined');
  });

  it('displays SharingPage when all preconditions are met', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      previewIsLoading: true,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        clientRedirects: ['redirectUrl'],
        preview: undefined,
        sharingCreated: undefined,
        sharingRevoked: undefined,
        sharingExpires: undefined,
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: false,
        unavailableForLegalReasonsMessage: undefined,
      },
      userSharing: {
        unmapped: {} as components['schemas']['GetUserSharingResponse'],
        sharingId: 'sharingId',
        clientName: 'clientName',
        datasourceName: 'datasourceName',
        created: 'created',
        revoked: 'revoked',
        expires: 'expires',
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('RedirectFlow-SharingPage')).toBeInTheDocument();
    expect(SharingPageMock).toHaveBeenCalledWith(
      {
        consumerClientId: 'consumerClientId',
        consumerName: 'consumerName',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        redirectUrl: 'redirectUrl',
        'data-testid': 'RedirectFlow-SharingPage',
        datasourceId: 'datasourceId',
        foreignUserId: 'foreignUserId',
        previewIsLoading: true,
        preview: undefined,
        sharing: {},
        dataLastAccess: undefined,
      },
      {},
    );
  });

  it('displays SharingPage with sharing status when sharing is fetched', () => {
    // Arrange
    vi.mocked(useRedirectFlowData, { partial: true }).mockReturnValue({
      isLoading: false,
      previewIsLoading: false,
      isError: false,
      error: {
        clientError: false,
        clientRedirectsError: false,
        datasourceError: false,
        datasourceOrganizationError: false,
        clientOrganizationError: false,
        userSharingsError: false,
        dataAccessError: false,
      },
      data: {
        consumerName: 'consumerName',
        consumerClientId: 'consumerClientId',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        clientRedirects: ['redirectUrl'],
        preview: 'preview',
        sharingCreated: 'sharingCreated',
        sharingRevoked: undefined,
        sharingExpires: undefined,
        sharingLastAccessed: undefined,
        unavailableForLegalReasons: false,
        unavailableForLegalReasonsMessage: undefined,
      },
      userSharing: {
        unmapped: {} as components['schemas']['GetUserSharingResponse'],
        sharingId: 'sharingId',
        clientName: 'clientName',
        datasourceName: 'datasourceName',
        created: 'created',
        revoked: 'revoked',
        expires: 'expires',
      },
    });

    // Act
    render(
      <RedirectFlow
        clientId={'clientId'}
        datasourceId={'datasourceId'}
        foreignUserId={'foreignUserId'}
        redirectUrl={'redirectUrl'}
      />,
    );

    // Assert
    expect(screen.getByTestId('LayoutWrapper')).toBeInTheDocument();
    expect(screen.getByTestId('RedirectFlow-SharingPage')).toBeInTheDocument();
    expect(SharingPageMock).toHaveBeenCalledWith(
      {
        consumerClientId: 'consumerClientId',
        consumerName: 'consumerName',
        consumerOrgNr: 'consumerOrgNr',
        providerName: 'providerName',
        redirectUrl: 'redirectUrl',
        'data-testid': 'RedirectFlow-SharingPage',
        datasourceId: 'datasourceId',
        foreignUserId: 'foreignUserId',
        previewIsLoading: false,
        preview: 'preview',
        dataLastAccess: undefined,
        sharing: {},
      },
      {},
    );
  });
});
