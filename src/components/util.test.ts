import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { isActive } from './util';

describe('isActive', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('returns false if sharing created is underfined', () => {
    // Arrange
    const revoked = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const expires = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(
      undefined,
      revoked.toISOString(),
      expires.toISOString(),
    );

    // Assert
    expect(result).toBeFalsy();
  });

  it('returns true if sharing not yet expired or revoked', () => {
    // Arrange
    const created = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const revoked = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const expires = new Date(Date.UTC(2024, 1, 2, 13, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(
      created.toISOString(),
      revoked.toISOString(),
      expires.toISOString(),
    );

    // Assert
    expect(result).toBeTruthy();
  });

  it('returns false if sharing has been revoked', () => {
    // Arrange
    const created = new Date(Date.UTC(2024, 1, 1, 9, 14, 15));
    const revoked = new Date(Date.UTC(2024, 1, 1, 10, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(
      created.toISOString(),
      revoked.toISOString(),
      undefined,
    );

    // Assert
    expect(result).toBeFalsy();
  });

  it('returns false if sharing has expired', () => {
    // Arrange
    const created = new Date(Date.UTC(2024, 1, 1, 9, 14, 15));
    const expires = new Date(Date.UTC(2024, 1, 1, 10, 14, 15));
    const newLocal = new Date(Date.UTC(2024, 1, 1, 13, 14, 15));
    const now = newLocal;
    vi.setSystemTime(now);

    // Act
    const result = isActive(
      created.toISOString(),
      undefined,
      expires.toISOString(),
    );

    // Assert
    expect(result).toBeFalsy();
  });

  it('returns true if sharing has not been revoked or expired', () => {
    // Act
    const created = new Date(Date.UTC(2024, 1, 1, 9, 14, 15));
    const result = isActive(created.toISOString(), undefined, undefined);

    // Assert
    expect(result).toBeTruthy();
  });
});
