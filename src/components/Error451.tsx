import React from 'react';
import { DigiButton } from '@digi/arbetsformedlingen-react';
import useTargetUrl from '@app/hooks/useTargetUrl';
import usePreventLeaving from '@app/hooks/usePreventLeaving';
import { ButtonVariation } from '@digi/arbetsformedlingen';
import ConsumerInfo from './ConsumerInfo';
import { useIntl } from 'react-intl';

export type Props = {
  message: string;
  redirectUrl: string;
  foreignUserId: string;
  orgNr?: string;
  orgName?: string;
};

function Error451({
  message,
  redirectUrl,
  foreignUserId,
  orgNr,
  orgName,
}: Props) {
  const intl = useIntl();
  const { setPreventLeaving } = usePreventLeaving(false);
  const { setTargetUrl } = useTargetUrl(undefined, setPreventLeaving);

  const handleAbort = () => {
    setTargetUrl(`${redirectUrl}?state=${foreignUserId}&message=Denied`);
  };

  return (
    <div>
      <h2>{intl.formatMessage({ id: 'digital-sharing-not-possible' })}</h2>
      <ConsumerInfo
        dataConsumerName={orgName}
        dataConsumerOrgNr={orgNr}
        data-testid="sharing-datainfo"
      />
      <p>{message}</p>
      <DigiButton
        onAfOnClick={() => handleAbort()}
        afVariation={ButtonVariation.SECONDARY}
        afFullWidth
        data-testid="abortError451"
      >
        {intl.formatMessage({ id: 'action-close-and-return' })}
      </DigiButton>
    </div>
  );
}

export default Error451;
