import { useIntl } from 'react-intl';

function Maintenance() {
  const intl = useIntl();
  return (
    <div data-testid="maintenance-page">
      <div className="mx-4 mb-8 flex flex-col items-center justify-center">
        <h1 data-testid="maintenance-page-title">
          {intl.formatMessage({ id: 'maintenance-mode-title' })}
        </h1>
        <p data-testid="maintenance-page-description">
          {intl.formatMessage({ id: 'maintenance-mode-description' })}
        </p>
      </div>
    </div>
  );
}

export default Maintenance;
