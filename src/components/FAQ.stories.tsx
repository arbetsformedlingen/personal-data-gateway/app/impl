import React from 'react';

import type { Meta, StoryFn } from '@storybook/react';
import FAQ from './FAQ';

export default {
  title: 'Components/FAQ',
  component: FAQ,
} as Meta<typeof FAQ>;

const Template: StoryFn<typeof FAQ> = (args) => <FAQ {...args} />;

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      key: 'item-key-1',
      heading: 'Lorem ipsum',
      itemContent: [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      ],
    },
  ],
};

export const MultipleParagraph = Template.bind({});
MultipleParagraph.args = {
  items: [
    {
      key: 'item-key-1',
      heading: 'Lorem ipsum',
      itemContent: [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      ],
    },
  ],
};
