import type { Meta, StoryObj } from '@storybook/react';

import SharingStatus from './SharingStatus';

const meta: Meta<typeof SharingStatus> = {
  component: SharingStatus,
  title: 'Components/SharingStatus',
};

export default meta;

type Story = StoryObj<typeof SharingStatus>;

export const Default: Story = {
  args: {
    created: '2024-08-11 13:45',
    fetched: '2024-09-11 08:45',
  },
};
