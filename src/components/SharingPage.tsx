import ApprovalForm from '@app/components/ApprovalForm';
import ConsumerInfo from '@app/components/ConsumerInfo';
import DataPreview from '@app/components/DataPreview';
import FAQ from '@app/components/FAQ';
import SharingStatus from './SharingStatus';
import { type components } from '@dist/spec';
import useSharingPageData from '@app/hooks/useSharingPageData';

type Props = {
  consumerName: string;
  consumerClientId: string;
  consumerOrgNr?: string;
  providerName: string;
  datasourceId: string;
  foreignUserId: string | undefined;
  redirectUrl: string | undefined;
  previewIsLoading: boolean;
  preview:
    | string
    | {
        status: number;
        error: string;
        message?: string | undefined;
      }
    | Blob
    | ArrayBuffer
    | ReadableStream<Uint8Array>
    | null
    | undefined;
  sharing?: components['schemas']['GetUserSharingResponse'];
  dataLastAccess?: components['schemas']['GetDataAccessResponse'];
};

function SharingPage({
  consumerName,
  consumerClientId,
  consumerOrgNr,
  providerName,
  datasourceId,
  foreignUserId,
  redirectUrl,
  previewIsLoading,
  preview,
  sharing,
  dataLastAccess,
}: Props) {
  const { header, paragraph, active, created, variant, faqItems } =
    useSharingPageData({
      providerName,
      sharing,
      datasourceId,
    });

  return (
    <div key="sharingPage" data-testid="sharing-page">
      <h1 data-testid="sharing-page-header">{header}</h1>
      <p data-testid="sharing-page-paragraph">{paragraph}</p>
      <ConsumerInfo
        dataConsumerName={consumerName}
        dataConsumerOrgNr={consumerOrgNr}
        data-testid="sharing-consumerinfo"
      />
      <DataPreview
        datasourceId={datasourceId}
        data-testid="sharing-datapreview"
        previewIsLoading={previewIsLoading}
        preview={preview}
      />
      <SharingStatus
        data-testid="sharing-status"
        created={created}
        active={active}
        fetched={dataLastAccess?.accessed}
      />
      <ApprovalForm
        variant={variant}
        sharingId={sharing?.sharingId}
        clientId={consumerClientId}
        datasourceId={datasourceId}
        foreignUserId={foreignUserId}
        redirectUrl={redirectUrl}
        data-testid="sharing-approvalform"
      />
      <FAQ data-testid="sharing-faq" items={faqItems} />
    </div>
  );
}

export default SharingPage;
