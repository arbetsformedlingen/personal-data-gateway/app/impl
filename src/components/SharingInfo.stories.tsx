import type { Meta, StoryObj } from '@storybook/react';

import SharingInfo from './SharingInfo';

const meta: Meta<typeof SharingInfo> = {
  component: SharingInfo,
  title: 'Components/SharingInfo',
};

export default meta;

type Story = StoryObj<typeof SharingInfo>;

export const Default: Story = {};

export const Example: Story = {
  args: {
    datasourceName: 'Datasource name',
    consumer: 'Consumer name',
    created: '2024-05-05 14:03',
    href: '/?clientId=b3835549-e74b-4ebb-ac91-366bd1d665b4&datasourceId=7fd32b62-1477-4925-920f-1021b323c413&state=abc123&redirectUrl=http%3A%2F%2Flocalhost:3300%2Freturn',
  },
};
