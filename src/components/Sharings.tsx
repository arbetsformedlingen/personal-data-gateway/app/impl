import LayoutWrapper from './LayoutWrapper';
import FAQ, { type FAQItem } from './FAQ';
import SharingList from './SharingList';
import { type UserSharing } from '@app/hooks/useUserSharings';

type Props = {
  userSharings: UserSharing[];
  faqItems: FAQItem[];
};

const Sharings = ({ userSharings, faqItems }: Props) => {
  return LayoutWrapper(
    <div>
      <h1>Datadelningar som du godkänt</h1>
      Ta del av information du delar med dina mottagare och välj om du vill
      fortsätta dela.
      <SharingList data-testid="sharings-list" userSharings={userSharings} />
      <FAQ data-testid="sharings-faq" items={faqItems} />
    </div>,
  );
};

export default Sharings;
