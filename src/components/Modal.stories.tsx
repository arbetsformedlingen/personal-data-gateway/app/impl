import type { Meta, StoryObj } from '@storybook/react';
import Modal from './Modal';

const meta: Meta<typeof Modal> = {
  component: Modal,
  title: 'Components/Modal',
};

export default meta;

type Story = StoryObj<typeof Modal>;

export const Error: Story = {};

export const Revoke: Story = {
  args: {
    title: 'Informationen delas inte längre med mottagren',
    message: 'Button',
    primaryButtonText: 'Ok',
  },
};
