import {
  LayoutBlockVariation,
  FormValidationMessageVariation,
  TypographyTimeVariation,
} from '@digi/arbetsformedlingen';
import {
  DigiLayoutBlock,
  DigiTypography,
  DigiFormValidationMessage,
  DigiTypographyTime,
} from '@digi/arbetsformedlingen-react';
import { useIntl } from 'react-intl';
import SharingFetched from './SharingFetched';

export type Props = {
  created?: string;
  active: boolean;
  fetched?: string;
};

const SharingStatus = ({ created, active, fetched }: Props) => {
  const intl = useIntl();

  if (created === undefined || !active) {
    return undefined;
  }

  return (
    <DigiLayoutBlock
      data-testid="sharing-status"
      afVariation={LayoutBlockVariation.SECONDARY}
      className="pt-4"
    >
      <DigiTypography>
        <div className="py-4">
          <DigiFormValidationMessage
            afVariation={FormValidationMessageVariation.SUCCESS}
          >
            <div>
              <h3>{intl.formatMessage({ id: 'sharing-status-text' })}</h3>
              <DigiTypographyTime
                afVariation={TypographyTimeVariation.PRIMARY}
                afDateTime={created}
              />
            </div>
          </DigiFormValidationMessage>
        </div>
      </DigiTypography>
      {fetched && <SharingFetched fetched={fetched} />}
    </DigiLayoutBlock>
  );
};

export default SharingStatus;
