import Home from './Home';
import NavigationFlow, {
  type Props as NavigationFlowProps,
} from './NavigationFlow';
import RedirectFlow, { type Props as RedirectFlowProps } from './RedirectFlow';

export type Props = {
  redirectFlow?: RedirectFlowProps;
  navigationFlow?: NavigationFlowProps;
};

function Application({ redirectFlow, navigationFlow }: Props) {
  if (redirectFlow) {
    const { clientId, datasourceId, foreignUserId, redirectUrl } = redirectFlow;
    return (
      <RedirectFlow
        clientId={clientId}
        datasourceId={datasourceId}
        foreignUserId={foreignUserId}
        redirectUrl={redirectUrl}
        data-testid="application-redirectFlow"
      />
    );
  } else if (navigationFlow) {
    const { sharingId } = navigationFlow;
    return (
      <NavigationFlow
        sharingId={sharingId}
        data-testid="application-navigationFlow"
      />
    );
  } else {
    return <Home data-testid="application-home" />;
  }
}

export default Application;
