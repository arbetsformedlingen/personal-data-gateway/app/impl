import { DialogHeadingLevel, DialogSize } from '@digi/arbetsformedlingen';
import { DigiDialog } from '@digi/arbetsformedlingen-react';
import { useIntl } from 'react-intl';

type Props = {
  onClose: () => void;
  title?: string;
  message?: string;
  closeButtonText?: string;
  primaryButtonText?: string;
  headingLevel?: DialogHeadingLevel;
};

const Modal = ({
  onClose,
  title,
  message,
  closeButtonText,
  primaryButtonText,
  headingLevel,
}: Props) => {
  const intl = useIntl();

  return (
    <DigiDialog
      data-testid="modal"
      afCloseButtonText={closeButtonText}
      afPrimaryButtonText={primaryButtonText}
      onAfOnClose={onClose}
      afSize={DialogSize.SMALL}
      afShowDialog={true}
      afHeading={title ?? intl.formatMessage({ id: 'generic-error' })}
      afHeadingLevel={headingLevel ?? DialogHeadingLevel.H2}
    >
      {message ?? intl.formatMessage({ id: 'please-try-again-later' })}
    </DigiDialog>
  );
};

export default Modal;
