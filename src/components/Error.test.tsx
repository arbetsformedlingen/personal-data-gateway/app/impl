// @vitest-environment jsdom
import { render } from '@testing-library/react';
import { expect, describe, it, vi, afterEach } from 'vitest';
import Error from './Error';

vi.mock('react-intl', () => ({
  useIntl: () => ({ formatMessage: formatMessageMock }),
}));

const { formatMessageMock } = vi.hoisted(() => ({
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
  formatMessageMock: vi.fn().mockImplementation(({ id }: { id: string }) => {
    return `example-${id}`;
  }),
}));

describe('Error', () => {
  afterEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
  });

  it('should render empty error', () => {
    const component = render(<Error headerId={''} bodyIds={[]} />);
    expect(component).toBeDefined();
    expect(formatMessageMock).toBeCalledTimes(1);
    expect(formatMessageMock).toBeCalledWith({ id: '' });
  });

  it('should render error with title', () => {
    const component = render(<Error headerId={'title'} bodyIds={[]} />);
    expect(component).toBeDefined();
    expect(formatMessageMock).toBeCalledTimes(1);
    expect(formatMessageMock).toBeCalledWith({ id: 'title' });
  });

  it('should render error with body array', () => {
    const component = render(
      <Error headerId={''} bodyIds={['body1', 'body2']} />,
    );
    expect(component).toBeDefined();
    expect(formatMessageMock).toBeCalledTimes(3);
    expect(formatMessageMock).toBeCalledWith({ id: '' });
    expect(formatMessageMock).toBeCalledWith({ id: 'body1' });
    expect(formatMessageMock).toBeCalledWith({ id: 'body2' });
  });
});
