// @vitest-environment jsdom
import { describe, expect, it, vi } from 'vitest';
import { screen, render } from '@testing-library/react';
import SharingFetched from './SharingFetched';

vi.mock('react-intl', () => ({
  useIntl: () => ({
    formatMessage: vi.fn().mockImplementation(({ id }: { id: string }) => {
      return `example-${id}`;
    }),
  }),
}));

describe('SharingFetched', () => {
  it('passes with fetched information', () => {
    // Act
    render(<SharingFetched fetched="2024-08-11" />);

    // Assert
    expect(screen.getByTestId('sharing-fetched')).toBeInTheDocument();
  });
});
