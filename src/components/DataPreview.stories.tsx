import type { Meta, StoryObj } from '@storybook/react';
import DataPreview from './DataPreview';

const meta: Meta<typeof DataPreview> = {
  component: DataPreview,
  title: 'Components/DataPreview',
  parameters: { layout: 'fullscreen' },
};

export default meta;

type Story = StoryObj<typeof DataPreview>;

export const Loading: Story = {
  args: {
    previewIsLoading: true,
  },
};

export const Default: Story = {};
