// @vitest-environment jsdom
import useDatasource from '@app/hooks/useDatasource';
import useFaqItems from '@app/hooks/useFaqItems';
import useOrganizationFaqs from '@app/hooks/useOrganizationFaqs';
import type { components } from '@dist/spec';
import { render, screen } from '@testing-library/react';
import { beforeEach, describe, expect, it, type Mocked, vi } from 'vitest';
import Error451Component from './Error451Component';

const { Error451Mock, FAQ } = vi.hoisted(() => ({
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Error451Mock: (props: any) => <div {...props} />,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  FAQ: (props: any) => <div {...props} />,
}));

vi.mock('@app/hooks/useDatasource');
vi.mock('@app/hooks/useFaqItems');
vi.mock('@app/hooks/useOrganizationFaqs');
vi.mock('./Error451', () => ({ default: Error451Mock }));
vi.mock('./FAQ', () => ({ default: FAQ }));

describe('Error451Component', () => {
  let useDatasourceMock = vi.fn();
  let useOrganizationFaqsMock = vi.fn();
  let useFaqItemsMock = vi.fn();

  beforeEach(() => {
    vi.clearAllMocks();
    vi.resetAllMocks();
    useDatasourceMock = vi.mocked(useDatasource).mockReturnValue({
      datasource: {} as Mocked<components['schemas']['GetDatasourceResponse']>,
    });
    useOrganizationFaqsMock = vi.mocked(useOrganizationFaqs).mockReturnValue({
      organizationFaqs: [] as Mocked<
        components['schemas']['GetOrganizationFaqsResponse']
      >,
    });
    useFaqItemsMock = vi.mocked(useFaqItems).mockReturnValue({ faqItems: [] });
  });

  it('invokes useDatasource with the "datasourceId" prop', () => {
    // Act
    render(
      <Error451Component
        datasourceId="datasourceId"
        consumerName="consumerName"
        consumerOrgNr="consumerOrgNr"
        message="message"
        redirectUrl="redirectUrl"
        foreignUserId="foreignUserId"
      />,
    );

    // Assert
    expect(useDatasourceMock).toHaveBeenCalledWith('datasourceId');
  });

  it('invokes useOrganizationFaqs with the "organizationId" retrieved from "useDatasource"', () => {
    // Arrange
    useDatasourceMock.mockReturnValue({
      datasource: { organizationId: 'organizationId' },
    });

    // Act
    render(
      <Error451Component
        datasourceId="datasourceId"
        consumerName="consumerName"
        consumerOrgNr="consumerOrgNr"
        message="message"
        redirectUrl="redirectUrl"
        foreignUserId="foreignUserId"
      />,
    );

    // Assert
    expect(useOrganizationFaqs).toHaveBeenCalledWith('organizationId');
  });

  it('invokes useFaqItems with the "organizationFaqs" array retrieved from useOrganizationFaqsMock', () => {
    // Arrange
    useOrganizationFaqsMock.mockReturnValue({
      organizationFaqs: 'organizationFaqs',
    });

    // Act
    render(
      <Error451Component
        datasourceId="datasourceId"
        consumerName="consumerName"
        consumerOrgNr="consumerOrgNr"
        message="message"
        redirectUrl="redirectUrl"
        foreignUserId="foreignUserId"
      />,
    );

    // Assert
    expect(useFaqItems).toHaveBeenCalledWith('organizationFaqs');
  });

  describe('Error451', () => {
    it('passes "message" from "message" prop', () => {
      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-component')).toHaveAttribute(
        'message',
        'message',
      );
    });

    it('passes "orgNr" from "consumerOrgNr" prop', () => {
      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-component')).toHaveAttribute(
        'orgNr',
        'consumerOrgNr',
      );
    });

    it('passes "orgName" from "consumerName" prop', () => {
      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-component')).toHaveAttribute(
        'orgName',
        'consumerName',
      );
    });

    it('passes "redirectUrl" from "redirectUrl" prop', () => {
      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-component')).toHaveAttribute(
        'redirectUrl',
        'redirectUrl',
      );
    });

    it('passes "foreignUserId" from "foreignUserId" prop', () => {
      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-component')).toHaveAttribute(
        'foreignUserId',
        'foreignUserId',
      );
    });
  });

  describe('FAQ', () => {
    it('passes "items" from "organizationFaqItems" prop', () => {
      // Arrange
      useFaqItemsMock.mockReturnValue({ faqItems: 'items' });

      // Act
      render(
        <Error451Component
          datasourceId="datasourceId"
          consumerName="consumerName"
          consumerOrgNr="consumerOrgNr"
          message="message"
          redirectUrl="redirectUrl"
          foreignUserId="foreignUserId"
        />,
      );

      // Assert
      expect(screen.getByTestId('error451-faq')).toHaveAttribute(
        'items',
        'items',
      );
    });
  });
});
