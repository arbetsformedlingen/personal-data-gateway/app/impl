import { useSearchParams } from 'next/navigation';
import LogoHeader from '@app/components/LogoHeader';
import { DigiLayoutContainer } from '@digi/arbetsformedlingen-react';
import AlertBanner from '@app/components/AlertBanner';
import Application from '@app/components/Application';
import Home from '@app/components/Home';

export default function IndexPage() {
  const searchParams = useSearchParams();
  const params = new URLSearchParams(searchParams);

  const content = (params: URLSearchParams) => {
    const sharingId = params.get('sharingId');
    const clientId = params.get('clientId');
    const datasourceId = params.get('datasourceId');
    const foreignUserId = params.get('state');
    const redirectUrl = params.get('redirectUrl');

    if (sharingId) {
      return (
        <Application
          data-testid="index-application-navigation-flow"
          navigationFlow={{ sharingId }}
        />
      );
    } else if (clientId && datasourceId && foreignUserId && redirectUrl) {
      return (
        <Application
          data-testid="index-application-redirect-flow"
          redirectFlow={{ clientId, datasourceId, foreignUserId, redirectUrl }}
        />
      );
    }
    return <Home data-testid="index-home" />;
  };

  return (
    <>
      <AlertBanner />
      <DigiLayoutContainer>
        <div className="mx-4 mb-8 flex flex-col items-center justify-center">
          <LogoHeader />
          {content(params)}
        </div>
      </DigiLayoutContainer>
    </>
  );
}
