import { getEnv } from '@app/env.mjs';
import {
  NotificationAlertSize,
  NotificationAlertVariation,
} from '@digi/arbetsformedlingen';
import {
  DigiLayoutContainer,
  DigiNotificationAlert,
} from '@digi/arbetsformedlingen-react';

function AlertBanner() {
  const {
    NEXT_PUBLIC_ALERT_LEVEL,
    NEXT_PUBLIC_ALERT_TITLE,
    NEXT_PUBLIC_ALERT_TEXT,
  } = getEnv();

  if (NEXT_PUBLIC_ALERT_LEVEL) {
    let alertLevel;
    switch (NEXT_PUBLIC_ALERT_LEVEL) {
      case 'SUCCESS':
        alertLevel = NotificationAlertVariation.SUCCESS;
        break;
      case 'INFO':
        alertLevel = NotificationAlertVariation.INFO;
        break;
      case 'WARNING':
        alertLevel = NotificationAlertVariation.WARNING;
        break;
      case 'DANGER':
        alertLevel = NotificationAlertVariation.DANGER;
        break;
      default:
        alertLevel = NotificationAlertVariation.INFO;
        break;
    }

    return (
      <DigiLayoutContainer afVerticalPadding={true}>
        <DigiNotificationAlert
          data-testid="alert-banner"
          afSize={NotificationAlertSize.LARGE}
          afVariation={alertLevel}
          afHeading={NEXT_PUBLIC_ALERT_TITLE}
        >
          {NEXT_PUBLIC_ALERT_TEXT}
        </DigiNotificationAlert>
      </DigiLayoutContainer>
    );
  }
}

export default AlertBanner;
