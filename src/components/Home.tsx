import useHomeData from '@app/hooks/useHomeData';
import {
  DigiLayoutContainer,
  DigiTypography,
} from '@digi/arbetsformedlingen-react';
import SharingList from './SharingList';
import FAQ from './FAQ';

const Home = () => {
  const { header, paragraph, faqItems, userSharings } = useHomeData();

  return (
    <DigiLayoutContainer
      data-testid="Home-container"
      afVerticalPadding
      afNoGutter
    >
      <div data-testid="Home-innerContainer">
        <DigiTypography data-testid="Home-typography">
          <h1 data-testid="Home-header">{header}</h1>
          <p data-testid="Home-paragraph">{paragraph}</p>
        </DigiTypography>
        <SharingList data-testid="Home-sharings" userSharings={userSharings} />
        <FAQ data-testid="Home-faq" items={faqItems} />
      </div>
    </DigiLayoutContainer>
  );
};

export default Home;
