import { createEnv } from '@t3-oss/env-nextjs';
import { z } from 'zod';

export const getEnv = () => createEnv({
  /**
   * Specify your server-side environment variables schema here. This way you can ensure the app
   * isn't built with invalid env vars.
   */
  server: {
    NODE_ENV: z.enum(['development', 'test', 'production']),
    SERVICE_URL: z.string().url(),
    AUTH_HEADER: z.string(),
    X_AUTH_KEY: z.string(),
    LOG_LEVEL: z.string().optional(),
  },

  /**
   * Specify your client-side environment variables schema here. This way you can ensure the app
   * isn't built with invalid env vars. To expose them to the client, prefix them with
   * `NEXT_PUBLIC_`.
   */
  client: {
    NEXT_PUBLIC_MAINTENANCE_MODE: z.string().optional(),
    NEXT_PUBLIC_ALERT_LEVEL: z.string().optional(),
    NEXT_PUBLIC_ALERT_TITLE: z.string().optional(),
    NEXT_PUBLIC_ALERT_TEXT: z.string().optional(),
  },

  /**
   * You can't destruct `process.env` as a regular object in the Next.js edge runtimes (e.g.
   * middlewares) or client-side so we need to destruct manually.
   */
  runtimeEnv: {
    NODE_ENV: process.env.NODE_ENV,
    SERVICE_URL: process.env.SERVICE_URL,
    AUTH_HEADER: process.env.AUTH_HEADER,
    X_AUTH_KEY: process.env.X_AUTH_KEY,
    LOG_LEVEL: process.env.LOG_LEVEL,
    NEXT_PUBLIC_MAINTENANCE_MODE: process.env.NEXT_PUBLIC_MAINTENANCE_MODE,
    NEXT_PUBLIC_ALERT_LEVEL: process.env.NEXT_PUBLIC_ALERT_LEVEL,
    NEXT_PUBLIC_ALERT_TITLE: process.env.NEXT_PUBLIC_ALERT_TITLE,
    NEXT_PUBLIC_ALERT_TEXT: process.env.NEXT_PUBLIC_ALERT_TEXT,
  },
  /**
   * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation.
   * This is especially useful for Docker builds.
   */
  skipValidation: !!process.env.SKIP_ENV_VALIDATION,
});

// This "env" export is deprecated, please use the exported "getEnv" instead.
export const env = getEnv();
